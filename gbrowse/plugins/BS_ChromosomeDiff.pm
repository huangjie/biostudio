package Bio::Graphics::Browser2::Plugin::BS_ChromosomeDiff;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw(&configure_BioStudio);
use Bio::BioStudio::GBrowse qw(get_gbrowse_src_list);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeDiff_$VERSION";
$| = 1;

##Global variables 
my $BS;
my %onlabels = (1 => "yes");

sub name { "BioStudio: Compare Chromosome Versions" }
sub type { 'dumper' }
sub verb { ' '  }

sub description 
{
  p("This plugin compares a single chromosome from different sources and 
    highlights the differences.");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub mime_type 
{
  return 'text/html'
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub configure_form 
{
  my $self = shift;
  my $curr_config = $self->configuration;
  my $gbrowse_settings = $self->page_settings;
  my $sourcename  = $$gbrowse_settings{source};
  my @srcs        = get_gbrowse_src_list($BS);
  
  my @choices = 
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Check ORF Translations",  
        td(
          checkbox_group(
            -name       => $self->config_name('PTRANSLATION'), 
            -values     => \%onlabels, 
            -linebreak  => 0, 
            -default    => 1))));
          
  push @choices, 
    TR({-class => 'searchtitle'},  
      th  ( {-align=>'RIGHT',-width=>'50%'},
      "Check for nucleotide sequence differences",  
        td(
          checkbox_group(
            -name       => $self->config_name('NSEQUENCE'), 
            -values     => \%onlabels, 
            -linebreak  => 0, 
            -default    => 1))));
            
  push @choices, 
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Make alignments for features with nucleotide sequence differences",  
        td(
          checkbox_group(
            -name       => $self->config_name('NALIGN'),
            -values     => \%onlabels,
            -linebreak  => 0,
            -default    => 0))));
            
  push @choices, 
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Make protein alignments for features with translation differences",  
        td(
          checkbox_group(
            -name       => $self->config_name('PALIGN'), 
            -values     => \%onlabels, 
            -linebreak  => 0, 
            -default    => 0))));
            
  push @choices, 
    TR({-class => 'searchbody'},
      th  ({-align=>'CENTER',-width=>'50%'},"Choose an \"original\"",  
        th  ({-align=>'CENTER'}, "Choose a \"variant\"")));
        
  push @choices, 
    TR({-class => 'searchtitle'},  
      td( {-align=>'CENTER', -width=>'50%'}, 
        popup_menu(
          -name     => $self->config_name('FIRST'), 
          -values   => \@srcs,
          -default  => $sourcename), 
        td({-align=>'CENTER'}, 
          popup_menu(
            -name     => $self->config_name('SECOND'), 
            -values   => \@srcs, 
            -default  => $sourcename))));
        
  my $html= table(@choices);
  $html;
}


sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for ChromosomeDiff job $sid")
      : start_html(-title => "Running ChromosomeDiff job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '60'}));
    print p(i("This page will refresh in 1 minute")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $command;
    $pa->{OUTPUT} = "html";
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);
    
   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_ChromosomeDiff;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title  => "Launching BioStudio...", 
        -head   => meta({
          -http_equiv => 'refresh', 
          -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_ChromosomeDiff.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute ChromosomeDiff: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'ChromosomeDiff',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__