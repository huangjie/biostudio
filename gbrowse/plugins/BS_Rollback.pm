package Bio::Graphics::Browser2::Plugin::BS_Rollback;

use Bio::Graphics::Browser2::Plugin;
use GeneDesign::Basic;
use BioStudio::Basic;
use BioStudio::GBrowse qw(get_gbrowse_src_list);
use CGI::Carp qw/fatalsToBrowser/;
use CGI qw(:standard *table);

use strict;
use warnings;

use vars qw($VERSION @ISA);
$VERSION = '1.00';
@ISA = qw(Bio::Graphics::Browser2::Plugin);

##Global variables 
my $BS;
my $bsversion = "BS_PCR_Tagger_$VERSION";
$| = 1;

sub name { "BioStudio: Rollback" }
sub type { 'dumper'}
sub verb { ' '  }

sub description 
{
  p("Delete versions of the chromosome (currently irreversible).");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("/Users/Shared/BioStudio");
}

sub mime_type 
{
  return 'text/html';
}

sub reconfigure 
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub config_defaults 
{
  my $self = shift;
  return;
}


sub configure_form 
{
  if ($BS->{server_permissions} != 1)
  {
    return p("This server is not authorized to edit chromosomes; this plugin will not run.<br>");
  }
  my $self = shift;
  my $curr_config = $self->configuration;
  my $gb_settings = $self->page_settings;
  my $sourcename  = $$gb_settings{source};
  my @srcs        = get_gbrowse_src_list($BS);
  
  my @choices;
  push @choices, TR({-class => 'searchbody'},
          th  ({-align=>'CENTER',-width=>'50%'},"Which version do you want to rollback?")  );
  push @choices, TR({-class => 'searchtitle'},  
          td  ( {-align=>'CENTER', -width=>'50%'}, 
            popup_menu(-name => $self->config_name('DROPPER'), -values =>\@srcs, -default => $sourcename)));
  my $html= table(@choices);
  $html;
}

sub dump 
{
  my $self    = shift;
  my $gb_settings = $self->page_settings;
  my $sourcename  = $$gb_settings{source};
  my $pa          = $self->configuration;

  print start_html($self->name);
  if ($BS->{server_permissions} != 1)
  {
    return p("This server is not authorized to edit chromosomes; this plugin will not run.<br>");
  }
  
  my ($species, $chrname, $genver, $chrver) = ($1, $2, $3, $4) if ($$pa{DROPPER} =~ $VERNAME);
  $$pa{FILE}        = "$BS->{genome_repository}/$species/chr$chrname/$$pa{DROPPER}.gff";
  
  if ($chrver + $genver == 0)
  {
    return p("$$pa{DROPPER} is wildtype; you may not delete the wildtype version. <br>");
  }
  
  rollback_gbrowse($BS, $pa);
  print p("$$pa{DROPPER} has been rolled back.");
}

1;
__END__