package Bio::Graphics::Browser2::Plugin::BS_RELocalMarkup;

use Bio::Graphics::Browser2::Plugin;
use GeneDesign::Basic;
use BioStudio::Basic;
use CGI qw(:standard *table);

use strict;
use warnings;

use vars '$VERSION','@ISA';
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '2.00';
my ($RE_DATA, $BS, $xlationref);
$| = 1;

sub name { "BioStudio: Annotate and Edit Potential Restriction Sites" }
sub type { 'annotator' }
sub verb {  ' '  }
sub description 
{
  p("The restriction site plugin generates a potential restriction map on the
     current view, which can then be used to add, modify, or remove restriction 
     enzyme recognition sites. This plugin will not run on ranges bigger than 10kb.
     This plugin will only show enzymes that can be made unique or absent in the
     current range. Do not use this plugin as a general surveyor of restriction
     enzyme recognition sites - use the Restriction Site Annotator instead.");
}

sub init 
{
  my $self = shift;
  $BS   = configure_BioStudio("/Users/Shared/BioStudio");
  $RE_DATA = define_sites("$BS->{enzyme_dir}/standard_and_IIB");
}

sub config_defaults 
{
  my $self = shift;
  return {padder => 0, on => 1, ilimit => 10, "non palindromic" => 1};
}

sub reconfigure
{
  my $self = shift;
  my $config = $self->configuration;
  %$config = map {$_=>1} $self->config_param('kind');
  $config->{on} = $self->config_param('on');
  $config->{ilimit} = $self->config_param('ilimit');
  $config->{padder} = $self->config_param('padder');
}

sub configure_form 
{
  my $self = shift;
  my $config = $self->configuration;
  my $gb_settings = $self->page_settings;
  my $start = $gb_settings->{view_start};
  my $stop  = $gb_settings->{view_stop};
  
  if ($stop - $start + 1 > 10000)
  {
    return p("This plugin does not process sequences longer than 10kb.<br>");
  }  
  return table(
    TR({-class=>'searchtitle'}, 
      th("Select Restriction Sites to Annotate")),
    TR({-class=>'searchbody'}, 
      th({-align=>'LEFT'}, "Display: ", 
        radio_group(
          -name     => $self->config_name('on'), 
          -values   => [0, 1], 
          -labels   => {0=> 'off', 1=>'on'},
          -default  => $config->{on}, 
          -override => 1))),
    TR({-class=>'searchbody'}, 
      th({-align=>'LEFT'}, "Which Types of Enzyme? ",
        checkbox_group(
          -name    => $self->config_name('kind'), 
          -values   => ['non palindromic', 'palindromic', 'blunt'],
          -defaults => [grep {$config->{$_}} keys %$config]))),
    TR({-class=>'searchbody'}, 
      th({-align=>'LEFT'}, "Removal limit (Maximum number of removals to make an enzyme eligible for uniquing)",
        textfield(
          -name      => $self->config_name('ilimit'), 
          -default   => $config->{ilimit}, 
          -size      => 4, 
          -maxlength => 3,    
          -override => 1))),
    TR({-class=>'searchbody'},
      th({-align=>'LEFT'},"Annotate padding? (Search the scrollable sequence outside the current view)",
        radio_group(
          -name     => $self->config_name('padder'),
          -values   => [ 0, 1 ],
          -labels   => { 0 => 'no', 1 => 'yes' },
          -default  => $config->{padder},    
          -override => 1 ))));
}

sub annotate 
{
  my $self    = shift;
  my $segment = shift;
  my $config        = $self->configuration;
#  return unless ($config->{on});
  my $gbrowse_settings = $self->page_settings;
  my $sourcename  = $$gbrowse_settings{source};
  my $sp = $1 if ($sourcename =~ $VERNAME);
  my $CODON_TABLE = define_codon_table($SPECIES{$sp});

# Get DNA and features          
  my $abs_start = $segment->start;
  my $abs_stop  = $segment->stop;
  my $dna       = $segment->seq->seq;  
  unless ($config->{padder})
  {
    my $offset = $gbrowse_settings->{view_start} - $abs_start;
    $abs_start = $gbrowse_settings->{view_start};
    $abs_stop  = $gbrowse_settings->{view_stop};
    $dna = substr($dna, $offset - 1, $abs_stop - $abs_start + 1);
  }
  return if ($abs_stop - $abs_start + 1 > 10000);
  
  my $feat_list = $self->new_feature_list(-smart_features => 1);
    
  my (%seeking, %lookout, %newfeats);
  my %palstat = (1 => "np", 0 => "p", -1 => "b");       

  for my $type (keys %$config)
  {
    $seeking{"1"}++ if ($type eq 'non palindromic');
    $seeking{"0"}++ if ($type eq 'palindromic');
    $seeking{"-1"}++ if ($type eq 'blunt');
  }    
    
#Mask exons and introns for easy lookup
  my @CDSes     = $segment->features(-types => 'CDS', -start => $abs_start, -end => $abs_stop, -range_type => "overlaps");
  my $emaskstr  = make_mask(length($dna), \@CDSes, $abs_start);
  my @eMASK     = split("", $emaskstr);
  my @introns   = $segment->features(-types => 'intron', -start => $abs_start, -end => $abs_stop, -range_type => "overlaps");
  my $imaskstr  = make_mask(length($dna), \@introns, $abs_start);
  my @iMASK     = split("", $imaskstr);

# For every present site, determine exonic state and overhang status
  my $SITE_STATUS = define_site_status($dna, $$RE_DATA{REGEX});
  my $locations = {};
  my $elocs = {};
  
  foreach my $enz (grep {$$SITE_STATUS{$_} <= $config->{ilimit}} keys %{$SITE_STATUS})
  {
#    print STDERR "Processing $enz...\n";
    $$locations{$enz} = [];
    $$elocs{$enz} = {};  
    my $repos = siteseeker($dna, $$RE_DATA{CLEAN}->{$enz}, $$RE_DATA{REGEX}->{$enz});
    my ($olapcount, $igencount) = (0, 0);
    my $regforw = regres($$RE_DATA{CLEAN}->{$enz}, 1);
    foreach my $pos (keys %$repos)
    {
      my $convstart = ($pos + $abs_start);
      my $orient = $$repos{$pos} =~ $regforw  ?  "+"  :  "-";
      my ($eflag, $iflag, $palflag)     = ($eMASK[$pos - 1], $iMASK[$pos - 1], -1);
      if ($$RE_DATA{TYPE}->{$enz} !~ /b/)
      {
        my $mattersbit = overhang($dna, $pos, $$repos{$pos}, $$RE_DATA{TABLE}->{$enz}, $$RE_DATA{CLEAN}->{$enz});
        $palflag = $mattersbit ne complement($mattersbit, 1)  ?  1  :  0;
      }
      push @{$$locations{$enz}}, [$convstart, $palflag, $eflag, $iflag, $orient];
      $olapcount++ if (abs($eflag) > 1 || ($eflag > 0 && $iflag > 0));
      $igencount++ if (($eflag == 0 && $iflag == 0) || ($eflag == 0 && $iflag > 0));     
    }
    next if ($igencount > 1 || $olapcount > 1);  #too many intergenic sites or overlaps
    next if ($igencount == 1 && $olapcount == 1);
    my @exonicsites = grep {$_->[2] == 1 && $_->[3] == 0} @{$$locations{$enz}};
    my %ehash = map {$enz . "." . $palstat{$_->[1]} . "." . $_->[0] => $_} @exonicsites;
    $$elocs{$enz} = \%ehash;

  # Admire all sites that only have one intergenic instance, or one instance in an overlap
    if ($igencount == 1 || $olapcount == 1)
    {
      my @sites = grep {($_->[2] == 0 && $_->[3] == 0) 
                      || ($_->[2] > 1) || ($_->[2] > 0 && $_->[3] > 0)} @{$$locations{$enz}};     
      my $site = $sites[0];
      next unless (exists $seeking{$site->[1]});
      my @exonnotes = keys %ehash;
      my $necc = {Track => "Intergenic_Sites", start => $site->[0]};
      $$necc{end} = $$necc{start} + length($$RE_DATA{CLEAN}->{$enz})-1;
      my $attr = {actions => ["Make_unique"], exonic_sites => \@exonnotes, 
        pal => [$site->[1]], enz => [$enz]};
      my $name = $enz . "." . $palstat{$site->[1]} . "." . $$necc{start};
      $$necc{attr} = $attr;
      $newfeats{$name} = $necc;
      foreach my $site (@exonicsites)
      {
        my $necc = {Track => "Exonic_Sites_IR", start => $site->[0]};
        $$necc{end} = $$necc{start} + length($$RE_DATA{CLEAN}->{$enz})-1;
        my $attr = {actions => ["remove"], enz => [$enz]};
        my $name = $enz . "." . $palstat{$site->[1]} . "." . $$necc{start};
        $$necc{attr} = $attr;
        $newfeats{$name} = $necc;       
      }
    }    
  # Admire all sites that occur exclusively exonically
    elsif ($igencount == 0 && $olapcount == 0)
    {
      $lookout{$enz}++;
      foreach my $site (@exonicsites)
      {
        my @exonnotes = grep {$ehash{$_}->[0] ne $site->[0]} keys %ehash;
        my $necc = {Track => "Exonic_Sites", start => $site->[0]};
        $$necc{end} = $$necc{start} + length($$RE_DATA{CLEAN}->{$enz})-1;
        my $attr = {actions => ["remove"], exonic_sites => \@exonnotes,
          pal => [$site->[1]], enz => [$enz]};
        my $name = $enz . "." . $palstat{$site->[1]} . "." . $$necc{start};
        push @{$$attr{actions}}, "make_unique" if (exists $seeking{$site->[1]});
        $$necc{attr} = $attr;
        $newfeats{$name} = $necc;
      }
    }
  }
  #scan ORFs for Silent RE sites of all completely absent sites or sites that occur exclusively exonically
  my @seeklist = grep {$$SITE_STATUS{$_} == 0 || exists $lookout{$_}} keys %{$SITE_STATUS};
  my $trees = build_suffix_tree(\@seeklist, $RE_DATA, $CODON_TABLE, $xlationref);

  my $silenthits = search_suffix_tree($trees, \@CDSes, $RE_DATA, $CODON_TABLE, $xlationref);
  foreach my $hit (@$silenthits)
  {
    my ($enz, $pos, $presence, $ohangs, $exon, $pproof, $ohangstart, $strand) = @$hit;
    next unless ($pos <= $abs_stop && $pos >= $abs_start);
    next unless ($eMASK[$pos - ($abs_start + 1)] == 1);
    my $zstrand =  $strand == 2 || $strand == -1  ? -1  : 1;
    my $attr = {ingene => [$exon->name], peptide => [$pproof], 
      strand => [$zstrand], ohangoffset => [$ohangstart], presence => [$presence]};
    my %ehsh = %{$$elocs{$enz}};
    my @exonnotes = keys %ehsh;
    if (scalar(@exonnotes))
    {
      $$attr{actions} = ["make_unique"];
      $$attr{exonic_sites}  = \@exonnotes;
    }
  #Blunts (and double cutters?)
    if (scalar(keys %$ohangs) == 0 && exists $seeking{-1})
    {  
      next if ($presence ne "p");
      my $necc = {start => $pos, end => $pos + length($$RE_DATA{CLEAN}->{$enz})-1};
      my $name = $enz . "." . $palstat{-1} . "." . $pos;
      push @{$$attr{actions}}, "introduce";
      $$necc{Track} = "Introducible_Sites_B";
      $$necc{attr} = $attr;
      $newfeats{$name} = $necc;
    }
    elsif (scalar(keys %$ohangs))
    {
      my @npohangs = grep {$_ ne complement($_, 1)} keys %$ohangs;
      my @pohangs   = grep {$_ eq complement($_, 1)} keys %$ohangs;
      if (scalar(@pohangs) && exists $seeking{0})
      {
        my $necc = {start => $pos, end => $pos + length($$RE_DATA{CLEAN}->{$enz})-1};
        my %attc = %$attr;
        $attc{pal} = [0];
        my $name = $enz . "." . $palstat{0} . "." . $pos;
        if ($presence eq "p")
        {
          push @{$attc{actions}}, "introduce";
          $$necc{Track} = "Introducible_Sites_P";
          $attc{ohangs} = \@pohangs;
          $$necc{attr} = \%attc;      
          $newfeats{$name} = $necc;
        }
        else
        {
          if (exists($newfeats{$name}))
          {
            my $atte = $newfeats{$name}->{attr};
            push @{$$atte{actions}}, "edit_ohang";
            $$atte{ohangs} = \@pohangs;
          }
          else
          {
            $$necc{Track} = "Exonic_Sites";
            $attc{actions} = ["edit_ohang", "remove"];
            $attc{ohangs}  = \@pohangs;
            $$necc{attr} = \%attc;      
            $newfeats{$name} = $necc;
          }
        }
      }
      if (scalar(@npohangs) && exists $seeking{1})
      {
        my $necc = {start => $pos, end => $pos + length($$RE_DATA{CLEAN}->{$enz})-1};
        my %attc = %$attr;
        $attc{pal} = [1];
        my $name = $enz . "." . $palstat{1} . "." . $pos;
        if ($presence eq "p")
        {
          $$necc{Track} = "Introducible_Sites_NP";
          $attc{ohangs} = \@npohangs; 
          $$necc{attr} = \%attc;      
          $newfeats{$name} = $necc;  
        }
        else
        {
          if (exists($newfeats{$name}))
          {
            my $atte = $newfeats{$name}->{attr};
            push @{$$atte{actions}}, "edit_ohang";
            $$atte{ohangs} = \@npohangs;
          }
          else
          {
            $$necc{Track} = "Exonic_Sites";
            $attc{actions} = ["edit_ohang", "remove"];
            $attc{ohangs} = \@npohangs; 
            $$necc{attr} = \%attc;      
            $newfeats{$name} = $necc;
          }        
        }
      }
    }
  }

#  my $link = "";
  my $link = '?plugin=BS_REEditor;plugin_do=Configure;BS_REEditor.acton=$name'; 
  $feat_list->add_type(
    "Introducible_Sites_NP" =>  {
      key => "Exonic Sites (introducible, nonpalindromic)",
      fgcolor => "darkblue", 
      bgcolor => "darkblue", 
      link => $link });
  $feat_list->add_type(
    "Introducible_Sites_P" =>    {
      key => "Exonic Sites (introducible, palindromic)",
      fgcolor => "blue",
      bgcolor => "darkblue",
      link => $link });
  $feat_list->add_type(
    "Introducible_Sites_B" =>    {
      key => "Exonic Sites (introducible, blunt or double cutting)",
      fgcolor => "lightblue", 
      bgcolor => "darkblue", 
      link => $link });    
  $feat_list->add_type(
    "Intergenic_Sites" => {
      key => "Intergenic Sites (uniqueable)",
      fgcolor => "darkred", 
      bgcolor => "darkred", 
      link => $link });
  $feat_list->add_type(
    "Exonic_Sites" => {
      key => "Exonic Sites (removable, uniqueable, possibly manipulable)",
      fgcolor => "darkgreen", 
      bgcolor => "darkgreen", 
      link => $link });
  $feat_list->add_type(
    "Exonic_Sites_IR" => {
      key => "Exonic Sites (to be removed to make Intergenic sites unique)",
      fgcolor => "red", 
      bgcolor => "red", 
      link => $link });
    
  print STDERR "Adding ", scalar(keys %newfeats), " features to panel...\n";  
  foreach my $featname (keys %newfeats)
  {    
    my $attr = $newfeats{$featname};
    my $feature = Bio::Graphics::Feature->new(
      -start        =>  $$attr{start},  
      -end          =>  $$attr{end},  
      -seq_id       =>  $segment->seq_id,  
      -name         =>  $featname,
      -primary_tag  =>  $$attr{Track},
      -attributes   =>  $$attr{attr},
      -source       =>  'BS_RELocalMarkup.pm');
    $feat_list->add_feature( $feature );
  }
  

  return $feat_list;
}
1;
__END__