package Bio::Graphics::Browser2::Plugin::BS_REAnnotator;

use Bio::Graphics::Browser2::Plugin;
use GeneDesign::Basic;
use BioStudio::Basic;
use CGI::Carp qw/fatalsToBrowser/;
use CGI qw(:standard *table);

use strict;
use warnings;

use vars '$VERSION','@ISA';
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '2.00';
my $BS;
my $RE_DATA;
my %palstat;

sub name { "BioStudio: Annotate Restriction Sites" }
sub type { 'annotator' }
sub verb { ' ' }

sub description 
{
  p("The restriction site plugin generates a restriction map, differentiating 
  between enzymes that have unique recognition sites in the current range, 
  enzymes that will leave palindromic, non-palindromic, or blunt overhangs, and 
  enzymes that occur in exons, in introns, and in intergenic regions.");
}

sub init 
{
  my $self = shift;
  $BS   = configure_BioStudio("/Users/Shared/BioStudio");
  $RE_DATA = define_sites("$BS->{enzyme_dir}/standard_and_IIB");
  %palstat = (1 => "np", 0 => "p", -1 => "b");
}

sub config_defaults 
{
  my $self = shift;
  return {padder => 1, on => 1};
}

sub reconfigure
{
  my $self = shift;
  my $current_config = $self->configuration;
  %$current_config = map {$_=>1} $self->config_param('enzyme');
  $current_config->{on} = $self->config_param('on');
  $current_config->{uniquer} = $self->config_param('uniquer');
  $current_config->{nper} = $self->config_param('nper');
  $current_config->{padder} = $self->config_param('padder');
}

sub configure_form 
{
  my $self = shift;
  my $current_config = $self->configuration;
  my @buttons = checkbox_group(
    -name     => $self->config_name('enzyme'),
    -values   => [sort keys %{$$RE_DATA{CLEAN}}],
    -cols     => 15,
    -defaults => [grep {$current_config->{$_}} keys %$current_config]);
  return table(
    TR({-class=>'searchtitle'},
      th("Select Restriction Sites To Annotate")),
    TR({-class=>'searchtitle'},
      th({-align=>'LEFT'},  "Restriction Site Display : ",
        radio_group(
          -name     => $self->config_name('on'),
          -values   => [ 0, 1 ],
          -labels   => { 0 => 'off', 1 => 'on' },
          -default  => $current_config->{on},
          -override => 1 ))),
    TR({-class=>'searchtitle'},
      th({-align=>'LEFT'},"Output separate track of unique sites? ",
        radio_group(
          -name     => $self->config_name('uniquer'),
          -values   => [ 0, 1 ],
          -labels   => { 0 => 'off', 1 => 'on' },
          -default  => $current_config->{uniquer},    
          -override => 1 ))),
    TR({-class=>'searchtitle'},
      th({-align=>'LEFT'},"Only show sites with nonpalindromic overhangs? ",
        radio_group(
          -name     => $self->config_name('nper'),
          -values   => [ 0, 1 ],
          -labels   => { 0 => 'off', 1 => 'on' },
          -default  => $current_config->{nper},    
          -override => 1 ))),
    TR({-class=>'searchtitle'},
      th({-align=>'LEFT'},"Annotate padding?",
        radio_group(
          -name     => $self->config_name('padder'),
          -values   => [ 0, 1 ],
          -labels   => { 0 => 'off', 1 => 'on' },
          -default  => $current_config->{padder},    
          -override => 1 ))),
    TR({-class=>'searchbody'},
      td(@buttons)));
}

sub annotate 
{
  my $self        = shift;
  my $segment     = shift;
  my $config      = $self->configuration;
  return unless ($config->{on});
  my $gb_settings = $self->page_settings;
  my $feat_list   = $self->new_feature_list;
  my $ref         = $segment->seq_id;
  my $abs_start = $segment->start;
  my $dna = $segment->seq->seq;
  unless ($config->{padder})
  {
    my $offset = $gb_settings->{view_start} - $abs_start;
    $abs_start = $gb_settings->{view_start};
    $dna = substr($dna, $offset, $gb_settings->{view_stop} - $gb_settings->{view_start} + 1);
  }
  
  my $SITE_STATUS = define_site_status($dna, $$RE_DATA{REGEX});
  my %enzhsh = map {$_ => 1} keys %{$$RE_DATA{CLEAN}};
  my %lookfornu = map {$_ => 1} grep {exists $enzhsh{$_}} keys %$config;
  
#Mask introns and exons exons across a string.
  my @CDSes     = $segment->features(-types => 'CDS');
  my $emaskstr  = make_mask(length($dna), \@CDSes, $abs_start);
  my @eMASK     = split("", $emaskstr);
  my @introns   = $segment->features(-types => 'intron');
  my $imaskstr  = make_mask(length($dna), \@introns, $abs_start);
  my @iMASK     = split("", $imaskstr);
    
#sites present in this segment: are they intronic, exonic, or intergenic? Are they palindromic or non palindromic overhangs?
  foreach my $enz (keys %lookfornu, grep {$$SITE_STATUS{$_} == 1 && ! exists $lookfornu{$_}} keys %$SITE_STATUS)
  {
    my $sitehash = siteseeker($dna, $$RE_DATA{CLEAN}->{$enz}, $$RE_DATA{REGEX}->{$enz});
    foreach my $pos (keys %$sitehash)
    {
      my $convstart  = ($pos + $abs_start);
      my $exonmask   = $eMASK[$pos - 1];  
      my $intronmask = $iMASK[$pos - 1];
      my $palflag = -1; #default, or blunt
      if ($$RE_DATA{TYPE}->{$enz} !~ /b/)
      {
        #my ($dna, $pos, $grabbedseq, $table, $clean, $swit) = @_;
        my $mattersbit = overhang($dna, $pos, $$sitehash{$pos}, $$RE_DATA{TABLE}->{$enz}, $$RE_DATA{CLEAN}->{$enz});
        $palflag = ($mattersbit ne complement($mattersbit, 1))  ?  1  :  0;
      }
      next if ($config->{nper} && $palflag != 1);
      create_annotation($ref, $feat_list, $enz, $convstart, "bs_Unique_Sites", "u", $palflag) if ($config->{uniquer} && $$SITE_STATUS{$enz} == 1);
      if (exists ($lookfornu{$enz}))
      {
        create_annotation($ref, $feat_list, $enz, $convstart, "bs_Intronic_Sites", "n", $palflag)       if ($intronmask > 0);                     #Intronic Sites
        create_annotation($ref, $feat_list, $enz, $convstart, "bs_Sites_in_an_Overlap", "e", $palflag)  if ($exonmask > 1 && $intronmask == 0);   #Sites in an Overlap
        create_annotation($ref, $feat_list, $enz, $convstart, "bs_Intergenic_Sites", "i", $palflag)     if ($exonmask == 0 && $intronmask == 0);  #Intergenic Sites
        create_annotation($ref, $feat_list, $enz, $convstart, "bs_Exonic_Sites", "e", $palflag)         if ($exonmask == 1 && $intronmask == 0);  #Exonic Sites
      }
    }
  }
#  my $link = $BS->{server_permissions} ? '?q=' . $segment . ';plugin=BS_RestrictionSiteEditor;plugin_do=Configure;BS_RestrictionSiteEditor.acton=$name;BS_RestrictionSiteEditor.landmark=yes'  : undef;
  if ($config->{uniquer})
  {
    $feat_list->add_type("bs_Unique_Sites" => { 
      key => "BS Unique Sites", 
      fgcolor => "darkpurple",  
      bgcolor => "purple",  });
  #    link => $link});
  }
  $feat_list->add_type("bs_Intergenic_Sites" => { 
    key => "BS Intergenic Sites",  
    fgcolor => "darkred",    
    bgcolor => "red"});  
  $feat_list->add_type("bs_Exonic_Sites" => {
    key => "BS Exonic Sites",  
    fgcolor => "darkblue",    
    bgcolor => "blue"});
  $feat_list->add_type("bs_Intronic_Sites" => {
    key => "BS Intronic Sites",  
    fgcolor => "darkgrey",    
    bgcolor => "grey"});    
  $feat_list->add_type("bs_Sites_in_an_Overlap" => {
    key => "BS Sites in an Overlap",  
    fgcolor => "black",    
    bgcolor => "black"});    
      
  return $feat_list;
}

sub create_annotation
{
  my ($ref, $feat_list, $enz, $pos, $type, $code, $palflag) = @_;
  my $end = $pos + length($$RE_DATA{CLEAN}->{$enz})-1;
  my $feature = Bio::Graphics::Feature->new(
    -start =>  $pos,  
    -stop  =>  $end,  
    -seq_id  =>  $ref,  
    -name  =>  $enz . "." . $palstat{$palflag} . "." . $pos,    
    -primary_tag  =>  $type,
    -source  =>  'BS_REAnnotator.pm');
  $feat_list->add_feature($feature);
}

1;
__END__