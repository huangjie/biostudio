package Bio::Graphics::Browser2::Plugin::BS_ChromosomeAnalyzer;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw(&configure_BioStudio fetch_enzyme_lists);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
$VERSION = '1.50';
@ISA = qw(Bio::Graphics::Browser2::Plugin);

##Global variables 
my $BS;
my $bsversion = "BS_ChromosomeAnalyzer_$VERSION";
$| = 1;

sub name { "BioStudio: Analyze Chromosome" } 
sub type { 'dumper' }
sub verb { ' '  }

sub description 
{
  p("Get codon usage tables for genes, gene statistics, etc.");
}

sub init 
{
  my $self = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub mime_type 
{
    return 'text/html'
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub configure_form 
{
  my $self      = shift;
  my @enzlists = fetch_enzyme_lists($BS);
  my @choices;
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("Scope of analysis"),
        td(radio_group(
          -name    => $self->config_name('SCOPE'), 
          -values  => ['chrom', 'seg'], 
          -default => 'chrom',
          -labels  => {
            'chrom' => 'whole chromosome', 
            'seg' => 'this segment entirely within view'})));
            
  push @choices, 
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Look at protein-coding genes? ",  
        td(
          checkbox(
            -name     => $self->config_name('PCG'), 
            -value    => '1', 
            -label    => " ", 
            -checked  => 1))));
            
  push @choices,
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Look at non protein-coding genes? ",  
        td(
          checkbox(
            -name     => $self->config_name('NPCG'),
            -value    => '1',
            -label    => " ",
            -checked  => 1))));
            
  push @choices,
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Look at transposons and repeat features? ",  
        td(
          checkbox(
            -name     => $self->config_name('TR'),
            -value    => '1',
            -label    => " ",
            -checked  => 1))));

  push @choices, 
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'}, 
      "Look at other chromosome features? ",  
        td(
          checkbox(
            -name     => $self->config_name('CF'),
            -value    => '1',
            -label    => " ",
            -checked  => 1))));
            
  push @choices,
    TR({-class => 'searchtitle'},  
      th( {-align=>'RIGHT',-width=>'50%'},
      "Look at restriction enzymes? ",  
        td(
          checkbox(
            -name     => $self->config_name('RE'),
            -value    => '1',
            -label    => " ",
            -checked  => 0))));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Enzyme set"),
        td(
          popup_menu(
            -name     => $self->config_name('RESET'), 
            -values   => \@enzlists, 
            -default  => "standard_and_IIB")));            

  my $html = table(@choices);
  $html;
}


sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for ChromosomeAnalyzer job $sid")
      : start_html(-title => "Running ChromosomeAnalyzer job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '60'}));
    print p(i("This page will refresh in 1 minute")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{CHROMOSOME}   = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start if ($pa->{SCOPE} eq "seg");
    $pa->{STOPPOS}  = $segment->end if ($pa->{SCOPE} eq "seg");
    $pa->{OUTPUT} = "html";
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);

   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_ChromosomeAnalyzer;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title  => "Launching BioStudio...",
        -head   => meta({
          -http_equiv => 'refresh',
          -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_ChromosomeAnalyzer.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute ChromosomeAnalyzer: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'ChromosomeAnalyzer',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__