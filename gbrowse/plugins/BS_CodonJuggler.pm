package Bio::Graphics::Browser2::Plugin::BS_CodonJuggler;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw($VERNAME %SPECIES &configure_BioStudio);
use Bio::GeneDesign::Codons qw(:all);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_CodonJuggler_$VERSION";
$| = 1;
my $BS;

sub name { "BioStudio: Change Codon Usage" }
sub type { 'dumper' }
sub verb { ' '  }

sub description 
{
  p("Switch one codon for another, either across the whole region, or inside the
   gbrowse view.");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub mime_type 
{
  return 'text/html'
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub configure_form 
{
  my $self      = shift;
  #Check if overwrite warning is needed
  my $gbrowse_settings = $self->page_settings;
  my $source  = $$gbrowse_settings{source};
  my ($tSPECIES, $CHRNAME, $GVER, $CVER) = ($1, $2, $3, $4) 
      if ($source =~ $VERNAME);
  
  #Check if overwrite warning is needed
  my $nGVER = $GVER + 1;
  my $nCVER = $CVER + 1;
  $nCVER = "0" . $nCVER while (length($nCVER) < 2);
  my $gscale = $tSPECIES . "_chr" . $CHRNAME . "_" . $nGVER . "_" . $CVER;
  my $cscale = $tSPECIES . "_chr" . $CHRNAME . "_" . $GVER . "_" . $nCVER;
  my $scalewarns = "<br>"; 
  my $DBLIST = Bio::BioStudio::MySQL::list_databases($BS); 
  if (exists($DBLIST->{$gscale}))
  {
    my $gwarn  = "$gscale already exists; if you increment the genome version ";
       $gwarn .= "it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$gwarn</strong><br> ");
  }
  if (exists($DBLIST->{$cscale}))
  {
    my $cwarn  = "$cscale already exists; if you increment the chromosome ";
       $cwarn .= "version it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$cwarn.</strong><br> ");
  }
  
  #Make sure this is allowed
  if ($BS->{server_permissions} != 1)
  {
    return p("This server is not authorized to edit chromosomes.<br>");
  }
  
  my $CODON_TABLE = define_codon_table($SPECIES{$tSPECIES});
  my @codons      = sort keys %$CODON_TABLE;
  
  my @choices;
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Codon Juggler Configuration<br>"));
      
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Editor Name", 
        td(
          textfield(
            -name       => $self->config_name('EDITOR'), 
            -default    => $ENV{REMOTE_USER}, 
            -size       => 25, 
            -maxlength  => 20))));
            
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Notes", 
        td(
          textfield(
            -name => $self->config_name('MEMO'), 
            -size => 50))));
            
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Increment genome version or chromosome version?$scalewarns", 
        td(
          radio_group(
            -name     => $self->config_name('SCALE'), 
            -values   => ['genome', 'chrom'], 
            -labels   => {'chrom' => 'chromosome', 'genome' => 'genome'}, 
            -default  => 'genome'))));
            
  push @choices,
    TR({-class => 'searchbody'},
      th("Codon replacement",
        td("replace all ",  
          popup_menu(
            -name   => $self->config_name('FROM'),
            -values => \@codons), 
          " codons with ", 
          popup_menu(
            -name   => $self->config_name('TO'), 
            -values => \@codons ), " codons" )));

  my $dlabel  = "allow non synonymous changes to dubious ORFs";
     $dlabel .= " on behalf of non-dubious ORFs";
  my $vlabel = "allow non synonymous changes to verified ORFs"; 
     $vlabel .= " on behalf of non-dubious ORFs";    
  push @choices,
    TR({-class => 'searchbody'},
      th("Overlapping ORF priority",
        td(
          checkbox(
            -name     => $self->config_name('DUBWHACK'), 
            -checked  => "checked", 
            -value    => "1", 
            -label    => $dlabel), 
          "<br>",
          checkbox(
            -name     => $self->config_name('VERWHACK'), 
            -value    => "1", 
            -label    => $vlabel))));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Scope of edit"),
        td(
          radio_group(
          -name     => $self->config_name('SCOPE'), 
          -values   => ['chrom', 'seg'], 
          -labels   => {
            'chrom' => 'whole chromosome', 
            'seg' => 'the genes contained in the current view'}, 
          -default  => 'chrom')));
          
  my $html = table(@choices);
  $html;
}

sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for CodonJuggler job $sid")
      : start_html(-title => "Running CodonJuggler job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '5'}));
    print p(i("This page will refresh in 5 seconds")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{OLDCHROMOSOME}   = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start;
    $pa->{STOPPOS}  = $segment->end;
    $pa->{OUTPUT}   = "html";

    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);
    
   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_CodonJuggler;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title => "Launching BioStudio...", 
        -head  => meta({
            -http_equiv => 'refresh', 
            -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_CodonJuggler.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute CodonJuggler: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'CodonJuggler',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__ 