package Bio::Graphics::Browser2::Plugin::BS_PCRTagDumper;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw(&configure_BioStudio);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_PCRTagDumper_$VERSION";
$| = 1;
my $BS;

sub name { "BioStudio: List PCRTag Sequences" }
sub type { 'dumper' }
sub verb { ' ' }

sub description 
{
  p("The PCRTag Dumper gives you the original wildtype sequence, original 
    synthetic sequence, and if applicable, the current synthetic sequence 
    of tags for every PCR Amplicon contained in the view.");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub mime_type 
{
  return 'text/html'
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub reconfigure 
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub configure_form 
{
  my $self        = shift;
  my @choices = ();
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Fetching PCR Tags<br>"));
      
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("Scope"),
        td(
          radio_group(
            -name     => $self->config_name('SCOPE'),
            -values   => ['chrom', 'seg'],
            -labels   => {
              'chrom' => 'whole chromosome', 
              'seg'   => 'the sequence in view now'},
            -default  => 'seg'))); 
            
  my $html = table(@choices);
  $html;
}

sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for PCRTagDumper job $sid")
      : start_html(-title => "Running PCRTagDumper job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '60'}));
    print p(i("This page will refresh in 1 minute")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{CHROMOSOME} = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start if ($pa->{SCOPE} eq "seg");
    $pa->{STOPPOS}  = $segment->end if ($pa->{SCOPE} eq "seg");
    $pa->{OUTPUT} = "html";
    
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);
    
   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_PCRTagDumper;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title  => "Launching BioStudio...", 
        -head   => meta({
          -http_equiv => 'refresh', 
          -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_PCRTagDumper.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute PCRTagDumper: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'PCRTagDumper',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__