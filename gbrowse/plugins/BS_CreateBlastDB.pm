package Bio::Graphics::Browser2::Plugin::BS_CreateBlastDB;
# $Id: CreateBlastDB.pm,v 1.1.14.1 2007-03-26 14:10:54 briano Exp $

use Bio::Graphics::Browser2::Plugin;
use Bio::Graphics::Feature;
use BioStudio::Basic;
use CGI qw(:standard *table);

use warnings;
use strict;

use vars '$VERSION','@ISA';
$VERSION = '0.15';

@ISA = qw(Bio::Graphics::Browser2::Plugin);

sub name { "BioStudio: Create a BLAST Database" }

sub description 
{
  p("This will dump this chromosome from its GFF database and create a BLAST database from it. ");
}

sub type { 'dumper' }
sub verb { ' '  }

sub mime_type 
{
    return "text/html";
}

sub init 
{
  my $self = shift;
  my $conf_dir = $self->config_path();
  my $browserstuff = $self->browser_config();
  my $sourcename = $browserstuff->source;
  my ($species, $chrom, $genver, $chrver) = ($1, $2, $3, $4) if ($sourcename =~ $VERNAME);
  my $file = $conf_dir . "/" . $GFFLOC ."/syn_" . $species . "/chr$chrom/" . $sourcename . ".gff";
  my $srchsh = {$sourcename => $file}; 
  die "Can't find GFF source $file\n" unless (-e $file); 
}

sub config_defaults 
{
  my $self = shift;
  return;
}

# we have no stable configuration
# sub reconfigure { }

sub configure_form 
{
  my $self = shift;
    return "<h2>nothing to configure</h2>"
}

sub dump 
{
  my $self = shift;
  my $conf_dir = $self->config_path();
  my $browserstuff = $self->browser_config();
  my $sourcename = $browserstuff->source;
  my ($species, $chrom, $genver, $chrver) = ($1, $2, $3, $4) if ($sourcename =~ $VERNAME);
  my $file = $conf_dir . "/" . $GFFLOC ."/syn_" . $species . "/chr$chrom/" . $sourcename . ".gff";
  my $srchsh = {$sourcename => $file}; 
  print "<h3>Making BLAST db...</h3>";
  my $result = make_BLAST_db($srchsh, $sourcename);
  print "<h3>Created $result</h3>";
  print "<a href = \"http://desdemona.ebalto.jhmi.edu/cgi-bin/gbrowse/$sourcename/\">Go Back</a><br><br>";
}

1;
