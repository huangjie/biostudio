package Bio::Graphics::Browser2::Plugin::BS_ChromosomeCutter;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw($VERNAME &configure_BioStudio fetch_custom_features);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeCutter_$VERSION";
$| = 1;
my $BS;

my $featdefault = " ( )";

sub name { "BioStudio: Delete from the Chromosome" }
sub type { 'dumper' }
sub verb { ' '  }

sub description 
{
  p("This is a chromosome editor.  It requires a selection be made in the 
    gbrowse view. Reference from the 5' end.");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub mime_type 
{
  return 'text/html';
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub configure_form 
{
  my $self        = shift;
  #Check if overwrite warning is needed
  my $gb_settings = $self->page_settings;
  my $source  = $$gb_settings{source};
  my ($SPECIES, $CHRNAME, $GVER, $CVER) = ($1, $2, $3, $4) 
    if ($source =~ $VERNAME);

  #Check if overwrite warning is needed
  my $nGVER = $GVER + 1;
  my $nCVER = $CVER + 1;
  $nCVER = "0" . $nCVER while (length($nCVER) < 2);
  my $gscale = $SPECIES . "_chr" . $CHRNAME . "_" . $nGVER . "_" . $CVER;
  my $cscale = $SPECIES . "_chr" . $CHRNAME . "_" . $GVER . "_" . $nCVER;
  my $scalewarns = "<br>";
  my $DBLIST = Bio::BioStudio::MySQL::list_databases($BS);
  if (exists($DBLIST->{$gscale}))
  {
    my $gwarn  = "$gscale already exists; if you increment the genome version ";
       $gwarn .= "it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$gwarn</strong><br> ");
  }
  if (exists($DBLIST->{$cscale}))
  {
    my $cwarn  = "$cscale already exists; if you increment the chromosome ";
       $cwarn .= "version it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$cwarn.</strong><br> ");
  }

  #Make sure this is allowed
  if ($BS->{server_permissions} != 1)
  {
    return p("This server is not authorized to edit chromosomes.<br>");
  }
  my $db    = $self->db_search;
  my $start = $gb_settings->{view_start};
  my $stop  = $gb_settings->{view_stop};
  my @a = $db->features(-range=>"contains");
  
  my @flankers = grep {($_->start < $start && ($_->end <= $stop && $_->end >= $start)) 
                    || ($_->end > $stop && ($_->start >= $start && $_->start <= $stop))} @a;
  @flankers = map {$_->primary_tag . " " . $_->Tag_load_id . "<br>"} @flankers;
                  
  my %FEATTYPES = ();
  my @contained = grep {$_->start >= $start && $_->end <= $stop} @a;
  $FEATTYPES{$_->primary_tag}++ foreach (@contained);
  my @featcounts = map { $FEATTYPES{$_} . " " . $_  . "<br>"} sort keys %FEATTYPES;
  my @featkeys = sort {$a cmp $b} keys %FEATTYPES;
  unshift @featkeys, $featdefault;
  
  my $BS_FEATS = fetch_custom_features($BS);
  my @BSKINDS = map {"<strong>" . $_->{NAME} . "</strong> " . $_->{KIND}  . "<br>"} values %$BS_FEATS;
  @BSKINDS = sort {$a cmp $b} @BSKINDS;
  my @inskeys = sort {$a cmp $b} map {$_->{NAME}} values %$BS_FEATS;
  unshift @inskeys, $featdefault;
  
  my %DELHASH;

  my $popupseqdelbsfeat = popup_menu(
    -name     => $self->config_name("seqdel.INSERT"), 
    -values   => \@inskeys, 
    -default  => $featdefault);
  $DELHASH{'seqdel'} = "delete this segment (and replace with a $popupseqdelbsfeat)<br>";
  
  $DELHASH{'seqdelprp'} = "propose this segment for deletion<br>";

  my $popupfeatdel = popup_menu(
    -name     => $self->config_name("featdel.FEATURE"),
    -values   => \@featkeys,
    -default  => $featdefault);
  my $popupfeatdelbsfeat = popup_menu(
    -name     => $self->config_name("featdel.INSERT"),
    -values   => \@inskeys,
    -default  => $featdefault);
  $DELHASH{'featdel'} = "delete the $popupfeatdel features wholly contained within this segment (and replace with $popupfeatdelbsfeat)<br>";
  
  my $popupfeatdelprp = popup_menu(
    -name     => $self->config_name("featdelprp.FEATURE"),
    -values   => \@featkeys,
    -default  => $featdefault);
  $DELHASH{'featdelprp'} = "propose the $popupfeatdelprp features wholly contained within this segment for deletion<br>";

  my @choices = ();
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Editing Chromosome Features<br>"));
      
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Editor Name", 
        td(
          textfield(
            -name       => $self->config_name('EDITOR'), 
            -default    => $ENV{REMOTE_USER},
            -size       => 25,
            -maxlength  => 20))));
            
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Notes", 
        td(
          textfield(
            -name => $self->config_name('MEMO'),
            -size => 50))));

  push @choices,
    TR({-class => 'searchtitle'}, 
      th("Increment genome version or chromosome version?$scalewarns", 
        td(
          radio_group(
            -name     => $self->config_name('SCALE'),
            -values   => ['genome', 'chrom'],
            -labels   => {
              'chrom'   => 'chromosome version',
              'genome'  => 'genome version'},
            -default  => 'chrom'))));
            
  autoEscape(0);
  
  push @choices,
    TR({-class=>'searchbody'}, 
      th({-align=>'RIGHT',-width=>'25%'}, "DELETION OPTIONS:",
        td("<br>", 
          radio_group(
            -name   => $self->config_name('ACTION'),
            -values => \%DELHASH), "<br><br>"))); 

  if (scalar(@flankers))             
  {
    push @choices, 
      TR({-class => 'searchtitle'},
        th({-align=>'RIGHT',-width=>'25%'}, 
        scalar(@flankers) . " features are not fully contained in this view ", 
          td("<br>@flankers<br>")));
  }        
  
  push @choices, 
    TR({-class => 'searchtitle'},
      th({-align=>'RIGHT',-width=>'25%'}, "Feature counts in this segment:",
        td(make_table(\@featcounts, 5))));
        
  push @choices,
    TR({-class => 'searchtitle'},
      th({-align=>'RIGHT',-width=>'25%'}, "Custom features available:",
        td(make_table(\@BSKINDS, 3))));
        
  my $html = table(@choices, 2);
  autoEscape(1);
  $html;
}

sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for ChromosomeCutter job $sid")
      : start_html(-title => "Running ChromosomeCutter job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '5'}));
    print p(i("This page will refresh in 5 seconds")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{OLDCHROMOSOME}   = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start;
    $pa->{STOPPOS}  = $segment->end;
    $pa->{OUTPUT}   = "html";
    my $action = $pa->{ACTION};
    foreach my $key (grep {$_ =~ /\./} keys %$pa)
    {
      if ($pa->{$key} ne $featdefault)
      {
        $pa->{$1} = $pa->{$key} if ($key =~ /\.(\w+)/);
      }
      delete $pa->{$key};
    }  
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);

   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_ChromosomeCutter;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title => "Launching BioStudio...", 
        -head  => meta({
          -http_equiv  => 'refresh', 
          -content     => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_ChromosomeCutter.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute ChromsomeCutter: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'ChromosomeCutter',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

sub make_table
{
  my ($arlist, $colcount) = @_;
  my ($x, $y) = (0, $colcount-1);
  my @table;
  while ($x < scalar(@$arlist))
  {
    my @slice = @$arlist[$x..$y];
    push @table, TR(td(\@slice));
    $x += $colcount;
    $y += $colcount;    
  }
  return table(@table);
}
1;
__END__