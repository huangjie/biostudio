package Bio::Graphics::Browser2::Plugin::BS_ChromosomeSegmentationPlanner;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::GBrowse qw(get_gbrowse_src_list);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeSegmentationPlanner_$VERSION";
$| = 1;
my $BS;

sub name { "BioStudio: Propose a Chromosome Segmentation Plan" }
sub type { 'dumper' }
sub verb { ' ' }

sub description 
{
  p("This plugin will propose a plan for the segmentation of a chromosome into
    modular, assemblable pieces by the placement of restriction enzyme
    recognition sites.");
}

sub init 
{
  my $self = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub config_defaults 
{
  my $self = shift;
  return;
}

sub mime_type 
{
  return 'text/html';
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    if ($_ eq "MARKERS")
    {
      my @choices = $self->config_param($_);
      $current->{$_} = \@choices;
    }
    else
    {
      $current->{$_} = $self->config_param($_)
              ?  $self->config_param($_)
              :  undef;      
    }
  }
}

sub configure_form
{
  my $self        = shift;
  #Check if overwrite warning is needed
  my $gb_settings = $self->page_settings;
  my $source  = $$gb_settings{source};
  my ($SPECIES, $CHRNAME, $GVER, $CVER) = ($1, $2, $3, $4) 
    if ($source =~ $VERNAME);
  my @srcs = grep {$_ =~ /$SPECIES\_chr$CHRNAME/} get_gbrowse_src_list($BS);
  @srcs = sort grep {$_ ne $source} @srcs;
  my @enzlists = fetch_enzyme_lists($BS);
  
  #Check if overwrite warning is needed
  my $nGVER = $GVER + 1;
  my $nCVER = $CVER + 1;
  $nCVER = "0" . $nCVER while (length($nCVER) < 2);
  my $gscale = $SPECIES . "_chr" . $CHRNAME . "_" . $nGVER . "_" . $CVER;
  my $cscale = $SPECIES . "_chr" . $CHRNAME . "_" . $GVER . "_" . $nCVER;
  my $scalewarns = "<br>";  
  my $DBLIST = Bio::BioStudio::MySQL::list_databases($BS);
  if (exists($DBLIST->{$gscale}))
  {
    my $gwarn  = "$gscale already exists; if you increment the genome version ";
       $gwarn .= "it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$gwarn</strong><br> ");
  }
  if (exists($DBLIST->{$cscale}))
  {
    my $cwarn  = "$cscale already exists; if you increment the chromosome ";
       $cwarn .= "version it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$cwarn.</strong><br> ");
  }
  
  #Make sure this is allowed
  if ($BS->{server_permissions} != 1)
  {
    return p("This server is not authorized to edit chromosomes.<br>");
  }
  
  my $markers = fetch_custom_markers($BS);
  my @markerlist = sort keys %$markers;
  my @choices = ();
  
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Planning Chromosome Segmentation<br>"));
      
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("SEGMENTATION OPTIONS:"));
      
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("Chunk Size (bp)"),
      td("min", 
        textfield(
          -name => $self->config_name('CHUNKLENMIN'),
          -size => 6, 
          -maxlength  => 5, 
          -default    => 6000),
        "max", 
        textfield(
          -name       => $self->config_name('CHUNKLENMAX'),
          -size       => 6, 
          -maxlength  => 5, 
          -default    => 9920)));
          
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("Number of Chunks per Megachunk"),
        td(
          textfield(
            -name       => $self->config_name('CHUNKNUM'),    
            -size       => 2, 
            -maxlength  => 1, 
            -default    => 4),
          "min",
          textfield(
            -name       => $self->config_name('CHUNKNUMMIN'),
            -size       => 2,
            -maxlength  => 1,
            -default    => 3),
          "max", 
          textfield(
            -name       => $self->config_name('CHUNKNUMMAX'),
            -size       => 2,
            -maxlength  => 1,
            -default    => 5)));
         
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Chunks Ovelap by (bp)"),
        td(
          textfield(
            -name       => $self->config_name('CHUNKOLAP'),
            -size       => 3,
            -maxlength  => 2,
            -default    => 40)));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("ISS Size (bp)"),
        td("min", 
          textfield(
            -name       => $self->config_name('ISSMIN'),
            -size       => 5, 
            -maxlength  => 4, 
            -default    => 900),
          "max", 
          textfield(
            -name       => $self->config_name('ISSMAX'),
            -size       => 5, 
            -maxlength  => 4, 
            -default    => 1500)));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Original Chromosome for ISS substitution"),
        td(
          popup_menu(
            -name     => $self->config_name('WTCHR'), 
            -values   => \@srcs, 
            -default  => $srcs[0])));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Enzyme set"),
      td(
        popup_menu(
          -name     => $self->config_name('RESET'), 
          -values   => \@enzlists, 
          -default  => "nonpal_and_IIB")));
          
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Essential / fast growth gene \"UTR\" Padding"),
      td("5\'", 
        textfield(
          -name       => $self->config_name('FPUTRPADDING'),
          -size       => 5, 
          -maxlength  => 4, 
          -default    => 500),
       "3\'", 
       textfield(
          -name       => $self->config_name('TPUTRPADDING'),
          -size       => 5,
          -maxlength  => 4,
          -default    => 100)));
          
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Megachunk markers"),
        td(
          checkbox_group(
            -name     => $self->config_name('MARKERS'),
            -values   => \@markerlist,
            -cols     => 4,
            -defaults => ["LEU2", "URA3"])));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Last chromosomal marker"),
      td(
        radio_group(
          -name     => $self->config_name('LASTMARKER'),
          -values   => \@markerlist,
          -cols     => 4,
          -defaults => ["URA3"])));
          
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Scope"),
        td(
          radio_group(
            -name   => $self->config_name('SCOPE'),
            -values => ['chrom', 'seg'],
            -labels => {
              'chrom' => 'whole chromosome', 
              'seg'   => 'the sequence in view now'}, 
            -default=>'chrom'))); 
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("First Letter"),
        td(
          textfield(
            -name       => $self->config_name('FIRSTLETTER'),
            -size       => 3,
            -maxlength  => 2,
            -default    => "A")));
                      
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Force recreation of restriction enzyme database"),
        td(
          checkbox(
            -name     => $self->config_name('REDOREDB'),
            -checked  => 0,
            -value    => 1,
            -label    => "YES")));
                                  
  my $html = table(@choices);
  $html;
}

sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(
          -title => "Results for ChromosomeSegmentationPlanner job $sid")
      : start_html(
          -title => "Running ChromosomeSegmentationPlanner job $sid", 
          -head=>meta({
            -http_equiv => 'refresh', 
            -content    => '5'}));
    print p(i("This page will refresh in 5 seconds")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{OLDCHROMOSOME}   = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start if ($$pa{SCOPE} eq "seg");
    $pa->{STOPPOS}  = $segment->end if ($$pa{SCOPE} eq "seg");
    $pa->{MARKERS} = join(",", @{$pa->{MARKERS}}); 
    
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);
    
   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url();
      $addy .= "?plugin=BS_ChromosomeSegmentationPlanner;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title  => "Launching BioStudio...", 
        -head   => meta({
          -http_equiv => 'refresh', 
          -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre($command);
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_ChromosomeSegmentationPlanner.pl";
        open STDERR, ">&=1";
        exec "$path $command" 
            || die "Cannot execute ChromosomeSegmentationPlanner: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'ChromosomeSegmentationPlanner',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__