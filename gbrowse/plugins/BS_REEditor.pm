package Bio::Graphics::Browser2::Plugin::BS_REEditor;

use Bio::Graphics::Browser2::Plugin;
use GeneDesign::Basic;
use BioStudio::Basic;
use CGI::Carp qw/fatalsToBrowser/;
use CGI qw(:standard *table);

use strict;
use warnings;

use vars '$VERSION','@ISA';
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my ($RE_DATA, $BS);
my %EDITS = ("make_unique" => "Make this restriction site unique in range",
             "remove" => "Remove this restriction site",
             "introduce" => "Introduce this restriction site",
             "edit_ohang" => "Change the overhang left at this restriction site");
$| = 1;
  
sub name { "BioStudio: Edit Restriction Sites" }
sub type { 'dumper' }
sub verb { ' '    }
sub hide {1}
sub description 
{
  p(" Introduce and Remove restriction sites.");
}
  
sub init 
{
  $BS   = configure_BioStudio("/Users/Shared/BioStudio");
  $RE_DATA     = define_sites("$BS->{enzyme_file}");
}

sub config_defaults
{
  my $self = shift;
  return;
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  {  
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub configure_form
{ 
  if ($BS->{server_permissions} != 1)
  {
    print p("This server is not authorized to edit chromosomes; this plugin will not run.<br>");
    return;
  }
  my $self      = shift;
  my $pa        = $self->configuration;
  if (! $$pa{acton})
  {
    return table(
      TR({-class=>'searchtitle'}, 
        th("Choose a site from the Annotate and Edit track.")));
  }  
  my $segment   = shift;
  my $render    = $self->renderer->external_data;
  print STDERR keys %$render, "\n\n";
  my $feathsh   = $render->{"plugin:BS_RELocalMarkup"}->features;

  my @actfeat = grep {$_->name eq $pa->{acton}} @$feathsh;
  my $acton = $actfeat[0];
  my @choices = sort $acton->get_tag_values("actions");
  my %choihsh = map {$_ => 1} @choices;

  my @table;
  push @table, TR({-class=>'searchtitle'}, 
    th("Editing restriction enzyme recognition site " . $acton->name . "<br>"));
  push @table, TR({-class => 'searchtitle'}, 
    th("Editor Name", 
    td(textfield(-name=>$self->config_name('EDITOR'), -default=> $ENV{REMOTE_USER}, -size=> 25, -maxlength=> 20))));
  push @table, TR({-class => 'searchtitle'}, 
    th("Notes", 
    td(textfield(-name=>$self->config_name('MEMO'), -size=> 50))));
  push @table, TR({-class => 'searchtitle'}, 
    th("Scale of Edit", 
    td(radio_group(-name=> $self->config_name('SCALE'), -values=>['genome', 'chrom'], -labels=>{'chrom' => 'chromosome', 'genome' => 'genome'}, -default=>'chrom'))));
  push @table, TR({-class=>'searchtitle'}, 
    th("Options"));
  push @table, TR({-class=>'searchbody'}, 
    th("Action to Take:"),
    td(popup_menu(-name => $self->config_name('ACTIONCHOICE'), -values=>\@choices, -labels=>\%EDITS)));
  my ($introline, $uniqueline, $removeline);
  if (exists $choihsh{"introduce"})
  {
    my @genes = grep {$_->overlaps($acton)} $segment->features(-types => 'gene');
    $introline .= "a site at " . $acton->start . " will be added to an exon in " . $_->name . "<br>" foreach (@genes);
    push @table, TR({-class=>'searchbody'}, th("If introduced"), td([$introline]));
  }
  if (exists $choihsh{"make_unique"})
  { 
    my @exonsites = sort $acton->get_tag_values("exonic_sites") if ($acton->has_tag("exonic_sites"));
    foreach my $exon (@exonsites)
    {
      my $range = Bio::Range->new(-start=>$exon, -end=>$exon, -strand=>1);
      my @genes = grep {$_->overlaps($range)} $segment->features(-types => 'gene');
      $uniqueline .= "the site at $exon will be removed from an exon in " . $_->name . "<br>" foreach (@genes);
    }
    $uniqueline .= $introline if (exists $choihsh{"introduce"});
    push @table, TR({-class=>'searchbody'}, th("If made unique"), td([$uniqueline]));
  }
  if (exists $choihsh{"remove"})
  {
    my @genes = grep {$_->overlaps($acton)} $segment->features(-types => 'gene');
    $removeline .= "the site at " . $acton->start . " will be removed from an exon in " . $_->name . "<br>" foreach (@genes);
    push @table, TR({-class=>'searchbody'}, th("If removed"), td([$removeline]));
  }
  if (exists $choihsh{"edit_ohang"} || (exists $choihsh{"introduce"} && $acton->has_tag("ohangs")))
  {
    my @olaps = sort $acton->get_tag_values("ohangs");
    push @table, TR({-class=>'searchbody'}, 
      th("The overhang choices are"), 
      td(popup_menu(-name=> $self->config_name('OHANGCHOICE'),-values=>\@olaps)));
  } 
  my $html = table(@table);
  return $html;
}

sub mime_type 
{
  my $self  = shift;
  my $config  = $self->configuration;
  return 'text/html';
}  

sub dump 
{
  my $self      = shift;
  my $conf_dir    = $self->config_path();
  my $gbrowse_settings = $self->page_settings;
  my $sourcename  = $$gbrowse_settings{source};
  my $pa        = $self->configuration;
  my $segment      = $self->segments->[0];
  
  open (ERR, ">>../../../../Users/sarah/Public/Drop Box/ssfout.txt") || die "can't open test output, $!";       
  print ERR "Editor $$pa{editor}, working on $$pa{acton} ($$pa{memo})<br>";
  close ERR;
  $$pa{OLDCHROMOSOME}    = $sourcename;
  ($$pa{SPECIES}, $$pa{CHRNAME}, $$pa{GENVER}, $$pa{VERNAME}) = ($1, $2, $3, $4) if ($sourcename =~ $VERNAME);
  my $CODON_TABLE = define_codon_table($SPECIES{$$pa{SPECIES}});
  $$pa{RSCU_VALUES} = define_RSCU_values($SPECIES{$$pa{SPECIES}});
  
  $$pa{FILE}        = "$BS->{genome_repository}/syn_$$pa{SPECIES}/chr$$pa{CHRNAME}/$$pa{OLDCHROMOSOME}.gff";
  $$pa{VERNAME}++;
  $$pa{VERNAME} = "0" . $$pa{VERNAME} while (length($$pa{VERNAME}) < 2);
  $$pa{NEWCHROMOSOME}  = $$pa{SPECIES} . "_chr" . $$pa{CHRNAME} . "_" . $$pa{GENVER} . "_" . $$pa{VERNAME};

  $pa = edit($pa);
  return;  
  if ($$pa{NEWGFF})
  {
    print p("Creating new gff file...<br>");
    update_gbrowse($conf_dir, $pa);
  }
  else
  {
    print "Couldn't create new gff file...<br>";
  }
}

sub edit
{

}



1;
__END__