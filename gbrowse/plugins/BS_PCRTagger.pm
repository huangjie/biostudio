package Bio::Graphics::Browser2::Plugin::BS_PCRTagger;

use Bio::Graphics::Browser2::Plugin;
use Bio::BioStudio::Basic qw($VERNAME &configure_BioStudio);
use Bio::BioStudio::MySQL qw(list_databases);
use CGI qw(:all delete_all);
use Digest::MD5;
use Cache::FileCache;

use strict;
use warnings;

use vars qw($VERSION @ISA);
@ISA = qw(Bio::Graphics::Browser2::Plugin);
$VERSION = '1.00';
my $bsversion = "BS_PCR_Tagger_$VERSION";
$| = 1;
my $BS;

sub name { "BioStudio: Add PCR Tags" }
sub type { 'dumper' }
sub verb { ' '    }

sub description 
{
  p(" This plugin adds PCR Tags to exons.  A better description is coming.");
}

sub init 
{
  my $self     = shift;
  $BS   = configure_BioStudio("**CONFLOC**");
}

sub config_defaults
{
  my $self = shift;
  return;
}

sub reconfigure
{
  my $self  = shift;
  my $current = $self->configuration;
  foreach ( $self->config_param() ) 
  { 
    $current->{$_} = $self->config_param($_)
            ?  $self->config_param($_)
            :  undef;
  }
}

sub mime_type 
{
  my $self  = shift;
  my $config  = $self->configuration;
  return 'text/html';
}
 
sub configure_form
{
  my $self = shift;
  my $gbrowse_settings = $self->page_settings;
  my $source  = $$gbrowse_settings{source};
  my ($SPECIES, $CHRNAME, $GVER, $CVER) = ($1, $2, $3, $4) 
      if ($source =~ $VERNAME);
  
  #Check if overwrite warning is needed
  my $nGVER = $GVER + 1;
  my $nCVER = $CVER + 1;
  $nCVER = "0" . $nCVER while (length($nCVER) < 2);
  my $gscale = $SPECIES . "_chr" . $CHRNAME . "_" . $nGVER . "_" . $CVER;
  my $cscale = $SPECIES . "_chr" . $CHRNAME . "_" . $GVER . "_" . $nCVER;
  my $scalewarns = "<br>"; 
  my $DBLIST = list_databases($BS); 
  if (exists($DBLIST->{$gscale}))
  {
    my $gwarn  = "$gscale already exists; if you increment the genome version ";
       $gwarn .= "it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$gwarn</strong><br> ");
  }
  if (exists($DBLIST->{$cscale}))
  {
    my $cwarn  = "$cscale already exists; if you increment the chromosome ";
       $cwarn .= "version it will be overwritten.";
    $scalewarns .= p("<strong style=\"color:#FF0000;\">$cwarn.</strong><br> ");
  }
  
  #Make sure this is allowed
  if ($BS->{server_permissions} != 1)
  {
    print p("This server is not authorized to edit chromosomes.<br>");
    return;
  }
  
  my @choices;
  push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Adding PCR Tags<br>"));
      
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Editor Name", 
        td(textfield(
          -name       => $self->config_name('EDITOR'), 
          -default    => $ENV{REMOTE_USER}, 
          -size       => 25, 
          -maxlength  => 20))));
          
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Notes", 
        td(textfield(
          -name => $self->config_name('MEMO'),
          -size => 50))));
          
  push @choices, 
    TR({-class => 'searchtitle'}, 
      th("Increment genome version or chromosome version?$scalewarns", 
        td(radio_group(
          -name     => $self->config_name('SCALE'), 
          -values   => ['genome', 'chrom'],
          -labels   => {'chrom' => 'chromosome', 'genome' => 'genome'}, 
          -default  => 'genome'))));
          
   push @choices, 
    TR({-class=>'searchtitle'}, 
      th("Options"));
      
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("Tm range for pcr tags (&deg;C)"),
        td("min", 
          textfield(
            -name       => $self->config_name('MINTAGMELT'),
            -size       => 4,
            -maxlength  => 2, 
            -default    => 58),
          "max", 
          textfield(
            -name      => $self->config_name('MAXTAGMELT'),
            -size      => 4,
            -maxlength => 2,
            -default   => 60)));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("range for tag size (bp)"),
        td("min", 
          textfield(
            -name       => $self->config_name('MINTAGLEN'),
            -size       => 4, 
            -maxlength  => 3, 
            -default    => 19),
          "max", 
          textfield(
            -name      => $self->config_name('MAXTAGLEN'),
            -size      => 4, 
            -maxlength => 3, 
            -default   => 28)));
            
  push @choices, TR({-class=>'searchbody'}, 
    th("range for amplicon size (bp)"),
    td("min", 
      textfield(
        -name       => $self->config_name('MINAMPLEN'),
        -size       => 4,
        -maxlength  => 3,
        -default    => 200),
       "max", 
       textfield(
         -name      => $self->config_name('MAXAMPLEN'),
         -size      => 4,
         -maxlength => 3,
         -default   => 500)));
         
  push @choices,
    TR({-class=>'searchbody'}, 
      th("maximum amplicon overlap (%)"),
      td(
        textfield(
          -name       => $self->config_name('MAXAMPOLAP'),
          -size       => 4,
          -maxlength  => 2, 
          -default    => 25)));
          
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("minimum bp diff between tags (%)"),
        td(
          textfield(
            -name       => $self->config_name('MINPERDIFF'),
            -size       => 4,
            -maxlength  => 3, 
            -default    => 33)));
            
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("minimum gene length"),
        td(
          textfield(
            -name       => $self->config_name('MINORFLEN'),
            -size       => 4, 
            -maxlength  => 3, 
            -default    => 501)));
            
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("most five prime base eligble"),
        td(
          textfield(
            -name       => $self->config_name('FIVEPRIMESTART'),
            -size       => 4, 
            -maxlength  => 3, 
            -default    => 101)));
            
  push @choices, 
    TR({-class=>'searchbody'}, 
      th("minimum RSCU value for codon replacement"),
        td(
          textfield(
            -name       => $self->config_name('MINRSCUVAL'),
            -size       => 5,
            -maxlength  => 4,
            -default    => 0.06)));
            
  push @choices,
    TR({-class=>'searchbody'}, 
      th("Tagging Scope"),
        td(
          radio_group(
            -name     => $self->config_name('SCOPE'), 
            -values   => ['chrom', 'seg'], 
            -labels   => {
              'chrom' => 'whole chromosome', 
              'seg' => 'the following genes entirely within view now'}, 
            -default  =>'chrom')));
            
  my $html = table(@choices);
  return $html;
}

sub dump 
{
  my $self      = shift;
  my $segment   = shift;

  #If we're monitoring the results, print out from the cache and refresh in 5
  if (my $sid = param('session'))
  {
    my $cache = get_cache_handle();
    my $data = $cache->get($sid);
    unless($data and ref $data eq "ARRAY")
    {
      #some kind of error
      exit 0;
    }
    print $data->[0] 
      ? start_html(-title => "Results for PCRTagging job $sid")
      : start_html(-title => "Running PCRTagging job $sid", 
                   -head=>meta({-http_equiv =>'refresh', -content => '5'}));
    print p(i("This page will refresh in 5 seconds")) unless $data->[0];
    print pre($data->[1]);
    print p(i("...continuing...")) unless $data->[0];
    print end_html;
    return;
  }
  
  #Otherwise we're launching the script
  else
  {
   #Prepare persistent variables
    my $sid = Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
    my $cache = get_cache_handle();
    $cache->set($sid, [0, ""]);
   
   #Prepare arguments
    my $pa               = $self->configuration;
    my $gbrowse_settings = $self->page_settings;
    my $command;
    $pa->{OLDCHROMOSOME}   = $$gbrowse_settings{source};
    $pa->{STARTPOS} = $segment->start if ($pa->{SCOPE} eq "seg");
    $pa->{STOPPOS}  = $segment->end if ($pa->{SCOPE} eq "seg");
    $pa->{OUTPUT} = "html";
    
    $pa->{$_} = "\"$pa->{$_}\"" foreach (grep {$pa->{$_} =~ /\ /} keys %$pa);
    $command .= "--" . $_ . " " . $pa->{$_} . " " foreach (keys %$pa);
    
   #If we're the parent, prepare the url and offer a link.
    if (my $pid = fork)
    {
      delete_all();
      my $addy = self_url() . "?plugin=BS_PCRTagger;plugin_action=Go;";
      $addy .= "session=$sid";
      print start_html(
        -title => "Launching BioStudio...", 
        -head  => meta({
          -http_equiv => 'refresh', 
          -content    => "10; URL=\"$addy\""}));
      print p(i("BioStudio is running."));
      print p("Your job number is $sid.");      
      print "If you are not redirected in ten seconds, ";
      print "<a href=\"$addy\">click here for your results</a><br>";
      print p("Command:");
      print pre("$command");
      print end_html;
      return;
    }
   #If we're a child, launch the script, feed results to the cache  
    elsif(defined $pid)
    {
      close STDOUT;
      unless (open F, "-|") 
      {
        my $path = $BS->{bin} . "/BS_PCRTagger.pl";
        open STDERR, ">&=1";
        exec "$path $command" || die "Cannot execute PCRTagger: $!";
      }
      my $buf = "";
      while (<F>) 
      {
        $buf .= $_;
        $cache->set($sid, [0, $buf]);
      }
      $cache->set($sid, [1, $buf]);
      exit 0;
    } 
   #Otherwise, uh oh
    else 
    {
      die "Cannot fork: $!";
    }
  }
}

sub get_cache_handle 
{
  Cache::FileCache->new
  ({
      namespace => 'PCRTagger',
      username => 'nobody',
      default_expires_in => '30 minutes',
      auto_purge_interval => '4 hours',
  });
}

1;
__END__