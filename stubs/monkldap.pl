#! /usr/bin/perl

use strict;
use Net::LDAP;

my $host = "128.220.136.130";
my $adminDn = "cn=admin,dc=sblab,dc=jhu,dc=edu";
my $adminPwd = "sbyeast10";
my $searchBase = "dc=sblab,dc=jhu,dc=edu";
my $userdn = testGuid ("test_user2", "test123");

if ($userdn)
{
	print "authentication successful!\n";
}
else
{
  print "authentication fail!\n"; 
}



sub getUserDn
{
	my ($guid) = @_;
	my $dn;
	my $entry;

  my $ldap = Net::LDAP->new($host, verify=>'none') or die "$@";		
	
	my $mesg = $ldap->bind ($adminDn, password=>"$adminPwd");
	
	$mesg->code && return undef;
	
	$mesg = $ldap->search(base => $searchBase, filter => "uid=$guid" );
	 
	$mesg->code && return undef;
	$entry = $mesg->shift_entry;
	 
	if ($entry)
	{
		$dn = $entry->dn;
    $entry->dump;
	}
	
	$ldap->unbind;
	
	return $dn;
}

sub testGuid
{
	my $ldap;

	my $guid = shift;
	my $userPwd = shift;

	my $userDn = getUserDn ($guid);

	return undef unless $userDn;
	
	$ldap = Net::LDAP->new($host, verify=>'none') or die "$@";

	my $mesg = $ldap->bind ($userDn, password=>"$userPwd");
	
	if ($mesg->code)
	{
		# Bad Bind
		print $mesg->error . "\n";
		return undef;
	}
	
	$ldap->unbind;
	
	return $userDn;
}
