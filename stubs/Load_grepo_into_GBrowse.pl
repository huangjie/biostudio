#!/usr/bin/perl

use Bio::BioStudio::Basic qw(:all);

my $BS = configure_BioStudio("/etc/BioStudio");

my @srcs = get_genome_list($BS);
foreach my $src (@srcs)
{
	print "Loading $src...\n";
	system "perl $BS->{bin}/BS_PrepareGBrowse.pl -C $src";
}
