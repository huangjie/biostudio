#!/usr/bin/perl

use BioStudio;
use GeneDesign;
use strict;

my ($inutd, $inold) = @ARGV;
my @temp = split(/\//, $inold);
my $path = @temp[-1];

die "\nNo chromosomes named for processing\n\n" if (!$inold); 
die "\nNo new gff file named as up-to-date\n\n" if (!$inutd); 

my ($oldsp, $chrname, $oldgenver, $oldvername) = ($1, $2, $3, $4) if ($inold =~ $VERNAME);

my ($newsp, $newname, $newgenver, $newvername) = ($1, $2, $3, $4) if ($inutd =~ $VERNAME);

my $CODON_TABLE	 = define_codon_table($SPECIES{$newsp});
	
my ($featureOld_ref, $motherlessold_ref) = get_GFF_features($inold);
my $seqhshold = get_GFF_sequences($inold);
my $commentold = get_GFF_comments($inold);

my ($featureUtd_ref, $motherlessutd_ref) = get_GFF_features($inutd);
my $seqhshutd = get_GFF_sequences($inutd);
my $commentutd = get_GFF_comments($inutd);

my @CHANGES = compare_GFF_features($featureUtd_ref, $featureOld_ref, $seqhshutd, $seqhshold, $CODON_TABLE, 0, 0);
push @CHANGES, @{compare_GFF_sequences($seqhshutd, $seqhshold)};

## SEQUENCE CHANGES
print "\n\nSEQUENCE CHANGES:\n";
my $chrchgflag = scalar( grep { $_->kind < 0 } @CHANGES );
if ($chrchgflag != 0)
{
	foreach (keys %$seqhshutd)
	{
		$$seqhshold{$_} = $$seqhshutd{$_};
	}
}
print "WOAH!!\n" if ($$seqhshold{chrIV} ne $$seqhshutd{chrIV});

## FEATURE CHANGES

#some must be modified in the out of date file
print "\n\nFEATURES TO BE MODIFIED:\n";
foreach my $diff ( grep { $_->kind == 4 || $_->kind == 5} @CHANGES )
{
#	print "\t", $diff->feature->id, "\n";
	if (exists ($$featureOld_ref{$diff->feature->id}))
	{
		$$featureOld_ref{$diff->feature->id}->start($$featureUtd_ref{$diff->feature->id}->start);
		$$featureOld_ref{$diff->feature->id}->stop($$featureUtd_ref{$diff->feature->id}->stop);
	}
	elsif(exists($$featureOld_ref{$diff->parent}))
	{
		$$featureOld_ref{$diff->parent}->children->{$diff->feature->id}->start($$featureUtd_ref{$diff->parent}->children->{$diff->feature->id}->start);
		$$featureOld_ref{$diff->parent}->children->{$diff->feature->id}->stop($$featureUtd_ref{$diff->parent}->children->{$diff->feature->id}->stop);
	}
}
foreach my $diff ( grep { $_->kind == 11 } @CHANGES )
{
#	print "\t", $diff->feature->id, " ", $diff->attr, " ", $diff->old, " to ", $diff->var, "\n";
	my $attr = $diff->attr;
	if (exists ($$featureOld_ref{$diff->feature->id}))
	{
		$$featureOld_ref{$diff->feature->id}->$attr($$featureUtd_ref{$diff->feature->id}->$attr);
	}
	elsif(exists($$featureOld_ref{$diff->parent}))
	{
		$$featureOld_ref{$diff->parent}->children->{$diff->feature->id}->$attr($$featureUtd_ref{$diff->parent}->children->{$diff->feature->id}->$attr);
	}
}

#some must be added to the out of date file
print "\n\nNEW FEATURES TO BE ADDED:\n";
foreach my $diff ( grep { $_->kind == 0 } @CHANGES )
{
#	print "\t", $diff->feature->id, "\n";
	if (exists ($$featureOld_ref{$diff->feature->id}))
	{
		print $diff->feature->id, " 0 KEY ALREADY EXISTS!\n";
	}
	$$featureOld_ref{$diff->feature->id} = $diff->feature;
}

#some must be dropped from the out of date file
print "\n\nOLD FEATURES TO BE DROPPED:\n";
foreach my $diff ( grep { $_->kind == 1 } @CHANGES )
{
#	print "\t", $diff->feature->id, "\n";
	if (! exists ($$featureOld_ref{$diff->feature->id}))
	{
		print $diff->feature->id, " 1 KEY ALREADY GONE!\n";
		next;
	}
	delete ($$featureOld_ref{$diff->feature->id});
	if (exists ($$featureOld_ref{$diff->feature->id}))
	{
		print $diff->feature->id, " 1 KEY NOT GONE!\n";
	}
}

## SUBFEATURE CHANGES
#some must be added to the out of date file
print "\n\nNEW SUBFEATURES TO BE ADDED:\n";
foreach my $diff ( grep { $_->kind == 2 } @CHANGES )
{
#	print "\t", $diff->parent, "'s ", $diff->feature->id, "\n";
	if (exists ($$featureOld_ref{$diff->parent}->children->{$diff->feature->id}))
	{
		print $diff->feature->id, " 2 KEY ALREADY EXISTS!\n";
		next;
	}
	$$featureOld_ref{$diff->parent}->children->{$diff->feature->id} = $diff->feature;
}

#some must be dropped from the out of date file
print "\n\nOLD SUBFEATURES TO BE DROPPED:\n";
foreach my $diff ( grep { $_->kind == 3 } @CHANGES )
{
#	print "\t", $diff->parent, "'s ", $diff->feature->id, "\n";
	if (! exists ($$featureOld_ref{$diff->parent}->children->{$diff->feature->id}))
	{
		print $diff->feature->id, " 3 KEY ALREADY GONE!\n";
		next;
	}
	delete ($$featureOld_ref{$diff->parent}->children->{$diff->feature->id});
	if (exists ($$featureOld_ref{$diff->parent}->children->{$diff->feature->id}))
	{
		print $diff->feature->id, " 3 KEY NOT GONE!\n";
	}
}

@CHANGES = compare_GFF_features($featureUtd_ref, $featureOld_ref, $seqhshutd, $seqhshold, $CODON_TABLE, 0, 0);

## ANNOTATION CHANGES
#some must be added to the out of date file
print "\n\nNEW ANNOTATIONS TO BE ADDED:\n";
foreach my $diff ( grep { $_->kind == 8 } @CHANGES )
{
	next if ($diff->feature->type eq "CDS" || $diff->feature->type eq "intron");
#	print "\t", $diff->feature->id, " to ", $diff->old, "\n";
	my @temparr = split(/ = /, $diff->old);
	my ($attkey, $attval) = ($temparr[0], $temparr[1]);
	if (! $diff->parent )
	{
		if (exists(${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey}) || ${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} eq $attval)
		{
			print $diff->feature->id, " 8 KEY NOT MISSING OR MISMATCHED!\n";
			next;
		}
		${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} = $attval;
		print $diff->feature->id, " 8 NEW KEY VAL WRONG\n" if (! exists(${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey}) || ${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} ne $attval)
	}
	elsif (exists($$featureOld_ref{$diff->parent}) )
	{
		if (exists(${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey}) || ${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} eq $attval)
		{
			print $diff->feature->id, " 8 KEY NOT MISSING OR MISMATCHED!\n";
			next;
		}
		${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} = $attval;
		print $diff->feature->id, " 8 NEW KEY VAL WRONG!\n" if (! exists(${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey}) || ${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} ne $attval);
	}
	else
	{
		print "can't find ", $diff->parent, " or ", $diff->feature->id, " in the hashes.\n";
	}
}

print "\n\nOLD ANNOTATIONS TO BE DROPPED:\n";
#some must be dropped from the out of date file
foreach my $diff ( grep { $_->kind == 9 } @CHANGES )
{
#	print "\t", $diff->feature->id, " from ", $diff->var, "\n";
	my @temparr = split(/ = /, $diff->var);
	my ($attkey, $attval) = ($temparr[0], $temparr[1]);
	next if ($attkey eq "essential_status" || $attval eq "transposable_element" || $attval eq "pseudogene");
	if (! $diff->parent)
	{
		if (! exists(${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey}))
		{
			print $diff->feature->id, " 9 KEY ALREADY EXISTS!\n";
			next;
		}
		delete(${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey});
		print $diff->feature->id, " 9 NEW KEY VAL WRONG!\n" if (exists(${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey}));
	}
	elsif (exists($$featureOld_ref{$diff->parent}) )
	{
		if (exists(${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey}))
		{
			print $diff->feature->id, " 9 KEY ALREADY EXISTS!\n";
			next;
		}
		delete(${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey});
		print $diff->feature->id, " 9 NEW KEY VAL WRONG!\n" if (exists(${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey}));
	}
	else
	{
		print "can't find ", $diff->parent, " or ", $diff->feature->id, " in the hashes.\n";
	}
}


print "\n\nANNOTATIONS TO BE MODIFIED:\n";
#update the out of date annotations
foreach my $diff ( sort {$a->parent cmp $b->parent} sort {$a->feature->start <=> $b->feature->start} grep { $_->kind == 10 } @CHANGES )
{
#	print "\t", $diff->feature->id, " to ", $diff->old, " from ", $diff->var, "\n";
	my @temparr = split(/ = /, $diff->old);
	my ($attkey, $attval) = ($temparr[0], $temparr[1]);
	my @temparr2 = split(/ = /, $diff->var);
	my $attval2 = $temparr2[1];
	if (! $diff->parent)
	{
		print $diff->feature->id, " 10 BEFORE DOESN'T MATCH!\n" if (${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} ne $attval2);
		${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} = $attval;
		print $diff->feature->id, " 10 AFTER DOESN'T MATCH!\n" if ( ${$$featureOld_ref{$diff->feature->id}->attrib}{$attkey} ne $attval);
	}
	elsif (exists($$featureOld_ref{$diff->parent}) )
	{
		next if (${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} eq $attval);
		
	#	print $diff->feature->id, " 10 BEFORE WASN'T FIXED!\n";
		${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} = $attval;
		print $diff->feature->id, "10 AFTER DOESN'T MATCH!\n" if ( ${${$$featureOld_ref{$diff->parent}->children}{$diff->feature->id}->attrib}{$attkey} ne $attval);
	}
	else
	{
		print "can't find ", $diff->parent, " or ", $diff->feature->id, " in the hashes.\n";
	}
}

## NEW GFF FILE
print "\n\nWRITING NEW GFF...\n\n";
my @newgff;
push @newgff, @$commentutd;
foreach my $feat (sort {$a->start <=> $b->start} values %$featureOld_ref)
{
	push @newgff, print_GFF_feature($feat, 1);
}
push @newgff, @{print_GFF_sequences( $seqhshold )};
open (OUT, '>', "syn_yeast 2" . "/$path");
print OUT @newgff;
close OUT;

