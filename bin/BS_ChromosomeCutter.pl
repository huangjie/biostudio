#!/usr/bin/env perl

use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GBrowse qw(:all);
use Bio::BioStudio::GFF3 qw(:all);
use Bio::GeneDesign::Basic qw(:all);
use Time::Format qw(%time);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeCutter_$VERSION";

$| = 1;

my %ACTIONS = (seqdel => 1, seqdelprp => 1, featdel => 1, featdelprp => 1);

my %pa;
GetOptions (
      'OLDCHROMOSOME=s'   => \$pa{OLDCHROMOSOME},
      'EDITOR=s'          => \$pa{EDITOR},
      'MEMO=s'            => \$pa{MEMO},
      'SCALE=s'           => \$pa{SCALE},
      'ACTION=s'          => \$pa{ACTION},
      'STARTPOS=i'        => \$pa{STARTPOS},
      'STOPPOS=i'         => \$pa{STOPPOS},
      'FEATURE=s'         => \$pa{FEATURE},
      'INSERT=s'          => \$pa{INSERT},
      'OUTPUT=s'          => \$pa{OUTPUT},
			'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
############################### SANITY CHECKING ################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");
my $BS_FEATS = fetch_custom_features($BS);

$pa{SCALE} = "chrom" unless $pa{SCALE};
$pa{OUTPUT} = "txt" unless ($pa{OUTPUT} && $pa{OUTPUT} eq "html");

unless ($pa{OLDCHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
if ($pa{OLDCHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{OLDFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" 
                . $pa{SEQID} . "/" . $pa{OLDCHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n" 
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

unless ($pa{EDITOR} && $pa{MEMO})
{
  print "\n ERROR: Both an editor's id and a memo must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

unless ($pa{ACTION})
{
  die "\n ERROR: No action was specified.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
if (! exists $ACTIONS{$pa{ACTION}})
{
  die "\n ERROR: Unrecognized action requested.\n";
}
if ($pa{ACTION} =~ /feat/ && ! $pa{FEATURE})
{
  print "\n ERROR: A feature based action was requested, ";
  print "but no feature was specified.\n";
  die();
}
if ($pa{INSERT} && ! exists $BS_FEATS->{$pa{INSERT}})
{
  die "\n ERROR: Unrecognized custom feature requested for insertion.\n";
}
if ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS})
{
  die "\n ERROR: The start and stop coordinates do not parse.\n";
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{BSVERSION} = $bsversion;

print "CONFIGURING...\n";
$pa{INTRO}    = $pa{GENVER} . "_" . $pa{VERNAME};
$pa{REASON}   = "by $pa{EDITOR} ($pa{MEMO})";
$pa{COMMENTS} = get_GFF_comments($pa{OLDCHROMOSOME}, $BS);

if ($pa{SCALE} eq "genome")
{
  $pa{GENVER}++;
}
elsif ($pa{SCALE} eq "chrom")
{
  $pa{VERNAME}++;
  $pa{VERNAME} = "0" . $pa{VERNAME} while (length($pa{VERNAME}) < 2);
}
$pa{NEWCHROMOSOME}  = $pa{SPECIES} . "_" . $pa{SEQID} . "_" 
                    . $pa{GENVER} . "_" . $pa{VERNAME};
$pa{NEWFILE}        = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/"
                    . $pa{SEQID} . "/" . $pa{NEWCHROMOSOME} . ".gff";
system("cp $pa{OLDFILE} $pa{NEWFILE}");
system("chmod 777 $pa{NEWFILE}");

print "CONFIGURING DBS...\n";
my $DBLIST = list_databases($BS);
unless (exists ($DBLIST->{$pa{OLDCHROMOSOME}}) )
{
  create_database($pa{OLDCHROMOSOME}, $BS);
  load_database($pa{OLDCHROMOSOME}, $BS);
}

drop_database($pa{NEWCHROMOSOME}, $BS) 
  if exists($DBLIST->{$pa{NEWCHROMOSOME}});
create_database($pa{NEWCHROMOSOME}, $BS);
load_database($pa{NEWCHROMOSOME}, $BS);
$pa{DB} = fetch_database($pa{NEWCHROMOSOME}, $BS);
$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
$pa{EDITCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});

$pa{REPORT} = {};
  
#If wiki is in use, prepare wiki constants
my @featlist = keys %$BS_FEATS;
push @featlist, ("deletion", "proposed_deletion");
wiki_edit_prep(\%pa, $BS, \@featlist) if ($BS->{enable_wiki});

my @newcomments;
my %moddedfeats;

################################################################################
################################### FEATDEL ####################################
################################################################################
if ($pa{ACTION} eq "featdel")
{
  my ($swit, $bsfeat, $bsfeatlen) = (0, undef, undef);
  $swit = $pa{INSERT}  ? 1 : 0;
  $bsfeat    = $swit ? $BS_FEATS->{$pa{INSERT}} : undef;
  $bsfeatlen = $swit ? length($bsfeat->{SEQ})  : 0;
  my @targets  = $pa{DB}->features(
                          -seqid => $pa{SEQID},
                          -start => $pa{STARTPOS},
                          -end   => $pa{STOPPOS},
                          -range_type => "contains",
                          -type  => $pa{FEATURE});
  my %containers;
  my %movehash = ();
  foreach my $prefeat (@targets)
  {
    my $featid   = $prefeat->primary_id;
    my $feature  = $pa{DB}->fetch($featid);
    my ($startoffset, $endoffset) = (0, 0);
    ($startoffset, $endoffset) = @{$movehash{$featid}} if (exists $movehash{$featid});
    my $featname = $feature->Tag_load_id;
    my $featlen  = $feature->length();
    $pa{REPORT}->{$featname} = "";
    
    my @affecteds = $pa{DB}->features( 
      -seqid => $pa{SEQID}, 
      -start => $pa{STARTPOS}-1
    );
    foreach my $subj (@affecteds)
    { 
      my $wikinote = undef;
      my $stype = $subj->primary_tag;
      $movehash{$subj->primary_id} = [0, 0, 0] unless exists $movehash{$subj->primary_id};
      my $sname = $subj->Tag_load_id;
      my ($funique, $common, $aunique) = $feature->overlap_extent($subj);
     #upstream, unaffected 
      if ($common == 0 && $subj->end < $feature->start)
      {
        next;
      }
     #downstream, non-overlapping
      elsif ($common == 0 && $subj->start > $feature->end)
      { 
        my $movelen = $swit  ? ($featlen - $bsfeatlen)  :  $featlen;
        $movehash{$subj->primary_id}->[0] -= $movelen;
        $movehash{$subj->primary_id}->[1] -= $movelen;
      }
     #completely contained
      elsif ($aunique == 0 )
      {
        unless ($stype eq $pa{FEATURE} || $stype eq "deletion")
        {
          $movehash{$subj->primary_id}->[2]++;
          $pa{REPORT}->{$sname} .= " Deleted (contained in $featname);";
          push @newcomments, "# # $stype $sname deleted\n";
          push @{$pa{WIKICOMMENTS}}, "# # $stype [[$sname]] deleted<br>\n";
          $wikinote = "deleted $pa{REASON}<br>\n";
          unless ($subj->has_tag("parent_id") && $subj->Tag_parent_id eq $feature->Tag_load_id)
          {
            $pa{DB}->delete($subj) || print "  Can't delete $sname!\n";
          }
        }
        if ($stype eq "deletion")
        {
          $movehash{$subj->primary_id}->[0] -= ($subj->start- $feature->start);
          $movehash{$subj->primary_id}->[1] -= ($subj->start- $feature->start);
        }
      }
     #overlapping
      elsif ($common <= $featlen)
      {
       #three prime truncation
        if ($subj->end <= $feature->end && $subj->end >= $feature->start && $stype ne "chromosome")
        {
          $movehash{$subj->primary_id}->[1] -= $common;
          $pa{REPORT}->{$sname} = "3' truncated by $common bases";
          push @newcomments, "# # $sname $stype 3' truncated by $common bases\n";
          push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] $stype 3' truncated by $common bases<br>";
          $wikinote = "3' Truncated by $common bases $pa{REASON}<br>\n";
        }
       #five prime truncation
        elsif ($subj->start <= $feature->end && $subj->start >= $feature->start && $stype ne "chromosome")
        {
          my $shiftlen = $swit ? $bsfeatlen : $common;
          $movehash{$subj->primary_id}->[0] += $shiftlen;
          $pa{REPORT}->{$sname} = "5' Truncated by $common bases";
          push @newcomments, "# # $sname $stype 5' truncated by $common bases\n";
          push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] 5' truncated by $common bases<br>\n";
          $wikinote = "5' truncated by $common bases $pa{REASON}<br>\n";
        }
       #internal deletion
        else
        {
          my $shiftdist = $swit  ? $common - $bsfeatlen : $common;
          $movehash{$subj->primary_id}->[1] -= $shiftdist;
          $containers{$sname} += $common;#add this up later to avoid annotation spamming
        }
      }
      else
      {
        print " Problem... can't figure out how $subj and $feature are spatially related!!!\n";
      }
      if ($BS->{enable_wiki} && $wikinote)
      {
        my $flag1 = exists $moddedfeats{$sname}  ? 1 : 0;
        my $flag2 = $wikinote =~ /deleted/  ? 1 : undef;
        my $add = "&nbsp;&nbsp;$wikinote<br>";
        my $note = "<br>Modifications: <br>";
        wiki_update_feature(\%pa, $BS, $subj, $add, $flag1, $note, $flag2);
        $moddedfeats{$sname}++;
      }        
    }

    #Concatenate CDSes if you are deleting introns
    if ($pa{FEATURE} eq "intron")
    {
      my @flcds = $pa{DB}->features( 
          -seqid => $pa{SEQID}, 
          -start => $feature->start - 1, 
          -end => $feature->end + 1,
          -range_type => "overlaps",
          -type => "CDS");
      @flcds = grep {$_->Tag_parent_id eq $feature->Tag_parent_id} @flcds;
      @flcds = sort {$a->start <=> $b->start} @flcds;
      if (scalar(@flcds))
      {
        my ($CDS5, $CDS3) = ($flcds[0], $flcds[1]);
        $CDS5->end($CDS3->end);
        $CDS5->update();
        $movehash{$CDS5->primary_id}->[1] = $movehash{$CDS3->primary_id}->[1];
        $movehash{$CDS3->primary_id}->[2]++;
        my ($name5, $name3) = ($CDS5->Tag_load_id, $CDS3->Tag_load_id);
        $pa{REPORT}->{$name3} = "Merged with $name5";
        $pa{REPORT}->{$name5} = "Absorbed $name3";
        push @newcomments, "# # $name5 merged with $name3\n";
        if ($BS->{enable_wiki})
        {
          push @{$pa{WIKICOMMENTS}}, "# # [[$name5]] merged with [[$name3]]<br>\n";
          my $note = "<br>Modifications: <br>";
          my $flag5 = exists $moddedfeats{$name5} ? 1 : 0;
          my $flag3 = exists $moddedfeats{$name3} ? 1 : 0;
          my $add5 = "&nbsp;&nbsp;merged with [[$name3]] $pa{REASON}<br>";
          my $add3 = "&nbsp;&nbsp;merged with [[$name5]] $pa{REASON}<br>";
          wiki_update_feature(\%pa, $BS, $CDS5, $add5, $flag5, $note);
          wiki_update_feature(\%pa, $BS, $CDS3, $add3, $flag3, $note);
          $moddedfeats{$name5}++;
          $moddedfeats{$name3}++;
        }
        $pa{DB}->delete($CDS3) || print " Can't delete $CDS3!\n";
      }
    }  
    $movehash{$feature->primary_id}->[2]++;
    my $delstart = ($feature->start() + $startoffset) - 1;
    $delstart = 1 if $delstart <= 0;
    my $newfeat = $pa{DB}->new_feature(
      -start => $delstart,
      -end => $feature->start() + $startoffset,
      -seq_id => $pa{SEQID},
      -primary_tag => "deletion",
      -source => "BIO",
      -attributes =>{
        'intro' => $pa{INTRO},
        'aimed_at' => $featname,
        'load_id' => $featname . "_deletion",
        'bsversion' => $bsversion,
        'size' => $featlen,
        'wtpos' => $feature->start()
      });
    $pa{REPORT}->{$featname} .= " Deleted;";
    push @newcomments, "# # $pa{FEATURE} $featname deleted\n";
    if ($BS->{enable_wiki})
    {
      push @{$pa{WIKICOMMENTS}}, "# # [[$featname]] deleted<br>\n";
      my $flag = exists $moddedfeats{$featname} ? 1 :0;
      my $add = "&nbsp;&nbsp;Deleted (see [[" . $newfeat->Tag_load_id . "]])<br>;";
      wiki_update_feature(\%pa, $BS, $feature, $add, $flag);
      $moddedfeats{$featname}++;
      wiki_add_feature(\%pa, $BS, $newfeat);
    }
    if ($swit)
    {
      my $ndn = $bsfeat->{NAME} ."_" . "del" . "_" . $featname;
      my $newfeat = $pa{DB}->new_feature(
              -start   => $feature->start() + $startoffset,
              -end     => $feature->start() + $startoffset + $bsfeatlen - 1,
              -seq_id  => $pa{SEQID},
              -primary_tag => $bsfeat->{KIND},
              -source  => $bsfeat->{SOURCE},
              -attributes =>{
                'intro' => $pa{INTRO},
                'aimed_at' => $featname,
                'load_id' => $ndn,
                'bsversion' => $bsversion
              });
      $pa{REPORT}->{$ndn} .= " Inserted anew;";
      push @newcomments, "# # $bsfeat->{KIND} $ndn inserted\n";
      if ($BS->{enable_wiki})
      {
        push @{$pa{WIKICOMMENTS}}, "# # $bsfeat->{KIND} [[$ndn]] inserted<br>";
        wiki_add_feature(\%pa, $BS, $newfeat);
        my $flag = exists $moddedfeats{$featname} ? 1 : 0;
        my $add = "&nbsp;&nbsp;Replaced by $bsfeat->{KIND} [[$ndn]] $pa{REASON}<br>";
        wiki_update_feature(\%pa, $BS, $feature, $add, $flag);
        $moddedfeats{$featname}++;
      }
    }
    $pa{DB}->delete($feature) || print " Can't delete $featname!\n";
    $pa{EDITCHR} = $swit   
      ? substr($pa{EDITCHR}, 0, ($feature->start + $startoffset) - 1) 
        . $bsfeat->{SEQ} 
        . substr($pa{EDITCHR}, ($feature->end + $endoffset))
      : substr($pa{EDITCHR}, 0, ($feature->start + $startoffset) - 1)                  
        . substr($pa{EDITCHR}, ($feature->end + $endoffset));
  }  
  # Do all non container coordinate updates at once
  foreach my $moveid (keys %movehash)
  {
    my ($startshift, $endshift, $delflag) = @{$movehash{$moveid}};
    $startshift = 0 unless $startshift;
    $endshift = 0 unless $endshift;
    $delflag = 0 unless $delflag;
    next if ($delflag);
    my $movefeat = $pa{DB}->fetch($moveid);
    if (! $movefeat) { print "$moveid returns no feature!";next;}
    $movefeat->start($movefeat->start + $startshift);
    $movefeat->end($movefeat->end + $endshift);
    $movefeat->update();
  }   
  foreach my $sname (keys %containers)
  {
    my $basechange = $containers{$sname};
    push @newcomments, "# # $sname lost $basechange bases\n";
    $pa{REPORT}->{$sname} = "lost $basechange bases";
    if ($BS->{enable_wiki})
    {
      push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] lost $basechange bases<br>\n";
      my @features = $pa{DB}->get_features_by_attribute(load_id => $sname);
      my $feature = $features[0];
      my $add = "&nbsp;&nbsp;Lost $basechange bases $pa{REASON}<br>";
      my $flag = exists $moddedfeats{$sname}  ? 1 : 0;
      my $note = "<br>Modifications: <br>";
      wiki_update_feature(\%pa, $BS, $feature, $add, $flag, $note);
      $moddedfeats{$sname}++;
    }
  }
}

################################################################################
################################# FEATDELPRP  ##################################
################################################################################
elsif ($pa{ACTION} eq "featdelprp")
{
  my @targets  = $pa{DB}->features(
                      -seqid => $pa{SEQID},
                      -start => $pa{STARTPOS},
                      -end   => $pa{STOPPOS},
                      -range_type => "contains",
                      -type => $pa{FEATURE});
  foreach my $feature (@targets)
  {
    my $featname = $feature->Tag_load_id;
    my $featlen = $feature->length();
    my $newfeat = $pa{DB}->new_feature(
      -start => $feature->start(),
      -end => $feature->end(),
      -seq_id => $pa{SEQID},
      -primary_tag => "proposed_deletion",
      -source => "BIO",
      -attributes =>{
        'intro' => $pa{INTRO},
        'aimed_at' => $featname,
        'load_id' => $featname . "_propdel",
        'bsversion' => $bsversion,
        'size' => $featlen,
        'wtpos' => $feature->start()
      }
    );
    $pa{REPORT}->{$newfeat->Tag_load_id} .= " Added;";
    push @newcomments, "# # $pa{FEATURE} $featname proposed for deletion\n";
    if ($BS->{enable_wiki})
    {
      push @{$pa{WIKICOMMENTS}}, "# # [[$featname]] proposed for deletion<br>";
      wiki_add_feature(\%pa, $BS, $newfeat);

      my $flag = exists $moddedfeats{$featname} ? 1 : 0;
      my $add = "&nbsp;&nbsp;Proposed for deletion $pa{REASON}<br>";
      wiki_update_feature(\%pa, $BS, $feature, $add, $flag, undef);
      $moddedfeats{$featname}++;
    }
  }    
}

################################################################################
################################### SEQDEL  ####################################
################################################################################
elsif ($pa{ACTION} eq "seqdel")
{
  my ($swit, $bsfeat, $bsfeatlen) = (0, undef, undef);
  $swit = $pa{INSERT}  ? 1 : 0;
  $bsfeat    = $BS_FEATS->{$pa{INSERT}};
  $bsfeatlen = $swit ? length($bsfeat->{SEQ})  : 0;
  my $segname = "del" . "$pa{SEQID}_$pa{INTRO}_" . $pa{STARTPOS} . ".." . $pa{STOPPOS};
  my $segfeat = Bio::SeqFeature::Generic->new(
          -start   => $pa{STARTPOS},
          -end     => $pa{STOPPOS},
          -seq_id  => $pa{SEQID},
          -primary_tag => "proposed_deletion",
          -source  => "BIO");
  my $seglen = $segfeat->length();
  
  my @affecteds = $pa{DB}->features( 
      -seqid      => $pa{SEQID}, 
      -start      => $pa{STARTPOS}-1, 
      -range_type => "overlaps");
  @affecteds = sort {$a->start <=> $b->start 
      || (($b->end - $b->start) <=> ($a->end - $a->start))
      || ($a->has_tag("parent_id") <=> ($b->has_tag("parent_id")))} @affecteds;
  foreach my $subj (@affecteds)
  {
    my $wikinote = undef;
    my ($sname, $stype) = ($subj->Tag_load_id, $subj->primary_tag());
    my ($funique, $common, $aunique) = $segfeat->overlap_extent($subj);
   #upstream, unaffected 
    if ($common == 0 && $subj->end < $segfeat->start)
    {
      next;
    }
   #downstream, non-overlapping
    elsif ($common == 0 && $subj->start > $segfeat->end)
    { 
      my $movelen = $swit  ? ($seglen - $bsfeatlen)  :  $seglen;
      $subj->start($subj->start - $movelen);
      $subj->end  ($subj->end   - $movelen);
      $subj->update();
    }
   #completely contained
    elsif ($common >= 1 && $aunique == 0)
    {
      if ($stype ne "deletion")
      {   
        $pa{REPORT}->{$sname} .= " Deleted $sname (contained in $segname);";
        push @newcomments, "# # $sname $stype deleted\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] deleted\n";
        $wikinote = "deleted $pa{REASON}<br>";
        if ($subj->has_tag("parent_id") && $subj->Tag_parent_id)
        {
          next unless (scalar($pa{DB}->get_features_by_name($subj->Tag_parent_id)));
        }      
        $pa{DB}->delete($subj) || print "  Can't delete $sname!\n";
      }
      elsif ($stype eq "deletion")
      {
        $subj->start($subj->start - $pa{STARTPOS});
        $subj->end($subj->end - $pa{STARTPOS});
      }
    }
   #overlapping
    elsif ($common <= $seglen)
    {
     #three prime truncation
      if ($subj->end <= $segfeat->end && $subj->end >= $segfeat->start)
      {
        $subj->end($subj->end - $common);
        push @newcomments, "# # $stype $sname 3' Truncated by $common bases\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] 3' truncated by $common bases\n";
        $wikinote = "3' truncated by $common bases $pa{REASON}<br>";
        $subj->update();
      }
     #five prime truncation
      elsif ($subj->start <= $segfeat->end && $subj->start >= $segfeat->start)
      {
        my $shiftlen = $swit ? $bsfeatlen : $common;
        $subj->start($subj->start + $shiftlen);
        push @newcomments, "# # $sname $stype 5' Truncated by $common bases\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] 5' truncated by $common bases\n";
        $wikinote = "5' truncated by $common bases $pa{REASON}<br>";
        $subj->update();
      }
     #internal deletion
      else
      {
        my $shiftdist = $swit  ? $common - $bsfeatlen : $common;
        $subj->end($subj->end - $shiftdist);
        push @newcomments, "# # $sname lost $shiftdist bases\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] lost $shiftdist bases\n";
        $wikinote = "lost $shiftdist bases $pa{REASON}<br>";
        $subj->update();
      }
    }
    else
    {
      print " Problem... can't figure out how $subj and $segfeat are spatially related!!!\n";
    }
    if ($BS->{enable_wiki} && $wikinote)
    {
      my $flag1 = exists $moddedfeats{$sname}  ? 1 : 0;
      my $flag2 = $wikinote =~ /deleted/  ? 1 : undef;
      my $add = "&nbsp;&nbsp;$wikinote<br>";
      wiki_update_feature(\%pa, $BS, $subj, $add, $flag1, undef, $flag2);
      $moddedfeats{$sname}++;
    }
  }
  if ($swit)
  {
    my $ndn = $bsfeat->{NAME} ."_" . "del" . "_" . $segname;
    my $newfeat = $pa{DB}->new_feature(
            -start   => $pa{STARTPOS},
            -end     => $pa{STARTPOS} + $bsfeatlen - 1,
            -seq_id  => $pa{SEQID},
            -primary_tag => $bsfeat->{KIND},
            -source  => $bsfeat->{SOURCE},
            -attributes =>{
              'intro' => $pa{INTRO},
              'aimed_at' => $segname.
              'load_id' => $ndn,
              'bsversion' => $bsversion
            });
    $pa{REPORT}->{$ndn} .= " Inserted;";
    push @newcomments, "# # $bsfeat->{KIND} $ndn inserted\n";
    if ($BS->{enable_wiki})
    {
      push @{$pa{WIKICOMMENTS}}, "# # $bsfeat->{KIND} [[$ndn]] inserted<br>";
      wiki_add_Feature(\%pa, $BS, $newfeat);
    }
  } 
  my $newfeat = $pa{DB}->new_feature(
    -start => $pa{STARTPOS} - 1,
    -end => $pa{STARTPOS},
    -seq_id => $pa{SEQID},
    -primary_tag => "deletion",
    -source => "BIO",
    -attributes =>{
      'intro' => $pa{INTRO},
      'aimed_at' => $segname,
      'load_id' => $segname . "_deletion",
      'bsversion' => $bsversion,
      'size' => $seglen,
      'wtpos' => $pa{STARTPOS}
    });
  push @newcomments, "# # $segname deleted\n";
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # $segname deleted<br>";
    wiki_add_feature(\%pa, $BS, $newfeat);
  }
  $pa{EDITCHR} = $swit
    ? substr($pa{EDITCHR}, 0, $segfeat->start - 1) 
      . $bsfeat->{SEQ} 
      . substr($pa{EDITCHR}, $segfeat->end)
    : substr($pa{EDITCHR}, 0, $segfeat->start - 1)                  
      . substr($pa{EDITCHR}, $segfeat->end);
}

################################################################################
################################## SEQDELPRP ###################################
################################################################################
elsif ($pa{ACTION} eq "seqdelprp")
{
  my $segname = "$pa{SEQID}_$pa{INTRO}_$pa{STARTPOS}..$pa{STOPPOS}";
  my $newfeat = $pa{DB}->new_feature(
    -start => $pa{STARTPOS},
    -end => $pa{STOPPOS},
    -seq_id => $pa{SEQID},
    -primary_tag => "proposed_deletion",
    -source => "BIO",
    -attributes =>{
      'intro' => $pa{INTRO},
      'aimed_at' => $segname,
      'load_id' => $segname . "_propdel",
      'bsversion' => $bsversion,
      'size' => $pa{STOPPOS} - $pa{STARTPOS} + 1,
      'wtpos' => $pa{STARTPOS}
    });
  $pa{REPORT}->{$newfeat->Tag_load_id} .= " Added;";
  push @newcomments, "# # segment $segname proposed for deletion\n";
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # Segment $segname proposed for deletion<br>";
    wiki_add_feature(\%pa, $BS, $newfeat)
  }
}

################################################################################
############################ WRITING AND REPORTING #############################
################################################################################
#Assemble new GFF file
#comments
my $comment  = "# $pa{NEWCHROMOSOME} created from $pa{OLDCHROMOSOME} ";
   $comment .= "$time{'yymmdd'} by $pa{EDITOR} ($pa{MEMO})\n";
unshift @newcomments, $comment;
my $womment  = "# [[$pa{NEWCHROMOSOME}]] created from [[$pa{OLDCHROMOSOME}]] ";
   $womment .= "$time{'yymmdd'} by [[Main.$pa{EDITOR}]] ($pa{MEMO})<br>";
unshift @{$pa{WIKICOMMENTS}}, $womment;

make_GFF3(\%pa, $BS, \@newcomments);
update_wiki($BS, \%pa, $pa{WIKICOMMENTS}) if ($BS->{enable_wiki});
update_gbrowse($BS, \%pa) if ($BS->{enable_gbrowse});

################################################################################
############################### ERROR  CHECKING ################################
################################################################################
#check fidelity of newseq tags
foreach my $feature ($pa{DB}->features())
{
  my $flag = check_new_sequence($feature);
  if ($flag == 0)
  {
    print "WARNING - ", $feature->Tag_load_id, " has bad newseq tag; ";
    print $feature->Tag_newseq, " tag vs ", $feature->seq->seq, " actual\n";
  }
}
print "\n\n";

#Summarize
print "\n\n";
print "Report:\n";
foreach my $featname (sort keys %{$pa{REPORT}})
{
  print "$featname : $pa{REPORT}->{$featname}\n";
}

exit;

__END__

=head1 NAME

  BS_ChromosomeCutter.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility removes features from a chromosome and offers the chance to
    replace them with custom features. Custom features should be defined in 
    config/features.txt. You must define a sequence segment for the edits;it 
    can be the entire chromosome.
    
  The utility can be run in four modes:
    seqdel - The defined sequence segment will be deleted. You can have a
      custom feature inserted in its place.
    seqdelprp - A deletion proposal feature that spans the defined sequence
      segment will be created.  No sequence editing will take place.
    featdel - All features of the target type in the defined sequence segment
      will be deleted. You can have a custom feature inserted in their place.
    featdelprp - Deletion proposal features that span each feature of the target
      type in the defined sequence segment will be created. No sequence editing
      will take place.

=head1 ARGUMENTS

Required arguments:

  -OL,   --OLDCHROMOSOME : The chromosome to be modified
  -E,   --EDITOR : The person responsible for the edits
  -M,   --MEMO : Justification for the edits
  -STA, --STARTPOS : The first base eligible for editing
  -STO, --STOPPOS : The last base eligible for editing
  -A,   --ACTION : The action to take.
                seqdel : delete the defined segment
                seqdelprp : propose the defined segment for deletion
                featdel : delete the defined features from the defined segment
                          requires -F
                featdelprp : propose the defined features for deletion 
                          requires -F

Optional arguments:

  -SC,  --SCALE : [genome, chrom (def)] Which version number to increment
  -F,   --FEATURE : The feature to be targeted
  -I,   --INSERT : The feature to be inserted in each deletion;
                   must be an entry in config/features.txt
  -OU,  --OUTPUT : html or txt 
  -h,   --help : Display this message

=cut
