#!/usr/bin/env perl

use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GFF3 qw(:all);
use Bio::BioStudio::GBrowse qw(:all);
use Bio::GeneDesign::Basic qw(:all);
use Time::Format qw(%time);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeSplicer_$VERSION";

$| = 1;

my %ACTIONS = (segmentflank => 1, featflank => 1, featins => 1);

my %pa;
GetOptions (
      'OLDCHROMOSOME=s'   => \$pa{OLDCHROMOSOME},
      'EDITOR=s'          => \$pa{EDITOR},
      'MEMO=s'            => \$pa{MEMO},
      'SCALE=s'           => \$pa{SCALE},
      'ACTION=s'          => \$pa{ACTION},
      'STARTPOS=i'        => \$pa{STARTPOS},
      'STOPPOS=i'         => \$pa{STOPPOS},
      'FEATURE=s'         => \$pa{FEATURE},
      'INSERT=s'          => \$pa{INSERT},
      'DISTANCE=i'        => \$pa{DISTANCE},
      'DIRECTION=i'       => \$pa{DIRECTION},
      'NAME=s'            => \$pa{NAME},
      'OUTPUT=s'          => \$pa{OUTPUT},
    	'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");
my $BS_FEATS = fetch_custom_features($BS);

$pa{SCALE} = "chrom" unless $pa{SCALE};
$pa{OUTPUT} = "txt" unless ($pa{OUTPUT} && $pa{OUTPUT} eq "html");

unless ($pa{OLDCHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{OLDCHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{OLDFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                  $pa{SEQID} . "/" . $pa{OLDCHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n" 
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

unless ($pa{EDITOR} && $pa{MEMO})
{
  print "\n ERROR: Both an editor's id and a memo must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

unless ($pa{ACTION})
{
  print "\n ERROR: No action was specified.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
unless (exists $ACTIONS{$pa{ACTION}})
{
  die "\n ERROR: Unrecognized action requested.\n";
}
if ($pa{ACTION} eq "featflank" && ! $pa{FEATURE})
{
  print "\n ERROR: A feature based action was requested, ";
  print "but no feature was specified.\n";
  die();
}
if ($pa{ACTION} eq "featflank" && (! $pa{DISTANCE} || ! $pa{DIRECTION}))
{
  print "\n ERROR: Feature flank requested but distance or direction ";
  print "not specified.\n";
  die();
}

if ($pa{DIRECTION} &&
   ! ($pa{DIRECTION} == 3 || $pa{DIRECTION} == 5 || $pa{DIRECTION} == 35))
{
  die "\n ERROR: Feature flank requested but direction not 3, 5, or 35.\n";
}
if ($pa{ACTION} eq "featins" && ! $pa{NAME})
{
  die "\n ERROR: An insertion was requested but not named (use -N).\n";
}
if ($pa{ACTION} =~ /ins/ && ! $pa{INSERT})
{
  print "\n ERROR: A insertion action was requested but no custom feature was ";
  print "specified for insertion.\n";
  die();
}
if ($pa{INSERT} && ! exists $BS_FEATS->{$pa{INSERT}})
{
  die "\n ERROR: Unrecognized custom feature requested for insertion.\n";
}
if ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS})
{
  die "\n ERROR: The start and stop coordinates do not parse.\n";
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{BSVERSION} = $bsversion;

print "CONFIGURING...\n";
$pa{INTRO}    = $pa{GENVER} . "_" . $pa{VERNAME};
$pa{COMMENTS} = get_GFF_comments($pa{OLDCHROMOSOME}, $BS);
$pa{REASON}   = "by $pa{EDITOR} ($pa{MEMO})";

if ($pa{SCALE} eq "genome")
{
  $pa{GENVER}++;
}
elsif ($pa{SCALE} eq "chrom")
{
  $pa{VERNAME}++;
  $pa{VERNAME} = "0" . $pa{VERNAME} while (length($pa{VERNAME}) < 2);
}
$pa{NEWCHROMOSOME}  = $pa{SPECIES} . "_" . $pa{SEQID} . "_" . 
                      $pa{GENVER} . "_" . $pa{VERNAME};
$pa{NEWFILE}        = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                      $pa{SEQID} . "/" . $pa{NEWCHROMOSOME} . ".gff";
system("cp $pa{OLDFILE} $pa{NEWFILE}");
system("chmod 777 $pa{NEWFILE}");

print "CONFIGURING DBS...\n";
my $DBLIST = list_databases($BS);
unless (exists ($DBLIST->{$pa{OLDCHROMOSOME}}) )
{
  create_database($pa{OLDCHROMOSOME}, $BS);
  load_database($pa{OLDCHROMOSOME}, $BS);
}
drop_database($pa{NEWCHROMOSOME}, $BS) 
  if exists($DBLIST->{$pa{NEWCHROMOSOME}});
create_database($pa{NEWCHROMOSOME}, $BS);
load_database($pa{NEWCHROMOSOME}, $BS);
$pa{DB} = fetch_database($pa{NEWCHROMOSOME}, $BS);
$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
$pa{EDITCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});

$pa{REPORT} = {};
  
#If wiki is in use, prepare wiki constants
my @featlist = keys %$BS_FEATS;
wiki_edit_prep(\%pa, $BS, \@featlist) if ($BS->{enable_wiki});

my @newcomments;
my %moddedfeats;

################################################################################
################################## FEATFLANK ###################################
################################################################################
if ($pa{ACTION} eq "featflank")
{
  my $bsfeat    = $BS_FEATS->{$pa{INSERT}};
  my $bsfeatlen = length($bsfeat->{SEQ});
  my @directs   = ();
  my %movehash  = ();
  push @directs, 3 if ($pa{DIRECTION} =~ /3/);
  push @directs, 5 if ($pa{DIRECTION} =~ /5/);
  my @targets  = $pa{DB}->features(
                        -seqid => $pa{SEQID},
                        -start => $pa{STARTPOS},
                        -end   => $pa{STOPPOS},
                        -range_type => "contains",
                        -type => $pa{FEATURE});
  unless (scalar(@targets))
  {
    print "\n ERROR: There are no features of type $pa{FEATURE} in the ";
    print "interval $pa{STARTPOS}..$pa{STOPPOS}.\n";
    die();
  }

########################### BEGIN SC2.0 CONCESSIONS ############################
  if ($pa{FEATURE} eq "gene")
  {
    @targets = $pa{DB}->features(
                        -seqid => $pa{SEQID},
                        -start => $pa{STARTPOS},
                        -end   => $pa{STOPPOS},
                        -range_type => 'contains',
                        -type => $pa{FEATURE},
                        -attribute=>{"orf_classification" => 'Verified'});
    @targets = grep {$_->Tag_essential_status ne "Essential"} @targets;
  }
############################ END SC2.0 CONCESSIONS #############################

#  @targets = sort {$a->start <=> $b->start} @targets;
  foreach my $prefeat (@targets)
  {
    my $feature = $pa{DB}->fetch($prefeat->primary_id);
    die ("can't find feature?") if (! $feature);
    my $featname = $feature->Tag_load_id;
    my $featid = $feature->primary_id;
    my ($startoffset, $endoffset) = (0, 0);
    ($startoffset, $endoffset) = @{$movehash{$featid}} 
      if (exists $movehash{$featid});
    $pa{REPORT}->{$featname} = "";
    foreach my $direct (@directs)
    {
      my ($targetstart) = (undef);
      if (  ($direct == 3 && $feature->strand > -1) 
         || ($direct == 5 && $feature->strand == -1))
      {
        $targetstart = $feature->end() + $pa{DISTANCE} + 1 + $startoffset;
      }
      elsif ( ($direct == 3 && $feature->strand == -1) 
           || ($direct == 5 && $feature->strand > -1))
      {
        $targetstart = $feature->start() - ($pa{DISTANCE}) + $startoffset;
      }  
      my $targetend = $targetstart + $bsfeatlen - 1;
      my $blockstart = $targetstart - $startoffset;
      my $blockend = $blockstart + 1;
      my @blockedby = $pa{DB}->features(
                                  -seqid => $pa{SEQID},
                                  -start => $blockstart,
                                  -end   => $blockend,
                                  -range_type => "overlaps");
      @blockedby = grep {$_->primary_tag ne "chromosome"} @blockedby;
      if (scalar(@blockedby))
      {
        my $miscreant = join(", ", map {$_->Tag_load_id} @blockedby);
        $pa{REPORT}->{$featname} .= " Can't insert $bsfeat->{NAME} at $direct'";
        $pa{REPORT}->{$featname} .= " end, blocked by $miscreant";
      }
      else
      {
        my $ndn = $bsfeat->{NAME} ."_" . $direct . "_" . $pa{DISTANCE};
           $ndn .= "_" . $featname;
        my $newfeat = $pa{DB}->new_feature(
              -start        => $targetstart,
              -end          => $targetend,
              -seq_id       => $pa{SEQID},
              -primary_tag  => $bsfeat->{KIND},
              -source       => $bsfeat->{SOURCE},
              -attributes   =>{
                'intro'     => $pa{INTRO},
                'aimed_at'  => $featname,
                'load_id'   => $ndn,
                'bsversion' => $pa{BSVERSION}
        });
        $pa{EDITCHR}  = substr($pa{EDITCHR}, 0, $newfeat->start - 1)
                      . $bsfeat->{SEQ}
                      . substr($pa{EDITCHR}, $newfeat->start - 1);
        push @newcomments, "# # $bsfeat->{KIND} $ndn inserted\n";
        $pa{REPORT}->{$featname} .= " Inserted $bsfeat->{NAME} $direct'";
        $pa{REPORT}->{$featname} .= " of $featname";
        if ($BS->{enable_wiki})
        {
          push @{$pa{WIKICOMMENTS}}, "# # $bsfeat->{KIND} [[$ndn]] inserted<br>";
          wiki_add_feature(\%pa, $BS, $newfeat);
          
          my $flag = exists $moddedfeats{$featname}  ? 1 : 0;
          my $add = "&nbsp;&nbsp;[[$ndn]]<br>\n";
          my $note = "<br>Flanking $bsfeat->{NAME}s:<br>\n";
          wiki_update_feature(\%pa, $BS, $feature, $add, $flag, $note);
          $moddedfeats{$featname}++;
        }
        my @mustmove = $pa{DB}->features(
            -seqid      => $pa{SEQID},
            -start      => $blockstart, 
            -range_type => 'overlaps');
        #Store the update changes
        foreach my $movefeat (@mustmove)
        {
          next if ($movefeat->primary_tag eq $bsfeat->{KIND});
          $movehash{$movefeat->primary_id} = [] 
            unless exists $movehash{$movefeat->primary_id};
          unless ($movefeat->primary_tag() eq "chromosome")
          {
            $movehash{$movefeat->primary_id}->[0] += $bsfeatlen;
          }
          $movehash{$movefeat->primary_id}->[1] += $bsfeatlen;
        }
      }
    }
  }
  # Do all updates at once
  foreach my $moveid (sort keys %movehash)
  {
    my ($startshift, $endshift) = @{$movehash{$moveid}};
    $startshift = 0 unless $startshift;
    $endshift = 0 unless $endshift;
    my $movefeat = $pa{DB}->fetch($moveid);
    $movefeat->start($movefeat->start + $startshift);
    $movefeat->end($movefeat->end + $endshift);
    $movefeat->update();
  }
}

################################################################################
########################### SEGMENTFLANK &  FEATINS ############################
################################################################################
elsif ($pa{ACTION} eq "segmentflank" || $pa{ACTION} eq "featins")
{
  my $bsfeat    = $BS_FEATS->{$pa{INSERT}};
  my $bsfeatlen = length($bsfeat->{SEQ});
  my $mult = $pa{ACTION} eq "featins"  ? 1 : 2;
  my $insname = $pa{NAME} || undef;
  $insname =~ s/\s/\_/g if ($pa{ACTION} eq "featins");

  my $segname  = "$bsfeat->{NAME}_$pa{SEQID}_$pa{INTRO}_";
     $segname .= "$pa{STARTPOS}..$pa{STOPPOS}";
  my $segfeat = Bio::SeqFeature::Generic->new(
          -start   => $pa{STARTPOS},
          -end     => $pa{STOPPOS},
          -seq_id  => $pa{SEQID},
          -primary_tag => "flankfeat",
          -source  => "BIO");
  my $seglen = $segfeat->length();
  
  my @affecteds = $pa{DB}->features( 
      -seqid => $pa{SEQID}, 
      -start => $pa{STARTPOS}-1, 
      -range_type => "overlaps");
  foreach my $subj (@affecteds)
  {
    my ($sname, $stype) = ($subj->Tag_load_id, $subj->primary_tag());
    my ($funique, $common, $aunique) = $segfeat->overlap_extent($subj);
   #upstream, unaffected 
    if ($common == 0 && $subj->end < $segfeat->start)
    {
      next;
    }
   #downstream, non-overlapping
    elsif ($common == 0 && $subj->start > $segfeat->end)
    { 
      my $movelen = $mult * $bsfeatlen;
      $subj->start($subj->start + $movelen);
      $subj->end  ($subj->end   + $movelen);
      $subj->update();
    }
   #completely contained
    elsif ($common > 1 && $aunique == 0)
    {
      my $movelen = $bsfeatlen;
      $subj->start($subj->start + $movelen);
      $subj->end  ($subj->end   + $movelen);
      $pa{REPORT}->{$sname} .= " Moved $sname $movelen downstream;";
      $subj->update();
    }
   #overlapping
    elsif ($common <= $seglen)
    {
      my $wikinote = undef;
     #three prime insertion
      if ($subj->end <= $segfeat->end && $subj->end >= $segfeat->start)
      {
        $subj->end($subj->end + $bsfeatlen);
        push @newcomments, "# # $stype $sname 3' interrupted by $bsfeatlen bases\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] 3' interrupted by $bsfeatlen bases<br>";
        $wikinote = "3' interruption for site specific recombination insertion $pa{REASON}<br>";
        $subj->update();
      }
     #five prime insertion
      elsif ($subj->start <= $segfeat->end && $subj->start >= $segfeat->start)
      {
        if ($pa{ACTION} eq "BSFEATSEGMENTFLANK")
        {
          $subj->end($subj->end + $bsfeatlen);
          push @newcomments, "# # $stype $sname 5' interrupted by $bsfeatlen bases\n";
          push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] 5' interrupted by $bsfeatlen bases<br>";
          $wikinote = "5' interruption for site specific recombination insertion $pa{REASON}<br>";
        }
        elsif ($pa{ACTION} eq "BSFEATINS")
        {
          $subj->start($subj->start + $bsfeatlen);
          $subj->end($subj->end + $bsfeatlen);
          $pa{REPORT}->{$sname} .= " Moved $sname $bsfeatlen downstream;";
        }
        $subj->update();
      }
     #internal insertion
      else
      {
        my $shiftdist = $mult * $bsfeatlen;
        $subj->end($subj->end + $shiftdist);
        push @newcomments, "# # $sname gained $shiftdist bases\n";
        push @{$pa{WIKICOMMENTS}}, "# # [[$sname]] gained $shiftdist bases<br>";
        $wikinote = "insertion of site specific recombination site $pa{REASON}<br>";
        $subj->update();
      }
      if ($BS->{enable_wiki} && $wikinote)
      {
        my $flag = exists $moddedfeats{$sname}  ? 1 : 0;
        my $add = "&nbsp;&nbsp;$wikinote<br>\n";
        wiki_update_feature(\%pa, $BS, $subj, $add, $flag);
        $moddedfeats{$sname}++;
      }
    }
    else
    {
      print " Problem... can't figure out how $subj and $segfeat are spatially related!!!\n";
    }
  }  
  my $att = {'intro' => $pa{INTRO}, 'aimed_at' => $segname, 'bsversion' => $pa{BSVERSION}};
  my %latt = %$att;
  $latt{'load_id'} = $pa{ACTION} eq "BSFEATINS" ? $insname : $segname . "_5flank";
  $latt{'wtpos'} = $pa{STARTPOS} if $pa{ACTION} eq "BSFEATINS";
  my $leftfeat = $pa{DB}->new_feature(
          -start   => $pa{STARTPOS},
          -end     => $pa{STARTPOS} + $bsfeatlen - 1,
          -seq_id  => $pa{SEQID},
          -primary_tag => $bsfeat->{KIND},
          -source  => $bsfeat->{SOURCE},
          -attributes => \%latt);
  $pa{REPORT}->{$leftfeat->Tag_load_id} .= " Inserted;";
  push @newcomments, "# # $bsfeat->{KIND} " . $leftfeat->Tag_load_id ." inserted\n";
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # $bsfeat->{KIND} [[" . $leftfeat->Tag_load_id . "]] inserted<br>";
    wiki_add_feature(\%pa, $BS, $leftfeat);
  }
      
  if ($pa{ACTION} eq "SSRSEGMENTFLANK")
  {
    my %ratt = %$att;
    $ratt{'load_id'} = $segname . "_3flank";
    my $rightfeat = $pa{DB}->new_feature(
             -start   => $pa{STOPPOS} + $bsfeatlen + 1,
             -end     => $pa{STOPPOS} + $bsfeatlen + $bsfeatlen,
             -seq_id  => $pa{SEQID},
             -primary_tag => $bsfeat->{KIND},
             -source  => $bsfeat->{SOURCE},
             -attributes => \%ratt);
    $pa{REPORT}->{$rightfeat->Tag_load_id} .= " Inserted;";
    push @newcomments, "# # $bsfeat->{KIND} " . $rightfeat->Tag_load_id ."inserted\n";
    if ($BS->{enable_wiki})
    {
      push @{$pa{WIKICOMMENTS}}, "# # $bsfeat->{KIND} [[" . $rightfeat->Tag_load_id . "]] inserted<br>";
      wiki_add_feature(\%pa, $BS, $rightfeat);
    }
    
    $pa{EDITCHR} = substr($pa{EDITCHR}, 0, $leftfeat->start - 1) 
                  . $bsfeat->{SEQ} 
                  . substr($pa{EDITCHR}, $leftfeat->start - 1, $seglen)
                  . $bsfeat->{SEQ} . substr($pa{EDITCHR}, $segfeat->end);
  }
  else
  {
    $pa{EDITCHR} = substr($pa{EDITCHR}, 0, $leftfeat->start - 1) 
                  . $bsfeat->{SEQ}
                  . substr($pa{EDITCHR}, $leftfeat->start - 1);
  }
}


################################################################################
############################ WRITING AND REPORTING #############################
################################################################################
#Assemble new GFF file
#comments
my $comment  = "# $pa{NEWCHROMOSOME} created from $pa{OLDCHROMOSOME} ";
   $comment .= "$time{'yymmdd'} $pa{REASON}\n";
unshift @newcomments, $comment;
my $womment  = "# [[$pa{NEWCHROMOSOME}]] created from [[$pa{OLDCHROMOSOME}]] ";
   $womment .= "$time{'yymmdd'} by [[Main.$pa{EDITOR}]] ($pa{MEMO})<br>";
unshift @{$pa{WIKICOMMENTS}}, $womment;

make_GFF3(\%pa, $BS, \@newcomments);
update_wiki($BS, \%pa, $pa{WIKICOMMENTS}) if ($BS->{enable_wiki});
update_gbrowse($BS, \%pa) if ($BS->{enable_gbrowse});

################################################################################
############################### ERROR  CHECKING ################################
################################################################################
#check fidelity of newseq tags
foreach my $feature ($pa{DB}->features())
{
  my $flag = check_new_sequence($feature);
  if ($flag == 0)
  {
    print "WARNING - ", $feature->Tag_load_id, " has bad newseq tag; ";
    print $feature->Tag_newseq, " tag vs ", $feature->seq->seq, " actual\n";
  }
}
print "\n\n";

#Summarize
print "\n\n";
print "Report\n";
foreach my $featname (sort keys %{$pa{REPORT}})
{
  print "$featname : $pa{REPORT}->{$featname}\n";
}

exit;

__END__

=head1 NAME

  BS_ChromosomeSplicer.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility inserts features into a chromosome. Custom features should be 
    defined in config/features.txt. You must define a sequence segment for the 
    edits;it can be the entire chromosome.
    
  The utility can be run in three modes:
    segmentflank - In this mode, a custom feature will be inserted at both 
      ends of the defined sequence segment. The insert will interrupt any
      feature that overlaps the segment edges.
    featflank - In this mode, all instances of a feature in the defined sequence
      segment will be flanked (3', 5', or both) at a defined distance with an 
      instance of a custom feature. This insertion will NOT interrupt any 
      features;if a target feature cannot be flanked without interrupting
      another chromosomal feature, you will be informed.
    featins - In this mode, a custom feature will be inserted at the five prime
      end of the defined sequence segment. You must give the feature a unique
      name.

=head1 ARGUMENTS

Required arguments:

  -OLD,   --OLDCHROMOSOME : The chromosome to be modified
  -E,   --EDITOR : The person responsible for the edits
  -M,   --MEMO : Justification for the edits
  -STA, --STARTPOS : The first base eligible for editing
  -STO, --STOPPOS : The last base eligible for editing
  -A,   --ACTION : The action to take.
            segmentflank : flank the defined segment with custom features
                    requires -I
            featflank : flank a feature type in the defined segment with
                    custom features;requires -I, -F, -DIS, -DIR
            featins : insert a custom feature at the five prime end of the
                    defined segment;requires -I, -N

Optional arguments:

  -SC,  --SCALE : [genome, chrom (def)] Which version number to increment
  -F,   --FEATURE : The feature to be targeted
  -I,   --INSERT : The custom feature to be inserted;must be an entry in 
            config/features.txt
  -DIS, --DISTANCE : The distance in base pairs from a feature for an
            insertion;required for action featflank
  -DIR, --DIRECTION : [3, 5, 35] Should custom features be inserted 3p, 5p, or
            both 3p and 5p of a feature;required for action featflank
  -N,   --NAME : What to name the inserted feature;required for action featins
  -OUT, --OUTPUT : [html, txt (def)] Format for reporting and output
  -h,   --help : Display this message
  
=cut
