#!/usr/bin/perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::RestrictionEnzymes qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Time::Format qw(%time);
use Config::Auto;
use Getopt::Long;
use Pod::Usage;

use strict;
use warnings;
use warnings::unused;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeSegmenter_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'PLAN=s'	          => \$pa{PLANDBNAME},
      'WTCHR=s'           => \$pa{WTCHR},
      'EDITOR=s'          => \$pa{EDITOR},
      'MEMO=s'            => \$pa{MEMO},
      'CHUNKOLAP=i'       => \$pa{CHUNKOLAP},
      'OUTPUT=s'          => \$pa{OUTPUT},
			'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
############################### SANITY CHECKING ################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");
my $DBLIST = list_databases($BS);
my $BS_MARKERS = fetch_custom_markers($BS);
my %BASES = map {$_ => 1} qw(A T C G);

$pa{OUTPUT} = "txt" unless ($pa{OUTPUT} && $pa{OUTPUT} eq "html");
$pa{CHUNKOLAP} = 40 unless $pa{CHUNKOLAP};
unless ($pa{PLANDBNAME})
{
  print "\n ERROR: No chromosome segmentation plan was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");  
}
if ($pa{PLANDBNAME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{REPOLOC} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . $pa{SEQID} . "/";
}
else
{
  die ("\n ERROR: Planned chromosome name doesn't parse to BioStudio standard.\n");
} 
unless (exists $DBLIST->{$pa{PLANDBNAME}})
{
  print "\n ERROR: You must run Chromosome Segmentation Planner before you ";
  print "can run Chromosome Segmentation.\n";
  die();
}
$pa{NEWCHROMOSOME}  = $pa{PLANDBNAME};
$pa{NEWCHROMOSOME} =~ s/\_PLAN//;
$pa{NEWFILE} = $pa{REPOLOC} . $pa{NEWCHROMOSOME} . ".gff";
                   
my $oldvername = $pa{VERNAME} - 1;
$oldvername = "0" . $oldvername while (length($oldvername) < 2);
$pa{OLDCHROMOSOME} = $pa{SPECIES} . "_" . $pa{SEQID} . "_" 
                   . $pa{GENVER} . "_" . $oldvername;
die "\n ERROR: Cannot find the pre-edit chromosome $pa{OLDCHROMOSOME}.\n"
  unless (exists $DBLIST->{$pa{OLDCHROMOSOME}});
  
$pa{OLDFILE} = $pa{REPOLOC} . $pa{OLDCHROMOSOME} . ".gff";
die "\n ERROR: Can't find the chromosome in the repository.\n"
  unless (-e $pa{OLDFILE});
  
$pa{REDBNAME} = $pa{OLDCHROMOSOME} . "_RED";
die "\n ERROR: You must run Global Restriction Enzyme Markup before you can run Chromosome Segmentation.\n"
  unless (exists $DBLIST->{$pa{REDBNAME}});
      
unless ($pa{WTCHR})
{
  print "\n ERROR: No target chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
  
if ($pa{WTCHR} =~ $VERNAME)
{
  my ($SPECIES, $CHRNAME) = ($1, $2);
  die "\n ERROR: The target chromosome is not the same species or chromosome as the edited chromosome.\n"
    if ($pa{SPECIES} ne $SPECIES || $pa{CHRNAME} ne $CHRNAME);
  $pa{WTFILE} = $pa{REPOLOC} . $pa{WTCHR} . ".gff";
  die "\n ERROR: Can't find the target chromosome in the repository.\n" 
    unless (-e $pa{WTFILE});
}
else
{
  die ("\n ERROR: Target chromosome name doesn't parse to BioStudio standard.\n");
}

unless ($pa{EDITOR} && $pa{MEMO})
{
  print "\n ERROR: Both an editor's id and a memo must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{BSVERSION} = $bsversion;
print "CONFIGURING...\n";

$pa{COMMENTS} = get_GFF_comments($pa{OLDCHROMOSOME}, $BS);

$pa{CODON_TABLE} = define_codon_table($SPECIES{$pa{SPECIES}});
$pa{RSCU_VALUES} = define_RSCU_values($SPECIES{$pa{SPECIES}});
my $prevref;

unless (exists $DBLIST->{$pa{WTCHR}})
{
  create_database($pa{WTCHR}, $BS);
  load_database($pa{WTCHR}, $BS);
}
my $wt = fetch_database($pa{WTCHR}, $BS);
    
print "Loading database...\n";
my $pdb = fetch_database($pa{PLANDBNAME}, $BS);

print "Creating new database...\n";
drop_database($pa{NEWCHROMOSOME}, $BS) 
  if (exists $DBLIST->{$pa{NEWCHROMOSOME}});
create_database($pa{NEWCHROMOSOME}, $BS);
load_database($pa{NEWCHROMOSOME}, $BS, $pa{OLDCHROMOSOME});

$pa{DB} = fetch_database($pa{NEWCHROMOSOME}, $BS, 1);
$pa{DB}->index_subfeatures("true");
      
## Initialize parameters and constants
my $RE_DATA = define_sites($BS->{enzyme_dir} ."/standard_and_IIB");
my @enzlist = keys %{$RE_DATA->{CLEAN}};
$pa{ENZLIST} = \@enzlist;

$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
$pa{EDITCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});

my @chunklist = $pdb->features(
    -seq_id => $pa{SEQID}, 
    -type   => "chunk");
my @mchunklist = $pdb->features(
    -seq_id => $pa{SEQID}, 
    -type   => "megachunk");
$pa{DB}->add_features(\@chunklist);
$pa{DB}->add_features(\@mchunklist);

if ($BS->{enable_wiki})
{
  my @features = qw(chunk megachunk restriction_enzyme_recognition_site coding_sequence_variant);
  wiki_edit_prep(\%pa, $BS, \@features);
  $pa{MODDEDGENES} = {}; 
}
################################################################################
################################### EDITING ####################################
################################################################################
print "COMMITTING...\n";
my @newcomments;
my @wikicomments;

my @boundaries;
##Perform and Store enzyme recognition site modifications, record annotations
print "Making changes...\n";
my @list = $pdb->get_features_by_type("enzyme_recognition_site");
my %saves = map {$_->display_name => 1} @list;
$pa{SAVES} = \%saves;
foreach my $recsite (sort {$a->start <=> $b->start} @list)
{
  my $presence = $recsite->Tag_presence;
  if ($recsite->has_tag("remove"))
  {
    foreach my $recname ($recsite->get_tag_values("remove"))
    {
      removeEnzyme($_, \%pa) foreach (SelectEnzymesByName($recname, \%pa));
    }
  }
  if ($presence ne "i")
  {
    push @boundaries, addEnzyme($recsite, \%pa);
  }
  else
  {   
    push @boundaries, annotateEnzyme($recsite, \%pa);
  }
  #  print "\n\n";
}

#Adjust chunk start and stops to obey minimum overlap constraint
foreach my $re (@boundaries)
{
  my @chunks = $re->get_tag_values("chunks");
  next unless (scalar(@chunks) == 2);
  my @inchunks;
  push @inchunks, $pa{DB}->get_features_by_name($_) foreach (@chunks);
  my ($lefch, $rigch) = sort {$a->start <=> $b->start} @inchunks;
  if ($lefch && $rigch)
  {
    my $overlap = $lefch->end - $rigch->start + 1;
    my $lefcheck = substr($lefch->seq->seq, -$overlap);
    my $rigcheck = substr($rigch->seq->seq, 0, $overlap);
    print "LOHNOES overlap mismatch ! $lefcheck != $rigcheck\n" unless ($lefcheck eq $rigcheck);
    my $diff = $pa{CHUNKOLAP} - $overlap;
    next if ($diff <= 0);
    my $lext = int(($diff/2) + 0.5);
    my $rext = $lext - ($diff % 2);
    #  print $re->display_name, " @chunks, ($overlap), $diff, $lext, $rext\n";
    $lefch->end($lefch->end + $lext);
    $rigch->start($rigch->start - $rext);
    $lefch->update();
    $rigch->update();    
  }
}


##Add megachunks, adjusting starts and stop, checking for excisor presence
print "Adjusting boundaries...\n";
foreach my $mchunk ($pa{DB}->get_features_by_type("megachunk"))
{
  my $mchname = $mchunk->display_name;
  print "\tworking on $mchname...\n";
  my @chunks = $pa{DB}->features(
          -seq_id     => $pa{SEQID}, 
          -type       => "chunk", 
          -attributes => {"megachunk" => $mchname});
  @chunks = sort {$a->start <=> $b->start} @chunks;
  foreach my $chunk (@chunks)
  {
    $mchunk->start($chunk->start) if (abs($mchunk->start - $chunk->start) <=50);
    $mchunk->end($chunk->end) if (abs($mchunk->end - $chunk->end) <=50);
    push @newcomments, "# # chunk " . $chunk->display_name ." annotated\n";
  }
  $mchunk->update();
  my $lastchunk = $chunks[-1];
  my $excisor = $mchunk->Tag_excisor;
  if ($excisor)
  {
    my @blist = SelectEnzymesByEnzPosition($excisor, $lastchunk->start, $lastchunk->end, \%pa);
    removeEnzyme($_, \%pa) foreach (@blist);   
  }
  push @newcomments, "# # megachunk " . $mchunk->display_name ." annotated\n";
}

################################################################################
############################ WRITING AND REPORTING #############################
################################################################################

#Assemble new GFF file
#comments
my $comment  = "# $pa{NEWCHROMOSOME} created from $pa{OLDCHROMOSOME} ";
   $comment .= "$time{'yymmdd'} by $pa{EDITOR} ($pa{MEMO})\n";
unshift @newcomments, $comment;
my $womment  = "# [[$pa{NEWCHROMOSOME}]] created from [[$pa{OLDCHROMOSOME}]] ";
   $womment .= "$time{'yymmdd'} by [[Main.$pa{EDITOR}]] ($pa{MEMO})<br>";
unshift @wikicomments, $womment;

make_GFF3(\%pa, $BS, \@newcomments);
update_wiki($BS, \%pa, \@wikicomments) if ($BS->{enable_wiki});
update_gbrowse($BS, \%pa) if ($BS->{enable_gbrowse});


################################################################################
############################### ERROR  CHECKING ################################
################################################################################
print "Proofreading...\n";
my $y = 0;
@mchunklist = $pa{DB}->get_features_by_type("megachunk");
foreach my $megachunk (@mchunklist)
{
  $y++;
  #  print $megachunk->display_name, "\n";
  my $mchname = $megachunk->display_name;
  my $excisor = $megachunk->Tag_excisor;
  my @chunks = $pa{DB}->features(
      -seq_id     => $pa{SEQID}, 
      -type       => "chunk", 
      -attributes => {"megachunk" => $mchname});

  @chunks = sort {$a->start <=> $b->start} @chunks;
  for my $x (1 .. scalar(@chunks))
  {
    my $flag = $x == scalar(@chunks)  ? 1 : 0;
    my $chunk = $chunks[$x-1];
    my $chname = $chunk->display_name;
    my @res = $pa{DB}->features(
        -seq_id     => $pa{SEQID}, 
        -type       => "restriction_enzyme_recognition_site", 
        -attributes => {"chunks" => $chname});
    @res = sort {$a->start <=> $b->start} @res;
    #    if ($flag == 0)
    {
      my $chunkseq = $chunk->seq->seq;
      my ($lefre, $rigre) = ($res[0], $res[-1]);
      $lefre = undef if ($y == 1 && $x == 1);
      $rigre = undef if (scalar @res == 1 && ! ($y == 1 && $x == 1));
      if ($lefre)
      {
        my $lefenz = $lefre->Tag_enzyme;
        my $lefsite = $RE_DATA->{CLEAN}->{$lefenz};
        my $leflen = length($lefsite);
        my $lefcut = $RE_DATA->{TABLE}->{$lefenz};
        my ($leflef, $lefrig) = (0, 0);
        my @lefregs = @{$RE_DATA->{REGEX}->{$lefenz}};
        ($leflef, $lefrig) = ($1, $2) if ($lefcut =~ $IIAreg);
        ($leflef, $lefrig) = ($lefrig, $leflef) if ($lefrig < $leflef);
        $lefrig = 0 if ($lefrig < 0);
        $leflen = $leflen + $lefrig + $pa{CHUNKOLAP};
        my $lchunkseq = substr($chunkseq, 0, $leflen);        
        unless (  $lchunkseq =~ $lefregs[0] 
              || (scalar(@lefregs) > 1 && $lchunkseq =~ $lefregs[1]))
        {
          print "\t", $chunk->display_name, " ", $chunk->start, "..", $chunk->end, " $flag\n";        
          print "\t\t\t5OHNO CAN'T FIND $lefenz at 5' end ($leflen, $lefsite, ";
          print "$lefcut, $leflef, $lefrig)!\n";
          print "\t\t5': $lefenz\t$lchunkseq\t$lefsite\t$lefcut\n";
        }
      }
      if ($rigre)
      {
        my $rigenz = $rigre->Tag_enzyme;
        my $rigsite = $RE_DATA->{CLEAN}->{$rigenz};
        my $riglen = length($rigsite);
        my $rigcut = $RE_DATA->{TABLE}->{$rigenz};
        my ($riglef, $rigrig) = (0, 0);
        my @rigregs = @{$RE_DATA->{REGEX}->{$rigenz}};
        ($riglef, $rigrig) = ($1, $2) if ($rigcut =~ $IIAreg);
        ($riglef, $rigrig) = ($rigrig, $riglef) if ($rigrig < $riglef); 
        $rigrig = 0 if ($rigrig < 0); 
        $riglen = $riglen + $rigrig + $pa{CHUNKOLAP};
        my $rchunkseq = substr($chunkseq, -($riglen+1500)); 
        unless ($rchunkseq =~ $rigregs[0] 
            || (scalar(@rigregs) > 1 && $rchunkseq =~ $rigregs[1]) 
            || ($y == scalar(@mchunklist) && $x == scalar(@chunks)))
        {
          print "\t", $chunk->display_name, " ", $chunk->start, "..", $chunk->end, " $flag\n";        
          print "\t\t\t3OHNO CAN'T FIND $rigenz at 3' end ($riglen, $rigsite, ";
          print "$rigcut, $riglef, $rigrig)!\n";
          print "\t\t3': $rigenz\t$rchunkseq\t$rigsite\t$rigcut\n";
        } 
      }  
      my %bordercount = ();
      $bordercount{$_->Tag_enzyme}++ foreach (@res);
      #my @ts = keys %bordercount;
      #      print "\t\t@ts\n"; 
      foreach my $re (@res)
      {
        my $enz = $re->Tag_enzyme;
        my $site = $RE_DATA->{CLEAN}->{$enz};
        my $temphash = siteseeker($chunkseq, $RE_DATA->{CLEAN}->{$enz}, $RE_DATA->{REGEX}->{$enz}); 
        my @positions = keys %$temphash;
        my $seq = substr($chunkseq, $positions[0], length($site));
        my $occurs = scalar(@positions);
        if ( $occurs != $bordercount{$enz} )
        {
          print "\t\t$enz\t$seq\t$site\n";          
          print "\t", $chunk->display_name, " ", $chunk->start, "..", $chunk->end, " $flag\n";          
          print "\t\t\tPOHNO $enz is not happening the right number of times ";
          print "in this chunk ($occurs vs $bordercount{$enz}, @positions)!\n$chunkseq\n\n";
        }
      }
      #      print "\n\n"
      #print "\t$chunkseq\n\n";
    }
  }
  if ($excisor)
  {
    my $lastchunk = $chunks[-1];
    my $chunkseq = $lastchunk->seq->seq;
    my $temphash = siteseeker($chunkseq, $RE_DATA->{CLEAN}->{$excisor}, $RE_DATA->{REGEX}->{$excisor}); 
    my @positions = keys %$temphash;
    if (scalar @positions)
    {
      @positions = map {$_ + $lastchunk->start} @positions;
      print "\t", $lastchunk->display_name, " ", $lastchunk->start, "..", $lastchunk->end, "\n";  
      print "BOHNO! $excisor must be removed from @positions in ";
      print $lastchunk->display_name, "\n$chunkseq\n\n";
    }
  }
  #  print "\n\n\n\n";
}

##CHECK ALL CDS TRANSLATIONS
my @CDSes     = $pa{DB}->get_features_by_type("CDS");
foreach my $CDS (@CDSes)
{
  my $name = $CDS->display_name;
  my $phase = $CDS->phase ? $CDS->phase : 0;
  my $pseq = substr($pa{ORIGCHR}, $CDS->start - 1, $CDS->stop - $CDS->start + 1);
  my $nseq = substr($pa{EDITCHR}, $CDS->start - 1, $CDS->stop - $CDS->start + 1);
  ($pseq, $nseq) = (complement($pseq, 1), complement($nseq, 1)) if ($CDS->strand == -1);
  my $paa = translate($pseq, $phase + 1, $pa{CODON_TABLE});
  my $naa = translate($nseq, $phase + 1, $pa{CODON_TABLE});
  print "\tEOHNOES! The translation of $name has changed!\n" if ($paa ne $naa);
}

################################################################################
############################# GENERATING SEQUENCE ##############################
################################################################################
## find genes, take note of essential genes
print "Parsing synthetic GFF annotations...\n";
$pa{CHRSEQ}   = $pa{DB}->fetch_sequence($pa{SEQID});
my @genes     = $pa{DB}->get_features_by_type("gene"); 

$pa{ASSEMBLYDIR} = $pa{REPOLOC} . "assemble_" . $pa{NEWCHROMOSOME} . "/";
mkdir $pa{ASSEMBLYDIR} unless (-e $pa{ASSEMBLYDIR});
$pa{MCHUNKDIR}  = $pa{ASSEMBLYDIR} . "MEGACHUNKS/";
mkdir $pa{MCHUNKDIR} unless (-e $pa{MCHUNKDIR});
$pa{CHUNKDIR}   = $pa{ASSEMBLYDIR} . "CHUNKS/";
mkdir $pa{CHUNKDIR} unless (-e $pa{CHUNKDIR});
$pa{FCHUNKDIR}  = $pa{CHUNKDIR} . "FASTA";
mkdir $pa{FCHUNKDIR} unless (-e $pa{FCHUNKDIR});
$pa{ACHUNKDIR}  = $pa{CHUNKDIR} . "ApE";
mkdir $pa{ACHUNKDIR} unless (-e $pa{ACHUNKDIR});

## Load up wildtype features
$pa{WTSEQ}    = $wt->fetch_sequence($pa{SEQID});

## Fetch the chunks and restriction enzyle landmarks
@mchunklist = sort {$a->start <=> $b->start} @mchunklist;
my $mchunknum = scalar(@mchunklist);
my $z = 0;
foreach my $megachunk (@mchunklist)
{
  $z++;
  my $mchname = $megachunk->display_name;
  my $mchseq = $megachunk->seq->seq;
  my $mchseqid = $mchname . ", " . $megachunk->start . ".." . $megachunk->end;
  my $wraparr = print_as_fasta($mchseq, $mchseqid);
  open (MCHUNK, ">$pa{MCHUNKDIR}/$mchname.FASTA") 
    or die ("can't open $pa{MCHUNKDIR}/$mchname.FASTA for writing, $!");
  print MCHUNK @$wraparr, "\n\n";
  close MCHUNK;
  
  my $marker = $megachunk->Tag_marker;
  my $excisor = $megachunk->Tag_excisor;
  print "$mchname $marker $excisor\n";
  my @chunks = $pa{DB}->features(
    -seq_id     => $pa{SEQID}, 
    -type       => "chunk", 
    -attribute  => {"megachunk" => $mchname});
  @chunks = sort {$a->start <=> $b->start} @chunks;
  for my $x (1 .. scalar(@chunks))
  {
    my $flag = $x == scalar(@chunks)  ? 1 : 0;
    my $chunk = $chunks[$x-1];
    my $chunkname = $chunk->display_name;
    open (CHUNK, ">$pa{FCHUNKDIR}/$chunkname.FASTA") 
      or die ("can't open $pa{FCHUNKDIR}/$chunkname.FASTA for writing, $!");
    my $apeout = Bio::SeqIO->new(
        -file => ">$pa{ACHUNKDIR}/$chunkname.gb",
        -format => 'genbank');
    my $apeseq = Bio::Seq->new( -id => $chunkname);
    my $apecomment = Bio::Annotation::Comment->new();
    my @apeenzlist;
    my $chlen = $chunk->end - $chunk->start + 1;
    my @res = $pa{DB}->features(
      -seq_id     => $pa{SEQID}, 
      -type       => "restriction_enzyme_recognition_site", 
      -attribute  => {"chunks" => $chunkname});
    @res = sort {$a->start <=> $b->start} @res;
    my ($lefre, $rigre) = ($res[0], $res[-1]);
    my $lefenz = join("", $lefre->get_tag_values("enzyme"));
    my $rigenz = join("", $rigre->get_tag_values("enzyme"));
    $lefenz = "the 5' telomere" if ($x == 1 && $z == 1);
    $rigenz = $excisor if ($flag == 1);
    my $chunkseq = $chunk->seq->seq;
    my $chunklen = length($chunkseq);
    if ($flag != 1)
    {
      my $note = $chlen > 10000 ? "!!" : "";
      print "\t", $chunkname, " $chlen $note bp ", $chunk->start, "..";
      print $chunk->end, " $flag\n";
      $apeseq->seq($chunkseq);
      my $comment  = "from $lefenz to $rigenz; $chunklen bp from ";
         $comment .= $chunk->start . ".." . $chunk->end;
      $apecomment->text("$chunkname from $comment");
      my $chunkseqid = "$chunkname $comment";
      my $wraparr = print_as_fasta($chunkseq, $chunkseqid);
      print CHUNK @$wraparr, "\n\n";
      push @apeenzlist, ($lefenz, $rigenz);
    }
    elsif ($flag == 1 && $z != $mchunknum)
    { 
      my @ores = $pa{DB}->features(
        -seq_id => $pa{SEQID}, 
        -type   => "restriction_enzyme_recognition_site", 
        -start  => $megachunk->end - 2000, 
        -end    => $megachunk->end);
      die ("oops, too many midres\n") if (scalar(@ores) != 1);
      my $midre = $ores[0];
      my $midenz = join("", $midre->get_tag_values("enzyme"));
      my @rems = ($rigenz, $lefenz, $midenz);
      my $markerregion = remove_markers(\%pa, $BS_MARKERS->{$marker}->{DB}, \@rems);
      my @midngenes = sort {abs($a->end - $midre->start) <=> abs($b->end - $midre->start)  } @genes;
      my @rigngenes = sort {abs($a->start - $megachunk->end) <=> abs($b->start - $megachunk->end)} @genes;
      my ($midgene, $riggene) = ($midngenes[0], $rigngenes[0]);   
      my @wtmidgenes = $wt->features(
          -seq_id => $pa{SEQID}, 
          -type   => "gene", 
          -name   => $midgene->display_name);
      my @wtriggenes = $wt->features(
          -seq_id => $pa{SEQID}, 
          -type   => "gene", 
          -name   => $riggene->display_name);
      my $wtmidgene = $wtmidgenes[0];
      my $wtriggene = $wtriggenes[0];
      my $midendoffset = $midgene->end - $midre->end;
      my $rigstartoffset = $megachunk->end - $riggene->start;
      
      #Figure out how much wt to pad the IIB site with
      my $rigcut = $RE_DATA->{TABLE}->{$rigenz};
      my $rigsite = $RE_DATA->{CLEAN}->{$rigenz};
      my $lenrigclean = length($rigsite);
      my ($wlef, $clef, $wrig, $crig) = ($1, $2, $3, $4) if ($rigcut =~ $IIBreg);
      ($wlef, $clef) = ($clef, $wlef) if ($wlef < $clef);
      ($wrig, $crig) = ($crig, $wrig) if ($wrig < $crig);
      ($wlef, $wrig) = ($wlef+5, $wrig+5);
      my $wtextension = $wlef + $lenrigclean + $wrig;
      
      my $wtstart = $wtmidgene->end - $midendoffset + 1;
      my $wtend = $wtriggene->start + $rigstartoffset - 1;
      $wtend += $wtextension;
      my $synstart = $midre->end + 1;
      my $synend = $megachunk->end -1;
      my $synsize = $synend - $synstart + 1;
      my $wtISS = substr($pa{WTSEQ}, $wtstart - 1, $wtend - $wtstart + 1);
      my $wtcopy = $wtISS;
      substr($wtcopy, -($wrig + $lenrigclean), $lenrigclean) = $rigsite;
      for my $x (length($wtcopy) - ($wrig + $lenrigclean) .. length($wtcopy)-($wrig+1))
      {
        my $wtb = substr($wtISS, $x, 1);
        my $reb = substr($wtcopy, $x, 1);
        if ($wtb eq $reb || $wtb =~ regres($reb))
        {
          substr($wtcopy, $x, 1) = $wtb;
        }
        else
        {
          my $str = $NTIDES{$reb};
          $str =~ s/\W//;
          my @arr = grep {exists $BASES{$_}} split("", $str);
          substr($wtcopy, $x, 1) = $arr[0];
        }
      }
      $wtISS = $wtcopy;
      $wtISS = lc $wtISS;
      my $synISS = lc substr($pa{CHRSEQ}, $synstart-1, $synend - $synstart +1);
      my $preseq = substr($pa{CHRSEQ}, $chunk->start - 1, $synstart - $chunk->start);
      my $newseq = $preseq . substr($synISS, 0,  int($synsize / 2)) 
                 . $markerregion . substr($wtISS, $synsize / 2);
      my $len = length($newseq);
      my $note = $len > 10000 ? "!!" : "";
      print "\t", $chunkname, " $len $note bp ", $chunk->start, "..", $chunk->end, " $flag\n";
      foreach my $chenz ($lefenz, $rigenz, $midenz)
      {
        my $checkhsh = siteseeker($newseq, $chenz, $RE_DATA->{REGEX}->{$chenz});
        print "\t\tOHNO! $chenz too many times\n" if (scalar(keys %$checkhsh) > 1);
        print "\t\tOHNO! $chenz too few times\n" if (scalar(keys %$checkhsh) == 0);        
      }
      $apeseq->seq($newseq);
      my $start = index $newseq, substr($BS_MARKERS->{$marker}->{SEQ}, 0, 30);
      my $comment  = "from $lefenz to ($midenz) $rigenz; $len bp total, ";
         $comment .= "annotated from " . $chunk->start . ".." . $chunk->end;
         $comment .= ";ISS: $midenz synseq ($synstart..$synend) <$marker>";
         $comment .= " wtseq ($wtstart..$wtend) $rigenz";
      $apecomment->text("$chunkname from $comment");
      my $chunkseqid = "$chunkname $comment";
      my $wraparr = print_as_fasta($newseq, $chunkseqid);
      print CHUNK @$wraparr, "\n\n";
      push @apeenzlist, ($lefenz, $midenz, $rigenz);
      my $feat = Bio::SeqFeature::Generic->new
      (
        -primary => "marker_gene",
        -start => $start + 1,
        -end   => $start + length($BS_MARKERS->{$marker}->{SEQ}),
        -tag   => {
          label            => $marker, 
          ApEinfo_fwdcolor => $BS_MARKERS->{$marker}->{COLOR}, 
          ApEinfo_revcolor => $BS_MARKERS->{$marker}->{COLOR}}
      );
      $apeseq->add_SeqFeature($feat) || print "no go on the feat add, boss\n";       
    }
    elsif ($z == $mchunknum)
    {
      my $note = $chlen > 10000 ? "!!" : "";
      print "\t", $chunkname, " $chlen $note bp ", $chunk->start, "..";
      print $chunk->end, " $flag\n";
      $apeseq->seq($chunkseq);
      my $comment  = "from $lefenz to just before the 3' UTC; $chunklen bp ";
         $comment .= "from " . $chunk->start . " to " . $chunk->end;
      $apecomment->text("$chunkname from $comment");
      my $chunkseqid = "$chunkname $comment";
      my $wraparr = print_as_fasta($chunkseq, $chunkseqid);
      print CHUNK @$wraparr, "\n\n";
      push @apeenzlist, $lefenz;
    }
    print "\n";
    
    my %seenenzs;
    # print "@enzlist $marker $id $desc\n\n";
    foreach my $enz (@apeenzlist)
    {
      next if (exists $seenenzs{$enz});
      my $checkhsh = siteseeker($apeseq->seq, $enz, $RE_DATA->{REGEX}->{$enz});
      foreach my $start (keys %$checkhsh)
      {
        my $feat = Bio::SeqFeature::Generic->new
        (
          -primary => "misc_feature",
          -start  => $start + 1,
          -end    => $start + length($$RE_DATA{CLEAN}->{$enz}),
          -tag    => {
            label            => $enz, 
            ApEinfo_fwdcolor => "#CC0000", 
            ApEinfo_revcolor => "#CC0000"}
        );
        $apeseq->add_SeqFeature($feat) || print "no go on the add boss\n";
      }
      $seenenzs{$enz}++;
    }
        
    $apeseq->add_Annotation('comment', $apecomment);
    $apeout->write_seq($apeseq);
    close CHUNK;
  }
  print "\n\n\n\n";
}


exit;

################################################################################
################################# SUBROUTINES ##################################
################################################################################
sub SelectEnzymesByName 
{
  my ($name, $pa) = @_;
  my $dbh = DBI->connect(
    "dbi:mysql:$pa->{REDBNAME}", $BS->{"mysql_user"}, $BS->{"mysql_pass"}, 
    { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $sth = $dbh->prepare
  (
    "SELECT presence, start, end, enzyme, feature, strand, peptide 
     FROM positions
     WHERE name = \"$name\""
  ) or die "Unable to prepare command: ". $dbh->errstr."\n";
  
  $sth->execute or die "Unable to exec cmd: ". $dbh->errstr."\n";
  my @res = ();
  while (my $aref = $sth->fetchrow_arrayref)
  {
    my ($presence, $start, $end, $enzyme, $feature, $strand, $peptide) = @$aref;
    my $newfeat = {};
    $newfeat->{NAME} = $name;
    $newfeat->{PRE} = $presence;
    $newfeat->{START} = $start;
    $newfeat->{END} = $end;
    $newfeat->{ENZ} = $enzyme;
    $newfeat->{FEAT} = $feature;
    $newfeat->{STRAND} = $strand;
    $newfeat->{PEP} = $peptide;
    push @res, $newfeat;
  }
  $sth->finish;
  $dbh->disconnect();  
  return @res;
}

sub SelectEnzymesByEnzPosition 
{
  my ($enz, $left, $right, $pa) = @_;
  my $dbh = DBI->connect(
    "dbi:mysql:$pa->{REDBNAME}", 
    $BS->{"mysql_user"}, $BS->{"mysql_pass"}, 
    { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $sth = $dbh->prepare
  (
  "SELECT name, presence, start, end, enzyme, feature, strand, peptide from positions 
    where enzyme = \"$enz\" and start >= $left and end <= $right"
  ) or die "Unable to prepare command: ". $dbh->errstr."\n";
  
  $sth->execute or die "Unable to exec cmd: ". $dbh->errstr."\n";
  my @res = ();
  while (my $aref = $sth->fetchrow_arrayref)
  {
    my ($name, $presence, $start, $end, $enzyme, $feature, $strand, $peptide) = @$aref;
    my $newfeat = {};
    $newfeat->{NAME} = $name;
    $newfeat->{PRE} = $presence;
    $newfeat->{START} = $start;
    $newfeat->{END} = $end;
    $newfeat->{ENZ} = $enzyme;
    $newfeat->{FEAT} = $feature;
    $newfeat->{STRAND} = $strand;
    $newfeat->{PEP} = $peptide;
    push @res, $newfeat;
  }
  $sth->finish;
  $dbh->disconnect();  
  return @res;
}

sub removeEnzyme
{
  my ($rfeat, $pa) = @_;
  return if (exists $pa->{SAVES}->{$rfeat->{NAME}});
  if ($rfeat->{PRE} eq "i")
  {
    print "WTF? $rfeat->{NAME} ? This is immutable...\n";
    return;
  }
  my $rfeatstart = $rfeat->{START};
  my $rfeatend = $rfeat->{END};
  my $rstrand = $rfeat->{STRAND};
  my $renz = $rfeat->{ENZ};
  my $site = $RE_DATA->{CLEAN}->{$renz};
  my $rpeptide = $rfeat->{PEP};
  my @remgenes = $pdb->features(
      -seq_id     => $pa->{SEQID}, 
      -type       => "CDS", 
      -range_type => 'overlaps', 
      -start      => $rfeatstart, 
      -stop       => $rfeatend);
  my $rCDS = $remgenes[0];
  
  my $genename = $rCDS->Tag_parent_id;
  my @genes = $pdb->get_features_by_name($genename);
  while ($genes[0]->has_tag("parent_id"))
  {
    $genename = $genes[0]->Tag_parent_id;
    @genes = $pdb->get_features_by_name($genename);
  }
  my $gene = $genes[0];
  die ("can't find gene from $genename!\n") unless $gene;
  
  if ($rCDS->strand == -1)
  {
    $rfeatend ++ while (($rCDS->end - $rfeatend) % 3 != 0);
    $rfeatstart -- while (($rfeatend - $rfeatstart + 1) /3  < length($rpeptide));
  }
  else
  {
    $rfeatstart -- while (($rfeatstart - $rCDS->start) % 3 != 0);
    $rfeatend ++ while (($rfeatend - $rfeatstart + 1) /3  < length($rpeptide));
  }  
  my $testseq = substr($pa->{EDITCHR}, $rfeatstart - 1, $rfeatend - $rfeatstart + 1);
  $testseq = complement($testseq, 1) if ($rCDS->strand == -1);
  my $regindex = $rstrand == 1  ? 0 : 1;
  return unless ($testseq =~ $RE_DATA->{REGEX}->{$renz}->[$regindex]);
  my $newpatt = pattern_remover($testseq, $RE_DATA->{REGEX}->{$renz}, 
                                $pa->{CODON_TABLE}, $pa->{RSCU_VALUES});
  if ($newpatt eq "0" || $newpatt eq $testseq)
  { 
    print "\tRemoving ", $rfeat->{NAME}, " from $rCDS... ($testseq to $newpatt)\n"; 
    print "\t\tROHNOES\t$renz w/ $rpeptide @ $rfeatstart\t($testseq ? $site ";
    print "? $newpatt)\n\n";
    next;
  }
  $newpatt = complement($newpatt, 1) if ($rCDS->strand == -1);
  $testseq = complement($testseq, 1) if ($rCDS->strand == -1);
  my $id = $rfeat->{NAME} . "_removed";
  my $csv = $pa->{DB}->new_feature(
    -seq_id       => $pa->{SEQID},
    -start        => $rfeatstart,
    -end          => $rfeatend,
    -source       => "BIO",
    -primary_tag  => "coding_sequence_variant",
    -display_name => $id,
    -index        => 1,
    -attributes   => {
            load_id   => $id, 
            bsversion => $pa->{BSVERSION}, 
            wtseq     => $testseq,
            newseq    => $newpatt,
            infeat    => $genename,  
            intro     => $pa->{INTRO}}
  );
  substr($pa->{EDITCHR}, $rfeatstart - 1, $rfeatend - $rfeatstart + 1) = $newpatt;
  push @{$pa->{NEWCOMMENTS}}, "# # coding_sequence_variant $id added\n";
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # coding_sequence_variant [[$id]] added<br>\n";
    wiki_add_feature(\%pa, $BS, $csv);
    my $flag = exists $pa->{MODDEDGENES}->{$genename} ? 1 : 0;
    my $add = "&nbsp;&nbsp;[[$id]]<br>\n";
    my $note = "<br>Restriction Enzyme Modifications:<br>\n";
    wiki_update_feature(\%pa, $BS, $gene, $add, $flag, $note);
    $pa->{MODDEDGENES}->{$genename}++;    
  }
  return;
}

sub addEnzyme
{
  my ($recsite, $pa) = @_;
  my $start = $recsite->start;
  my $end = $recsite->end;
  my $strand = $recsite->strand;
  my $enz = $recsite->Tag_enzyme;
  my $site = $RE_DATA->{CLEAN}->{$enz};
  my $cut = $RE_DATA->{TABLE}->{$enz};
  my ($lef, $rig) = (0, 0);
  ($lef, $rig) = ($1, $2) if ($cut =~ $IIAreg);
  ($lef, $rig) = ($rig, $lef) if ($rig < $lef);
  $rig = 0 if ($rig < 0);
  my $sitelen = length($site);
  my @ingenes = $pdb->features(
      -seq_id     => $pa->{SEQID}, 
      -type       => "CDS", 
      -range_type => 'overlaps', 
      -start      => $start, 
      -stop       => $end);
  my $peptide = $recsite->Tag_peptide;
  my $CDS = $ingenes[0];
  my $genename = $CDS->Tag_parent_id;
  my @genes = $pdb->get_features_by_name($genename);
  while ($genes[0]->has_tag("parent_id"))
  {
    $genename = $genes[0]->Tag_parent_id;
    @genes = $pdb->get_features_by_name($genename);
  }
  my $gene = $genes[0];
  die ("can't find gene from $genename!\n") unless $gene;
  my $ohang = $recsite->Tag_wanthang;
  my $offset = $recsite->Tag_ohangoffset || 0;
  if ($CDS->strand == -1)
  {
    $end ++ while (($CDS->end - $end) % 3 != 0);
    $start -- while (($end - $start + 1) /3  < length($peptide));
  }
  else
  {
    $start -- while (($start - $CDS->start) % 3 != 0);
    $end ++ while (($end - $start + 1) /3  < length($peptide));
  }
  my $testseq = substr($pa->{EDITCHR}, $start - 1, $end - $start + 1);
  $testseq = complement($testseq, 1) if ($CDS->strand == -1);
  my $aa = translate($testseq, 1, $pa->{CODON_TABLE});
  $site = complement($site, 1) if ($strand == -1);
  my $alnmat = pattern_aligner($testseq, $site, $aa, $pa->{CODON_TABLE}, 0, $prevref);
  unless ($RE_DATA->{TYPE}->{$enz} =~ /b/)
  {
    substr($alnmat, $offset, length($ohang)) = $ohang;
  }
  my $newpatt = pattern_adder($testseq, $alnmat, $pa->{CODON_TABLE}, $prevref); 
  $newpatt = complement($newpatt, 1) if ($CDS->strand == -1);
  $testseq = complement($testseq, 1) if ($CDS->strand == -1);
#  my $oldmat = $alnmat;  
  #    print "\twant hang $ohang in site $cut pep $peptide... ";
  #    print "$offset\n\t$testseq\n\t$oldmat\n\t$alnmat\n\t$newpatt\n\n";
  
  if (! $newpatt || $aa ne $peptide)
  {  
    print "\tAdding $recsite to $CDS...\n";
    print "\tAOHNOES\t$enz w/ $peptide @ $start in $CDS\t($aa  ? $testseq ? ";
    print "$site) ($alnmat $newpatt $testseq)\n\n";
    next;
  }
  my @chunks = $recsite->get_tag_values("chunks");
  my @inchunks;
  push @inchunks, $pa->{DB}->get_features_by_name($_) foreach (@chunks);
  my $id = "$enz" . "_" . $start;
  my $re = $pa->{DB}->new_feature(
    -seq_id       => $pa->{SEQID},
    -start        => $start,
    -end          => $end,
    -source       => "BIO",
    -score        => $recsite->score,
    -primary_tag  => "restriction_enzyme_recognition_site",
    -display_name => $id,
    -strand       => $strand,
    -index        => 1,
    -attributes   => {
          load_id   => $id,
          peptide   => $peptide,
          enzyme    => $enz, 
          wtseq     => $testseq,
          newseq    => $newpatt,
          intro     => $pa->{INTRO},
          megachunk => $recsite->Tag_megachunk, 
          infeat    => $genename,
          bsversion => $pa->{BSVERSION},
          ohang     => $ohang}
  );
  $re->add_tag_value("chunks", $_) foreach (@chunks);
  $re->update();
  substr($pa->{EDITCHR}, $start -1, $end - $start + 1) = $newpatt;
  push @{$pa->{NEWCOMMENTS}}, "# # restriction_enzyme_recognition_site $id added\n";
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # restriction_enzyme_recognition_site [[$id]] added<br>";
    wiki_add_feature(\%pa, $BS, $re);
    my $flag = exists $pa->{MODDEDGENES}->{$genename} ? 1 : 0;
    my $add = "&nbsp;&nbsp;[[$id]]<br>\n";
    my $note = "<br>Restriction Enzyme Modifications:<br>\n";
    wiki_update_feature(\%pa, $BS, $gene, $add, $flag, $note);
    $pa->{MODDEDGENES}->{$genename}++;
  }
  my $regindex = $RE_DATA->{CLASS}->{$enz} eq "IIP" || $CDS->strand == $strand ? 0  : 1;
  my $reg = $RE_DATA->{REGEX}->{$enz}->[$regindex];
  #Precisely mark position in chunks
  foreach my $chunk (@inchunks)
  {
    $chunk->start(1) if ($chunk->display_name =~ /\.A1\Z/);
    $chunk->update();
    my @positions;
    if (abs($start - $chunk->start) <= 50)
    {
      while ($newpatt =~ /(?=$reg)/ig)
      {
        push @positions, (pos $newpatt);
      }
      print "\t\tA2OHNO CAN'T FIND $enz in $newpatt!!!\n" unless (scalar(@positions));
      $chunk->start($start + $positions[0]) if ($regindex == 0);
      $chunk->start($start + $positions[0] - $rig) if ($regindex == 1);
      #      print $chunk->display_name, " new start: ", $chunk->start;
      $chunk->update();
    }
    if (abs($start - $chunk->end) <= 50)
    {
      while ($newpatt =~ /(?=$reg)/ig)
      {
        push @positions, (pos $newpatt);
      }
      print "\t\tA2OHNO CAN'T FIND $enz in $newpatt!!!\n" unless (scalar(@positions));
      $chunk->end($start + $positions[0] + $sitelen - 1) if ($regindex == 0);
      $chunk->end($start + $positions[0] + ($sitelen + $rig) - 1) if ($regindex == 1);
      #print $chunk->display_name, " new end: ", $chunk->end, " ($positions[0], $sitelen, $rig)\n";
      $chunk->update();
    }
  }
  return $re;
}

sub annotateEnzyme
{
  my ($recsite, $pa) = @_;
  #    print "\tAnnotating $recsite...\n";
  my $enz = $recsite->Tag_enzyme;
  my $start = $recsite->start;
  my $end = $recsite->end;
  my $strand = $recsite->strand;
  my $ohang = $recsite->Tag_wanthang;
  my $site = $RE_DATA->{CLEAN}->{$enz};
  my $sitelen = length($site);
  my $cut = $RE_DATA->{TABLE}->{$enz};
  my ($lef, $rig) = (0, 0);
  ($lef, $rig) = ($1, $2)     if ($cut =~ $IIAreg);
  ($lef, $rig) = ($rig, $lef) if ($rig < $lef);
  $rig = 0 if ($rig < 0);
  my @chunks = $recsite->get_tag_values("chunks");
  my @inchunks;
  my $id = $recsite->display_name;
  push @inchunks, $pa->{DB}->get_features_by_name($_) foreach (@chunks);
  my $re = $pa->{DB}->new_feature(
    -seq_id       => $pa->{SEQID},
    -start        => $start,
    -end          => $end,
    -source       => "BIO",
    -primary_tag  => "restriction_enzyme_recognition_site",
    -display_name => $id,
    -strand       => $strand,
    -attributes   => {
            load_id   => $id, 
            enzyme    => $enz, 
            intro     => $pa->{INTRO}, 
            bsversion => $pa->{BSVERSION}, 
            ohang     => $ohang,
            megachunk => $recsite->Tag_megachunk
    }
  );
  $re->add_tag_value("chunks", $_) foreach (@chunks);
  $re->update();
  push @{$pa->{NEWCOMMENTS}}, "# # restriction_enzyme_recognition_site $id annotated\n"; 
  if ($BS->{enable_wiki})
  {
    push @{$pa{WIKICOMMENTS}}, "# # restriction_enzyme_recognition_site [[$id]] annotated<br>";
    wiki_add_feature(\%pa, $BS, $re);
  }
  foreach my $chunk (@inchunks)
  {
    if (abs($start - $chunk->start) <= 50)
    {
      $chunk->start($start-1) if ($strand == 1);
      $chunk->start($start-1 - $rig) if ($strand == -1);
      # print $chunk->display_name, " new start: ", $chunk->start;
      $chunk->update();
    }
    if (abs($start - $chunk->end) <=50)
    {
      $chunk->end($start + $sitelen - 2) if ($strand == 1);
      $chunk->end($start + $sitelen + $rig - 2) if ($strand == -1);
      # print $chunk->display_name, " new end: ", $chunk->end, "\n";
      $chunk->update();
    }
  }
  return $re;
}

sub remove_markers
{
  my ($pa, $mdb, $remlist) = @_;
  my @genes = $mdb->features(-type => "CDS");
  my @regions = $mdb->features(-type => "region");
  my $region = $regions[0]->seq->seq;
  foreach my $gene (@genes)
  {
    my $geneseq = $gene->seq->seq;
    #   print "before: $geneseq\n";
    my $aaseq = translate($geneseq, 1, $pa->{CODON_TABLE});
    foreach my $remsite (@$remlist)
    {
      my $temphash = siteseeker($geneseq, $RE_DATA->{CLEAN}->{$remsite}, 
                                $RE_DATA->{REGEX}->{$remsite});
      foreach my $result (sort {$b <=> $a} keys %$temphash)
      {
        #     print "removing $remsite\n";
        my $patterns = $RE_DATA->{REGEX}->{$remsite};
        my $framestart = $result % 3;
        my $startpos = $result - $framestart;
        my $critseg = substr($geneseq, $startpos, ((int(length($temphash->{$result})/3 + 2))*3));
        my $newcritseg = pattern_remover($critseg, $patterns, $pa->{CODON_TABLE}, $pa->{RSCU_VALUES} );
        substr($geneseq, $startpos, length($critseg)) = $newcritseg;
        #		print "$result, $temphash->{$result}, $critseg, $newcritseg,\n";
        print "OHNOES! bad trans\n" if ($aaseq ne translate($geneseq, 1, $pa->{CODON_TABLE}));
        my $nSITE_STATUS = define_site_status($geneseq, $RE_DATA->{REGEX});
        last if ($nSITE_STATUS->{$remsite} == 0)
      }
      my $nSITE_STATUS = define_site_status($geneseq, $RE_DATA->{REGEX});
      print "OHNOES! bad remove\n" if ($nSITE_STATUS->{$remsite} != 0);
    }
    #    print "after: $geneseq\n";
    substr($region, $gene->start-1, $gene->end - $gene->start + 1) = $geneseq;
  }
  return $region;
}

__END__

=head1 NAME

  BS_ChromosomeSegmenter.pl
  
=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility implements a restriction enzyme recognition site modification
    plan from the BS_ChromosomeSegmentationPlanner plugin.

=head1 ARGUMENTS

Required arguments:

  -P,  --PLAN : The chromosome to be modified
  -W,  --WTCHR  : The chromosome that will receive chunks (usually wildtype)
  -E,  --EDITOR : The person responsible for the edits
  -M,  --MEMO   : Justification for the edits
  
Optional arguments:

  --CHUNKOLAP : The number of bases each chunk must overlap (default 40)
  --OUTPUT    : [html, txt (def)] Format of reporting and output
  -h,   --help : Display this message
  
=cut
