#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::RestrictionEnzymes qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Config::Auto;
use Getopt::Long;
use Pod::Usage;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_GlobalREMarkup_$VERSION";

$| = 1;

my %pa;
GetOptions (
			'CHROMOSOME=s'	=> \$pa{CHROMOSOME},
			'RESET=s'       => \$pa{RESET},
			'CULLINC=i'     => \$pa{CULLINC},
			'CHUNKLENMIN=i' => \$pa{CHUNKLENMIN},
			'CHUNKLENMAX=i' => \$pa{CHUNKLENMAX},
			'REDOREDB'      => \$pa{REDOREDB},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");
$pa{CULLINC} = 2000 unless ($pa{CULLINC});
$pa{REDOREDB} = 0 unless ($pa{REDOREDB});
$pa{BSVERSION} = $bsversion;

unless ($pa{CHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
  die;
}
if ($pa{CHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}) = ($1, $2);
  $pa{SEQID} = "chr" . $pa{CHRNAME};  
  $pa{FILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                $pa{SEQID} . "/" . $pa{CHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n"
    unless (-e $pa{FILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n\n");
}

unless ($pa{RESET})
{
  die "\n ERROR: No restriction enzyme set was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
die "\n ERROR: cannot find restriction enzyme list.\n\n"
  unless (-e $BS->{enzyme_dir} . "/" . $pa{RESET} );
  
unless ($pa{CHUNKLENMIN} && $pa{CHUNKLENMAX})
{
  die "\n ERROR: Both a chunk minimnum and maximum size must be defined.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
my $xlationref = {}; #to speed degraded codon translation analysis;

$pa{REPATH} = $BS->{enzyme_dir} . $pa{RESET};
$pa{CODON_TABLE} = define_codon_table($SPECIES{$pa{SPECIES}});
$pa{REDBNAME} = $pa{CHROMOSOME} . "_RED";

##Configure MySQL access and initialize databases
my $DBLIST = list_databases($BS);
unless (exists($DBLIST->{$pa{CHROMOSOME}}))
{
  drop_database($pa{CHROMOSOME}, $BS);
  create_database($pa{CHROMOSOME}, $BS);
  load_database($pa{CHROMOSOME}, $BS);  
}

## find genes, find intergenic regions and add to database (unless they exist)
## Load the database for the original chromosome
my $RE_DATA = define_sites($BS->{enzyme_dir} ."/" . $pa{RESET});
my $db = fetch_database($pa{CHROMOSOME}, $BS);
my @genes  = $db->get_features_by_type("gene");
my @exons   = $db->get_features_by_type("CDS");
my $chromosome = $db->fetch_sequence($pa{SEQID});
my $chrlen = length($chromosome);
my $mask = make_mask(length($chromosome), \@genes);

if ($pa{REDOREDB} || !exists($DBLIST->{$pa{REDBNAME}}))
{
  print "Creating RE database...\n";
  drop_database($pa{REDBNAME}, $BS) if exists($DBLIST->{$pa{REDBNAME}});
  create_database($pa{REDBNAME}, $BS);
  my $dbh = DBI->connect(
      "dbi:mysql:$pa{REDBNAME}", 
      $BS->{mysql_user}, 
      $BS->{mysql_pass}, 
      { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $query = ("CREATE TABLE IF NOT EXISTS `$pa{REDBNAME}`.`positions` (
      `id` INT NOT NULL AUTO_INCREMENT,
      `name` VARCHAR(45) NOT NULL ,
      `presence` VARCHAR(1) NOT NULL ,
      `eligible` VARCHAR(1) NULL , 
      `start` INT NOT NULL ,
      `end` INT NOT NULL ,
      `enzyme` VARCHAR(45) NOT NULL ,
      `feature` VARCHAR(100) NOT NULL ,
      `peptide` VARCHAR(45) NULL ,
      `overhangs` TEXT NULL ,
      `strand` VARCHAR(3) NOT NULL ,
      `overhangoffset` INT NULL, 
      PRIMARY KEY (`id`), 
      INDEX `ENZYME` (`enzyme` ASC), 
      INDEX `POSITION` (`start` ASC, `end` ASC)
    ) ENGINE = InnoDB;");
  $dbh->do($query);

  ## Create and populate the suffix tree of restriction enzyme recognition sites
  print "Loading restriction enzymes... ";
  my @enzlist = keys %{$$RE_DATA{CLEAN}};
  my %lenhsh = map {$_ => length($$RE_DATA{CLEAN}->{$_})} @enzlist;
  my @ordlen = sort {$lenhsh{$b} <=> $lenhsh{$a}} keys %lenhsh;
  my $maxenzlen = $lenhsh{$ordlen[0]};
  my $treelist = build_suffix_tree(\@enzlist, $RE_DATA, $pa{CODON_TABLE});
  print scalar(@enzlist), " restriction enzymes loaded (up to $maxenzlen bp).\n";
  print scalar(@$treelist), " suffix trees created.\n\n";

  my %types = map {$_->method => 1} $db->types();
  unless (exists $types{"intergenic_sequence"})
  {
    my %intergen = @{mask_filter($mask)};
    foreach (keys %intergen)
    {
      my $start = $_;
      my $end = $intergen{$_};
      my $f = $db->new_feature(
          -display_name  => "igenic_" . $start . "-" . $end,
          -start => $start,
          -end  => $end,
          -strand => 0,
          -seq_id => $pa{SEQID},
          -type  => "intergenic_sequence",
          -source => "BIO");
      #to catch lost enzymes at the interfaces:
      $f->start($f->start - $maxenzlen);
      $f->start(1) if ($f->start <= 0);
      $f->end($f->end + $maxenzlen);
      $f->end(length($chromosome)) if ($f->end > length($chromosome));
      $db->store($f);
    }
  }
  my @igenics = $db->get_features_by_type("intron", "intergenic_sequence");
  print scalar(@exons), " exons and ",  
  scalar(@igenics), " intergenic regions parsed and queued for processing.\n\n";

  ## Begin feature processing
  my @features;
  push @features, @igenics;
  push @features, @exons;
  ## If there are defined boundaries, don't process anything too far outside them
  if ($pa{STARTPOS})
  {
    @features = grep {$_->start >= $pa{STARTPOS} - $pa{CHUNKLENMAX}} @features;
  }
  if ($pa{STOPPOS})
  {
    @features = grep {$_->start <= $pa{STOPPOS} + $pa{CHUNKLENMAX}} @features;
  }
  @features = sort {$a->start - $a->end <=> $b->start - $b->end} @features;
  print "Processing ", scalar(@features), " features...\n";

  ## Print results out to a text file that will be fastloaded into MySQL
  my ($x, $y) = (1, 0);
  my $filename = $BS->{tmp_dir} . "/" . $pa{CHROMOSOME} . "_RED.out";
  chmod 0777, $filename;
  open OUT, ">$filename";
  foreach my $feature ( @features ) 
  { 
    my $zfeat = $feature->display_id();
    my $ref = undef;
    if ($feature->type =~ "CDS")
    {
      $ref = search_suffix_tree($treelist, [$feature], 
                                $RE_DATA, $pa{CODON_TABLE}, $xlationref);
    }
    else
    {
      $ref = search_DNA([$feature], $chromosome, $RE_DATA, $pa{CODON_TABLE});
    }
    if ($ref)
    {
      foreach my $ar (@$ref)
      {
        my ($enzname, $start, $presence, $ohang, 
            $feat, $peptide, $ohangstart, $strand) = @$ar;
        my $zid = $presence ne "i"  ? $enzname . "_" . $start . "_" . 
                  $peptide  : $enzname . "_" . $start;
        my $zend = $start + length($$RE_DATA{CLEAN}->{$enzname}) - 1;
        my $zhangs = join(",", keys %$ohang);
        my $zstrand =  $strand == 2 || $strand == -1  ? -1  : 1;
        my $zpep = $peptide ? $peptide  : "NULL";
        print OUT "$zid.$presence.$start.$zend.$enzname.$zfeat.";
        print OUT "$zpep.$zhangs.$zstrand.$ohangstart\n";
        $y++;
      }
    }
    $x++;
    print "Processed $x features...\n" if ($x % 100 == 0);
  }
  print "found $y restriction enzyme sites.\n\n";
  close OUT;

  ## Fastload the MySQL database
  print "Loading database...\n";
  my $table = $pa{REDBNAME} . ".positions";
  my $command = "LOAD DATA INFILE \"$filename\" INTO TABLE $table ";
  $command .= "FIELDS TERMINATED BY '.' LINES TERMINATED BY '\n' ";
  $command .= "(name, presence, start, end, enzyme, feature, peptide, ";
  $command .= "overhangs, strand, overhangoffset);";
  my $sth = $dbh->prepare($command) or die ($dbh->errstr."\n");
  $sth->execute or die ($dbh->errstr."\n");
  $sth->finish;
  $dbh->disconnect();
}

## Gather genes and check for essentiality.  
## Find the right telomere and make the junction just before it

my @UTCS = $db->features(-seq_id => $pa{SEQID}, -type => "UTC");
@UTCS = sort {$b->start <=> $a->start} @UTCS;
my $rutc = $UTCS[0];
my $chrend = $rutc  ? $rutc->start-1  : $chrlen;
my $limit = $pa{STOPPOS}  ? $pa{STOPPOS}+ $pa{CHUNKLENMAX}  : $chrend;
$limit = $chrend if ($limit > $chrend);
my $start = $pa{STARTPOS} ? $pa{STARTPOS}-$pa{CHUNKLENMAX}  : 1;
$start = 1 if ($start < 1);

$pa{GENEMASK} = make_mask(length($chromosome), \@exons);
my %ESSENTIALS;
foreach my $CDS (@exons)
{
  my @parents = $db->get_feature_by_name($CDS->Tag_parent_id);
  my $parent = $parents[0];
  my $status = $parent->Tag_essential_status;
  $ESSENTIALS{$CDS->Tag_load_id} = $status;
}

my $cheader = "DELETE from positions where id in ";
my $iheader = "UPDATE positions set `eligible` = \"n\" where id in ";

##Begin culling
my $position = $start;
my $drcount = 0;
my $igcount = 0;
my $ticker = 0;
while ($position <= $chrlen)
{
  print "@ $position bp\n" if ($ticker % 50 == 0);
  my ($culls, $bads) = Score($position, \%pa);
  my $ccount = scalar(@$culls);
  if ($ccount)
  {
    my $cmdstr = sqlizer($culls, $cheader);
    my @cmds = split(";\n\n", $cmdstr);
    db_execute($_) foreach (@cmds);
    $drcount += $ccount;
  }
  my $icount = scalar(@$bads);
  if ($icount)
  {
    my $cmdstr = sqlizer($bads, $iheader);
    my @cmds = split(";\n\n", $cmdstr);
    db_execute($_) foreach (@cmds);
    $igcount += $icount;
  }
  $position += $pa{CULLINC};
  print "\t$drcount culls, $igcount ignores so far...\n" if ($ticker % 50 == 0);
  $ticker++;
}
print  "dropped $drcount sites and marked $igcount sites ignorable.\n\n";

#my $left = $y - ($drcount + $igcount);
#print "Database contains $left restricion site possibilities\n";
##finish - cleanup database with "intergenic sequence" features
load_database($pa{CHROMOSOME}, $BS);

exit;

#uses globals $RE_DATA and $ESSENTIALS
# Given a position along the chromosome, find sites that will never be eligible 
# for landmark status. If the site is "p", remove it from the db.  If the site
# is "e" or "i", mark it ineligible in the db so it can be skipped in future
# analyses.
sub Score
{
	my ($position, $pa) = @_;
  my $lcpos = $position - 2000;
  $lcpos = 1 if ($lcpos < 0);
  my $rcpos = $position + 2000;  
  my $lef = $position - ($$pa{CHUNKLENMIN}+2000);
  $lef = 1 if ($lef <= 0);
  my $rig = $position + ($$pa{CHUNKLENMIN}+2000);                                               
  #print "\tlooking for REs between $lcpos and $rcpos...\n";
  my $pool = SelectPositions ($lef, $rig);
  my @candidates = grep {$_->{START} >= $lcpos && $_->{END} <= $rcpos} @$pool;
  my @culls;
  my @ignores;
  #print "\tvetting ", scalar(@candidates), " enzymes...\n";
  foreach my $re (@candidates)
  {
    next if ($re->{IGNORE}); 
    my $enz = $re->{ENZ};
    my $pre = $re->{PRE};
    my $size = $re->{END} - $re->{START} + 1;
    my $maskbit = substr($$pa{GENEMASK}, $re->{START} - 1, $size);
    if ($maskbit =~/[^1]/) #DROP: exonic in exon overlap
    {
      push @culls, $re->{ID} if ($re->{PRE} eq "p");
      push @ignores, $re->{ID} if ($re->{PRE} ne "p");
      next;
    }
    my @buds = grep {abs($_->{START} - $re->{START}) <= $$pa{CHUNKLENMIN} 
                  && $_->{PRE} ne "p" && $_->{ENZ} eq $enz 
                  && $_->{NAME} ne $re->{NAME}} @$pool;
    my @igenics = grep {$_->{PRE} eq "i"} @buds;
    if (scalar(@igenics)) #DROP: too many intergenics around
    {
      push @culls, $re->{ID} if ($re->{PRE} eq "p");
      push @ignores, $re->{ID} if ($re->{PRE} ne "p");
      next;
    }
    my $gene = $pre ne "i"  ? $re->{FEAT}  : "";
    my @exonics = grep {$_->{PRE} eq "e"} @buds;
    my $ess_flag = exists($ESSENTIALS{$gene}) && $ESSENTIALS{$gene} eq "Essential"   ? 1 : 0;
    my $fgw_flag = exists($ESSENTIALS{$gene}) && $ESSENTIALS{$gene} eq "fast_growth" ? 1 : 0;
    my $lap_flag = 0;
    my @movers;    
    foreach my $ex (@exonics)
    {
      my $egene = $ex->{FEAT};
      my $emaskbit = substr($$pa{GENEMASK}, $ex->{START} - 1, $size);
      $lap_flag++ if ($emaskbit =~/[^1]/);
      $ess_flag++ if (exists ($ESSENTIALS{$gene}) && $ESSENTIALS{$egene} eq "Essential");
      $fgw_flag++ if (exists ($ESSENTIALS{$gene}) && $ESSENTIALS{$egene} eq "fast_growth");
      push @movers, $ex->{NAME};
    }
    if ($lap_flag != 0) #DROP: modification in exon overlap
    {
      push @culls, $re->{ID} if ($re->{PRE} eq "p");
      push @ignores, $re->{ID} if ($re->{PRE} ne "p");
      next;
    }
    #my $distance = abs($position - $re->{START});
    #my $score = $distance <= 2000 ?	0	:	0.002  * $distance - 4;               
    # Plus the log of the price per unit    
    my $score   = log($$RE_DATA{SCORE}->{$enz});                                
    # Plus one tenth point for each orf modified
    $score   += 0.1 * scalar(@movers);                                          
    # Plus one half point for each fast growth orf modified
    $score   += 0.5 * $fgw_flag;										                            
    # Plus one point for each essential orf modified
    $score   += 1.0 * $ess_flag;											                          

    if ($score > 1) #DROP: score too high (>1)
    {
      push @culls, $re->{ID} if ($re->{PRE} eq "p");
      push @ignores, $re->{ID} if ($re->{PRE} ne "p");                          
      next;
    }
  }
  return (\@culls, \@ignores);
}

sub SelectPositions 
{
  my ($left, $right) = @_;
  my $dbh = DBI->connect("dbi:mysql:$pa{REDBNAME}", $BS->{mysql_user}, 
            $BS->{mysql_pass}, { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $sth = $dbh->prepare
  (
    "SELECT id, name, presence, eligible, start, end, 
            enzyme, feature, overhangs, strand 
     FROM positions
     WHERE start >= $left and end <= $right"
  ) or die ($dbh->errstr."\n");
  $sth->execute or die ($dbh->errstr."\n");
  my @res = ();
  while (my $aref = $sth->fetchrow_arrayref)
  {
    my ($id, $name, $presence, $eligible, $start, 
        $end, $enzyme, $feature, $overhangs, $strand) = @$aref;
    my $newfeat = {};
    $newfeat->{ID} = $id;
    $newfeat->{NAME} = $name;
    $newfeat->{PRE} = $presence;
    $newfeat->{IGNORE} = $eligible;
    $newfeat->{START} = $start;
    $newfeat->{END} = $end;
    $newfeat->{ENZ} = $enzyme;
    $newfeat->{FEAT} = $feature;
    $newfeat->{HANG} = $overhangs;
    $newfeat->{STRAND} = $strand;
    push @res, $newfeat;
  }
  $sth->finish;
  $dbh->disconnect();  
  return \@res;
}

sub db_execute
{
  my ($command) = @_;
  my $dbh = DBI->connect("dbi:mysql:$pa{REDBNAME}", $BS->{mysql_user}, 
        $BS->{mysql_pass}, { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $sth = $dbh->prepare($command) or die ($dbh->errstr . "\n");
  $sth->execute or die ($dbh->errstr . "\n");
  $sth->finish;
  $dbh->disconnect();
}

sub sqlizer
{
  my ($arrref, $header) = @_;
  my $newstr = $header . " (" if (scalar(@$arrref));
  my $counter = 1;
  foreach my $ins (@$arrref)
  {
    if ($counter % 500 == 0 && $counter < scalar(@$arrref))
    {
      $ins .=   ");\n\n$header (";
    }
    elsif ($counter == scalar(@$arrref))
    {
      $ins .= ");\n\n";
    }
    else
    {
      $ins .= ", ";
    }
    $newstr .= $ins;
    $counter++;
  }
  $newstr;
}

__END__

=head1 NAME

  BS_GlobalREMarkup.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility creates an exhaustive database of restriction enzyme recognition 
   sites along a chromsome, both existing and potential.
  Every intergenic sequence is parsed for existing recognition sites. Those that
   are found are marked (i)mmutable; A suffix tree is created from all possible 
   6-frame translations of restriction enzyme recognition sites, such that each 
   node in the tree is an amino acid string that may be reverse translated to be
   a recognition site. Every exonic sequence in the chromosome is then searched
   with the suffix tree both for (e)xisting recognition sites and for sites
   where a (p)otential recognition site could be introduced without changing the
   protein sequence of the gene. As long as they occur within protein coding 
   genes, existing and potential recognition sites may be manipulated to yield 
   any of several different overhangs, all of which are computed by the 
   algorithm. 
  A score is assigned to every restriction enzyme site. The score is a function
   of the log of the enzyme's price per unit, plus one tenth for each orf that 
   must be modified to make the enzyme unique within the range of the chunk 
   size, plus one half for each fast growth orf modified, plus one for each 
   essential orf modified.
  Every extant site is indexed so that later in the design process, when 
    potential sites are considered, the number of sites that must be modified is
    a known contribution to the cost. However, all potential sites that could
    not be made unique under any circumstances are culled from the database,
    with a window size that defaults to 2000 base pairs but may be defined by 
    the user.

=head1 ARGUMENTS

Required arguments:

  -CHR, --CHROMOSOME : The chromosome to be modified
  --RESET : Which list of restriction enzymes to use (config/enzymes)
  --CHUNKLENMIN : The minimum size of chunks to be designed
  --CHUNKLENMAX : The maximum size of chunks to be designed

Optional arguments:

  --REDOREDB : Whether or not to reform the RE database from scratch
  -CU,  --CULLINC : The width of the culling window
  -h,   --help : Display this message

=cut
