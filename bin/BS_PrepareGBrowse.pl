#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GBrowse qw(:all);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_PrepareGBrowse_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'CHROMOSOME=s'  => \$pa{CHROMOSOME},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

die ("This installation of BioStudio is not using GBrowse.\n") 
  unless ($BS->{enable_gbrowse});  
  
unless ($pa{CHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
  die;
}

if ($pa{CHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}) = ($1, $2);
  $pa{SEQID} = "chr" . $pa{CHRNAME};  
  $pa{FILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" .
                $pa{SEQID} . "/" . $pa{CHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the genome repository.\n"
    unless (-e $pa{FILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}


################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{BSVERSION} = $bsversion;

my $DBLIST = list_databases($BS);
drop_database($pa{CHROMOSOME}, $BS) if exists($DBLIST->{$pa{CHROMOSOME}});
create_database($pa{CHROMOSOME}, $BS);
load_database($pa{CHROMOSOME}, $BS);

################################################################################
############################### Loading GBrowse ################################
################################################################################

write_conf_file($BS, $pa{CHROMOSOME}, "");

exit;

__END__

=head1 NAME

  BS_PrepareGBrowse.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility takes a chromosome from the BioStudio genome repository and loads
   it into MySQL, creates a GBrowse configuration file, and edits the 
   GBrowse.conf file so that it may be viewed in and edited from GBrowse.

=head1 ARGUMENTS

Required arguments:

  -C, --CHROMOSOME : The chromosome to be loaded into GBrowse.  Must be in the
        BioStudio genome repository.
  
Optional arguments:

  -h, --help : Display this message
  
=cut
