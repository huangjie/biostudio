#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::Codons qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GFF3 qw(:all);
use Bio::BioStudio::GBrowse qw(:all);
use Time::Format qw(%time);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_CodonJuggler_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'OLDCHROMOSOME=s'   => \$pa{OLDCHROMOSOME},
      'EDITOR=s'          => \$pa{EDITOR},
      'MEMO=s'            => \$pa{MEMO},
      'SCALE=s'           => \$pa{SCALE},
      'SCOPE=s'           => \$pa{SCOPE},
      'STARTPOS=i'        => \$pa{STARTPOS},
      'STOPPOS=i'         => \$pa{STOPPOS},
      'FROM=s'            => \$pa{FROM},
      'TO=s'              => \$pa{TO},
      'DUBWHACK'          => \$pa{DUBWHACK},
      'VERWHACK'          => \$pa{VERWHACK},
      'OUTPUT'            => \$pa{OUTPUT},
			'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

$pa{SCALE} = "chrom" unless $pa{SCALE};
$pa{SCOPE} = "chrom" unless $pa{SCOPE};
$pa{OUTPUT} = "txt" unless ($pa{OUTPUT} && $pa{OUTPUT} eq "html");
$pa{BSVERSION} = $bsversion;

unless ($pa{OLDCHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{OLDCHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{OLDFILE} = get_src_path($pa{OLDCHROMOSOME}, $BS);
  die "\n ERROR: Can't find the chromosome in the repository.\n" 
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

unless ($pa{EDITOR} && $pa{MEMO})
{
  print "\n ERROR: Both an editor's id and a memo must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

unless ($pa{FROM} && $pa{TO})
{
  die "\n ERROR: Two codons must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
($pa{FROM}, $pa{TO}) = (uc $pa{FROM}, uc $pa{TO});
if (length($pa{FROM}) != 3 || length($pa{TO}) != 3)
{
  die "\n ERROR: Both codons must be three bases long.\n";
}

my $CODON_TABLE = define_codon_table($SPECIES{$pa{SPECIES}});
unless (exists $$CODON_TABLE{$pa{FROM}} && exists $$CODON_TABLE{$pa{TO}})
{
  print "\n ERROR: At least one of the specified codons doesn't exist in ";
  print " the codon table. Make sure the codons contain only the characters ";
  print "A, T, C, and G.\n";
  die();
}

if ($pa{FROM} eq $pa{TO})
{
  print "\n ERROR: The two codons you selected are the same; no editing ";
  print "will be done.\n";
  die();
}

if ($$CODON_TABLE{$pa{FROM}} ne $$CODON_TABLE{$pa{TO}})
{
  print "\n WARNING: The two codons you have selected encode different ";
  print "residues in $pa{SPECIES}.\n";
  die();
}

if ($pa{SCOPE} && $pa{SCOPE} eq "seg" && 
  ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS}))
{
  print "\n ERROR: The start and stop coordinates do not parse and scope ";
  print "is set to seg.\n";
  die();
}

################################################################################
################################# CONFIGURING ##################################
################################################################################

my $DBLIST = list_databases($BS);
$pa{INTRO}    = $pa{GENVER} . "_" . $pa{VERNAME};
$pa{COMMENTS} = get_GFF_comments($pa{OLDCHROMOSOME}, $BS);

if ($pa{SCALE} eq "genome")
{
  $pa{GENVER}++;
}
elsif ($pa{SCALE} eq "chrom")
{
  $pa{VERNAME}++;
  $pa{VERNAME} = "0" . $pa{VERNAME} while (length($pa{VERNAME}) < 2);
}
$pa{NEWCHROMOSOME}  = $pa{SPECIES} . "_" . $pa{SEQID} . "_" 
                    . $pa{GENVER} . "_" . $pa{VERNAME};
$pa{NEWFILE}        = get_src_path($pa{NEWCHROMOSOME}, $BS);

system("cp $pa{OLDFILE} $pa{NEWFILE}");
system("chmod 777 $pa{NEWFILE}");

print "CONFIGURING DBS...\n";
unless (exists ($DBLIST->{$pa{OLDCHROMOSOME}}) )
{
  create_database($pa{OLDCHROMOSOME}, $BS);
  load_database($pa{OLDCHROMOSOME}, $BS);
}
drop_database($pa{NEWCHROMOSOME}, $BS) 
  if exists($DBLIST->{$pa{NEWCHROMOSOME}});
create_database($pa{NEWCHROMOSOME}, $BS);
load_database($pa{NEWCHROMOSOME}, $BS);
$pa{DB} = fetch_database($pa{NEWCHROMOSOME}, $BS);

my @genes = $pa{DB}->features(
    -seq_id     => $pa{SEQID}, 
    -range_type => 'contains', 
    -types      => 'gene', 
    -start      => $pa{STARTPOS}, 
    -end        => $pa{STOPPOS});
my %dogenes = map {$_->Tag_load_id => $_} @genes;
$pa{DOGENES} = \%dogenes if ($pa{SCOPE} eq "seg");
$pa{REPORT} = {};
  
my @allgenes  = $pa{DB}->get_features_by_type("gene");
my %geneinf   = map {$_->Tag_load_id => $_} @genes;
$pa{GENEINF}  = \%geneinf;
$pa{ALLORFS}  = ORF_compile(\@allgenes);
$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
$pa{EDITCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});

# compile CDSes to individual gene masks and a big chromosome mask
$pa{GENEMASKS} = {};
my @CDSes;
foreach my $gene (@allgenes)
{
  my @exons = $gene->get_SeqFeatures("CDS");
  $pa{GENEMASKS}->{$gene->Tag_load_id} 
            = make_mask($gene->end - $gene->start + 1, \@exons, $gene->start-1);
  push @CDSes, @exons;
}
$pa{CHRMASK} = make_mask(length($pa{ORIGCHR}), \@CDSes);

# make display names for the genes for the summary later
$pa{GENEDISPLAY} = $BS->{enable_gbrowse} 
  ? gbrowse_gene_names(\@genes, \%pa, $BS)
  : gene_names(\@genes, $BS);
$pa{NEWGFF} = [];

$pa{SWAPTYPE}  = codon_change_type($pa{FROM}, $pa{TO}, $CODON_TABLE);
$pa{CODOLAPS}  = allowable_codon_changes($pa{FROM}, $pa{TO}, $CODON_TABLE);

#If wiki is in use, create wiki topics
wiki_edit_prep(\%pa, $BS, [$pa{SWAPTYPE}]) if ($BS->{enable_wiki});

################################################################################
################################### EDITING ####################################
################################################################################

print "EDITING...\n";
my %state;
my $genemasks = {};
my @newcomments;

foreach my $gname (keys %{$pa{GENEINF}})
{
  $state{$gname} = 2;
  $pa{REPORT}->{$gname} = [];
  if ($pa{SCOPE} eq "seg" && ! exists $pa{DOGENES}->{$gname})
  {
    $state{$gname} = 4;
  }
}

print "Swapping codons...\n";
my %moddedgenes = ();
my @todos = sort {$pa{GENEINF}->{$a}->start <=> $pa{GENEINF}->{$b}->start}
            grep {$state{$_} == 2} 
            keys %{$pa{GENEINF}};
foreach my $gname (@todos)
{
  my $gene = $pa{GENEINF}->{$gname};
  my (@cannotchange, @changed) = ( (), ());
  $genemasks->{$gname} = substr($pa{CHRMASK}, $gene->start-1, $gene->end - $gene->start +1);
  my $umask = mask_combine($pa{GENEMASKS}->{$gname}, $$genemasks{$gname});
  my @ubermask = split("", $umask);
  my $x = 0;
  while ($x < scalar(@ubermask))
  {
    $x++ while ($ubermask[$x] < 2 && $x < scalar(@ubermask)-2);
    last if ($x >= scalar(@ubermask)-2);
    my $basea = $x + $gene->start;
    my $maska = $ubermask[$x];
    $x++;
    $x++ while ($ubermask[$x] < 2);
    my $baseb = $x + $gene->start;
    my $maskb = $ubermask[$x];
    $x++;
    $x++ while ($ubermask[$x] < 2);
    my $basec = $x + $gene->start;
    my $maskc = $ubermask[$x];
    my $cod = substr($pa{ORIGCHR}, $basea-1, 1) 
            . substr($pa{ORIGCHR}, $baseb-1, 1) 
            . substr($pa{ORIGCHR}, $basec-1, 1);
    $x++;
    $cod = complement($cod, 1) if ($gene->strand == -1);
    if ($pa{FROM} eq $cod)
    {
      #All three bases not in overlaps
      if ($maska == 2 && $maskb == 2 && $maskc == 2)
      {
        push @changed, [$basea, $baseb, $basec];
        my $newcod = $pa{TO};
        my $oldcod = $pa{FROM};
        $newcod = complement($newcod, 1) if ($gene->strand == -1);
        $oldcod = complement($oldcod, 1) if ($gene->strand == -1);
        my $oldaa = $$CODON_TABLE{$pa{FROM}};
        my $newaa = $$CODON_TABLE{$pa{TO}};
        my $offset = $basea - $gene->start + 1;
        my $name = $gname . "_" . $oldaa . $offset . $newaa;
        my $newfeat = $pa{DB}->new_feature(
          -start        => $basea,
          -end          => $basec,
          -primary_tag  => $pa{SWAPTYPE},
          -seq_id       => $pa{SEQID},
          -source       => "BIO",
          -attributes   => {
              'wtseq'     => $oldcod, 
              'newseq'    => $newcod, 
              'wtpos'     => $basea,
              'intro'     => $pa{INTRO},
              'load_id'   => $name,
              'parent_id' => $gname,
              'bsversion' => $pa{BSVERSION}}
        );
        $gene->add_SeqFeature(($newfeat));

        my $comment = "# # $pa{SWAPTYPE} " . $newfeat->Tag_load_id . " added\n";
        push @newcomments, $comment;
        substr($pa{EDITCHR}, $basea-1, 1) = substr($newcod, 0, 1);
        substr($pa{EDITCHR}, $baseb-1, 1) = substr($newcod, 1, 1);
        substr($pa{EDITCHR}, $basec-1, 1) = substr($newcod, 2, 1);
        $pa{REPORT}->{$gname}->[0]++;
        
        if ($BS->{enable_wiki})
        {
          # * is illegal in FosWiki topic names.  replace * with X for the wiki
          my $wikiid = $newfeat->Tag_load_id;
          $wikiid =~ s/\*/X/g;
          push @{$pa{WIKICOMMENTS}}, "# # $pa{SWAPTYPE} [[$wikiid]] added\n";
          $newfeat->remove_tag("load_id");
          $newfeat->add_tag_value("load_id", $wikiid);
          wiki_add_feature(\%pa, $BS, $newfeat);
          
          my $flag = exists $moddedgenes{$gname}  ? 1 : 0;
          my $add = "&nbsp;&nbsp;[[$wikiid]]<br>\n";
          my $note = "<br>Modified codons:<br>\n";
          wiki_update_feature(\%pa, $BS, $gene, $add, $flag, $note);
          
          $moddedgenes{$gname}++;
        }
        
      }
      elsif ($maskc - $maska > 2)
      {
        push @cannotchange, [$basea, $baseb, $basec];
        $pa{REPORT}->{$gname}->[2] .= "cannot change codon at $basea ";
        $pa{REPORT}->{$gname}->[2] .= "(intron AND overlap) ";
      }
      elsif ($maska == 3 && $maskb == 3 && $maskc == 3)
      {
        my $genstatus = $gene->Tag_orf_classification;
      #Find the overlapping gene and its overlapping CDS
        my @laps = grep {$gene->overlaps($pa{GENEINF}->{$_}) && $gname ne $_} 
                  keys %{$pa{GENEINF}};
        my @genelaps = grep {$pa{GENEINF}->{$_}->start <= $basea && $pa{GENEINF}->{$_}->end >= $basea} @laps;
        my $genelap = $genelaps[0];
        my $genelapfeat = $pa{GENEINF}->{$genelap};
        my $lapstatus = $genelapfeat->Tag_orf_classification;
        my @lapsubfeats = flatten_subfeats($genelapfeat);
        my @lapCDSes = grep {$_->primary_tag eq "CDS" && $gene->overlaps($_)} @lapsubfeats;
        my $CDSlap = $lapCDSes[0];
        my $phase = $CDSlap->phase  ? $CDSlap->phase  :  0;
        my $CDSseq = get_feature_sequence($CDSlap, $pa{ORIGCHR});
      #Determine frame
        my $frame = ($basea - ($CDSlap->start + $phase)) % 3;
        my $length = $basec - ($basea - $frame) + 1;
        $length++ while ($length % 3 != 0);
        my $syncodstart = $basea - $CDSlap->start - $frame + 1;
        my $inframe = substr($CDSseq, $syncodstart-1, $length);
        my $lappep = $CDSlap->strand == 1
          ?  translate($inframe, 1, $CODON_TABLE)
          :  translate(complement($inframe, 1), 1, $CODON_TABLE);
        my $orientswit = $genelapfeat->strand eq $gene->strand  ?  0  :  1;
        my %allowed = %{$pa{CODOLAPS}->{$orientswit}};
        if (exists $allowed{$lappep} 
          || ($pa{DUBWHACK} && $lapstatus eq "Dubious" && $genstatus ne "Dubious")
          || ($pa{VERWHACK} && $genstatus ne "Dubious"))
        {
          push @changed, [$basea, $baseb, $basec];
          my $newcod = $pa{TO};
          my $oldcod = $pa{FROM};
          $newcod = complement($newcod, 1) if ($gene->strand == -1);
          $oldcod = complement($oldcod, 1) if ($gene->strand == -1);
          my $oldaa = $$CODON_TABLE{$pa{FROM}};
          my $newaa = $$CODON_TABLE{$pa{TO}};
          my $offset = $basea - $gene->start + 1;
          my $name = $gname . "_" . $oldaa . $offset . $newaa;
          my $newfeat = $pa{DB}->new_feature(
            -start => $basea,
            -end   => $basec,
            -primary_tag => $pa{SWAPTYPE},
            -seq_id => $pa{SEQID},
            -source => "BIO",
            -attributes => {
                'wtseq' => $oldcod, 
                'newseq' => $newcod, 
                'wtpos' => $basea,
                'intro' => $pa{INTRO},
                'load_id' => $name,
                'parent_id' => $gname,
                'bsversion' => $pa{BSVERSION}
            }
          );
          $gene->add_SeqFeature(($newfeat));
            
          push @newcomments, "# # $pa{SWAPTYPE} " . $newfeat->Tag_load_id . " added\n";
          substr($pa{EDITCHR}, $basea-1, 1) = substr($newcod, 0, 1);
          substr($pa{EDITCHR}, $baseb-1, 1) = substr($newcod, 1, 1);
          substr($pa{EDITCHR}, $basec-1, 1) = substr($newcod, 2, 1);
          $pa{REPORT}->{$gname}->[0]++;
          if ($BS->{enable_wiki})
          {
            # * is illegal in FosWiki topic names. replace * with X for the wiki
            my $wikiid = $newfeat->Tag_load_id;
            $wikiid =~ s/\*/X/g;
            push @{$pa{WIKICOMMENTS}}, "# # $pa{SWAPTYPE} [[$wikiid]] added\n";
            $newfeat->remove_tag("load_id");
            $newfeat->add_tag_value("load_id", $wikiid);
            wiki_add_feature(\%pa, $BS, $newfeat);

            my $flag = exists $moddedgenes{$gname}  ? 1 : 0;
            my $add = "&nbsp;&nbsp;[[$wikiid]]<br>\n";
            my $note = "<br>Modified codons:<br>\n";
            wiki_update_feature(\%pa, $BS, $gene, $add, $flag, $note);
            
            $moddedgenes{$gene->Tag_load_id}++;
          }
          unless ($lappep eq $$CODON_TABLE{$pa{TO}})
          {
            my $newCDSseq = get_feature_sequence($CDSlap, $pa{EDITCHR});
            my $newframe = substr($newCDSseq, $syncodstart-1, $length);
            for (my $offset = 0; $offset <= $length-3; $offset += 3)
            {
              my $oldsyncod = substr($inframe, $offset, 3);
              my $newsyncod = substr($newframe, $offset, 3);
              my $pos = $syncodstart + $offset + $CDSlap->start;
              if ($oldsyncod ne $newsyncod)
              {
                my $newcod = $newsyncod;
                my $oldcod = $oldsyncod;
                $newcod = complement($newcod, 1) if ($genelapfeat->strand == -1);
                $oldcod = complement($oldcod, 1) if ($genelapfeat->strand == -1);
                my $oldaa = $$CODON_TABLE{$oldcod};
                my $newaa = $$CODON_TABLE{$newcod};
                my $swaptype = codon_change_type($oldcod, $newcod, $CODON_TABLE);
                my $offset = $pos - $genelapfeat->start + 1;
                my $name = $genelap . "_" . $oldaa . $offset . $newaa;
                $newcod = complement($newcod, 1) if ($genelapfeat->strand == -1);
                $oldcod = complement($oldcod, 1) if ($genelapfeat->strand == -1);
                my $modnote = "Modified to accommodate $pa{FROM} to $pa{TO} change in $gname";
                my $newsfeat = $pa{DB}->new_feature(
                  -start => $pos-1,
                  -end   => $pos+1,
                  -primary_tag => $swaptype,
                  -seq_id => $pa{SEQID},
                  -source => "BIO",
                  -attributes => {
                      'wtseq' => $oldcod, 
                      'newseq' => $newcod, 
                      'wtpos' => $pos,
                      'intro' => $pa{INTRO},
                      'load_id' => $name,
                      'parent_id' => $genelap,
                      'bsversion' => $pa{BSVERSION},
                      'desc' => $modnote
                  }
                );
                $genelapfeat->add_SeqFeature(($newsfeat));
              #  $offset = $length;
                
                push @newcomments, "# # $swaptype " . $newsfeat->Tag_load_id . " added\n";
                $pa{REPORT}->{$genelap}->[2] .= "$modnote; ";
                if ($BS->{enable_wiki})
                {
                  wiki_edit_prep(\%pa, $BS, [$swaptype]);
                  
                  # * is illegal in FosWiki topic names. replace * with X
                  my $wikiid = $newsfeat->Tag_load_id;
                  $wikiid =~ s/\*/X/g;
                  push @{$pa{WIKICOMMENTS}}, "# # $swaptype [[$wikiid]] added\n";
                  $newsfeat->remove_tag("load_id");
                  $newsfeat->add_tag_value("load_id", $wikiid);
                  wiki_add_feature(\%pa, $BS, $newsfeat);
                  
                  my $flag = exists $moddedgenes{$genelap}  ? 1 : 0;
                  my $add = "&nbsp;&nbsp;[[$wikiid]]<br>\n";
                  my $note = "<br>Modified codons:<br>\n";
                  wiki_update_feature(\%pa, $BS, $genelapfeat, $add, $flag, $note);
                  
                  $moddedgenes{$genelap}++;
                }
              }
            }
          }
        }
        else
        {
          push @cannotchange, [$basea, $baseb, $basec];
          $state{$gname} = 3;
          $pa{REPORT}->{$gname}->[2] .= "cannot change codon at $basea ";
          $pa{REPORT}->{$gname}->[2] .= "($genstatus $gname\'s codon overlaps ";
          $pa{REPORT}->{$gname}->[2] .= "in bad frame of $lapstatus $genelap); ";
        }
      }  
      else
      {
        push @cannotchange, [$basea, $baseb, $basec];
        $state{$gname} = 3;
        $pa{REPORT}->{$gname}->[2] .= "cannot change codon at $basea ";
        $pa{REPORT}->{$gname}->[2] .= "(too many genes overlap); ";
        next;
      }
    }
  }
  if (scalar(@changed) + scalar(@cannotchange) == 0)
  {
    $state{$gname} = 0;
  } 
}


################################################################################
############################### ERROR  CHECKING ################################
################################################################################

#check fidelity of codon substitutions
foreach my $gname (keys %{$pa{GENEINF}})
{
  my $genfeat = $pa{GENEINF}->{$gname};
  my $newgeneseq = get_feature_sequence($genfeat, $pa{EDITCHR});
  my $oldgeneseq = get_feature_sequence($genfeat, $pa{ORIGCHR});
  if ($newgeneseq eq $oldgeneseq && $state{$gname} == 2)
  {
    $state{$gname} = -2;
    $pa{REPORT}->{$gname}->[2] .= "No sequence change is apparent!; ";
  }
	my $neworfseq = "";
  my @subs = flatten_subfeats($genfeat);
  my @CDSes = grep {$_->primary_tag eq "CDS"} @subs;
  @CDSes = sort {$b->start <=> $a->start} @CDSes if ($genfeat->strand == -1);
  @CDSes = sort {$a->start <=> $b->start} @CDSes if ($genfeat->strand == 1);
  foreach (@CDSes)
  {
    my $seq = substr($pa{EDITCHR}, $_->start - 1, $_->end - $_->start + 1);
    $seq = complement($seq, 1) if ($_->strand == -1);
    $neworfseq .= $seq;
  }
  my $newpep = translate($neworfseq, 1, $CODON_TABLE);
  my $oldpep = translate($pa{ALLORFS}->{$gname}, 1, $CODON_TABLE);
  if ($newpep ne $oldpep)
  {
    $state{$gname} = -2;
    $pa{REPORT}->{$gname}->[2] .= "A translation change has occured!; ";
  }
}

################################################################################
############################ WRITING AND REPORTING #############################
################################################################################

#Assemble new GFF file
#comments
my $comment  = "# $pa{NEWCHROMOSOME} created from $pa{OLDCHROMOSOME} ";
   $comment .= "$time{'yymmdd'} by $pa{EDITOR} ($pa{MEMO})\n";
unshift @newcomments, $comment;
my $womment  = "# [[$pa{NEWCHROMOSOME}]] created from [[$pa{OLDCHROMOSOME}]] ";
   $womment .= "$time{'yymmdd'} by [[Main.$pa{EDITOR}]] ($pa{MEMO})<br>";
unshift @{$pa{WIKICOMMENTS}}, $womment;

make_GFF3(\%pa, $BS, \@newcomments);
update_wiki($BS, \%pa, $pa{WIKICOMMENTS}) if ($BS->{enable_wiki});
update_gbrowse($BS, \%pa) if ($BS->{enable_gbrowse});

#check fidelity of newseq tags
foreach my $feature ($pa{DB}->features())
{
  my $flag = check_new_sequence($feature);
  if ($flag == 0)
  {
    print "WARNING - ", $feature->Tag_load_id, " has bad newseq tag; ";
    print $feature->Tag_newseq, " tag vs ", $feature->seq->seq, " actual\n";
  }
}
print "\n\n";

#Summarize the results
my @list = sort {$pa{GENEINF}->{$a}->start <=> $pa{GENEINF}->{$b}->start} 
              keys %state;

my @donegene = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} ==  2} @list;
if (@donegene)
{
  print "The following genes had all $pa{FROM} codons changed to $pa{TO}:\n";
  print genelist(\@donegene), "\n\n";
}

my @notdone  = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} ==  3} @list;
if (@notdone)
{
  print "The following genes did not have all $pa{FROM} codons changed ";
  print "(probably overlaps with other genes):\n";
  print genelist(\@notdone), "\n\n";
}

my @badswap  = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -2} @list;
if (@badswap)
{
  print "The following genes may have suffered translation changes ";
  print "in the process:\n";
  print genelist(\@badswap), "\n\n";
}

#my @nocodons = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} ==  0} @list;
#if (@nocodons)
#{
#  print "The following genes did not have any $pa{FROM} codons:\n";
#  print genelist(\@nocodons), "\n\n";
#}

print "Report:\n";
foreach my $gname (sort @list)
{
  my @results = @{$pa{REPORT}->{$gname}};
  next unless($results[0] || $results[1] || $results[2]);
  print $pa{GENEDISPLAY}->{$gname}, " : ";
  if ($results[0])
  {
    my $plural = $results[0] > 1  ? "s" : "";
    print "$results[0] $pa{FROM} codon$plural changed; " 
  }
  print "$results[1] codons changed on behalf of overlapping $pa{FROM codons}; " if ($results[1]);
  print "$results[2]" if ($results[2]);
  print "\n";
}

exit;

sub genelist
{
  my ($listref) = @_;
  my ($string, $count) = ("", 0);
  foreach my $item (@$listref)
  {
    $string .= $item;
    $count++;
    $string .= ", " if ($count < scalar(@$listref));
    $string .= "\n" if ($count % 10 == 0);
  }
  return $string;
}

sub codon_change_type
{
	my ($oldcod, $newcod, $CODON_TABLE) = @_;
	my $type = $$CODON_TABLE{$oldcod} eq $$CODON_TABLE{$newcod}
						?	$$CODON_TABLE{$oldcod} eq "*"
							?	"stop_retained_variant"
							:	"synonymous_codon"
						:	$$CODON_TABLE{$oldcod} eq "*"
							?	"stop_lost"
							:	$$CODON_TABLE{$newcod} eq "*"
								?	"stop_gained"
								:	"non_synonymous_codon";
	
	return $type;
}

__END__

=head1 NAME

  BS_CodonJuggler.pl
  
=head1 VERSION

    Version 1.00

=head1 DESCRIPTION

  This utility switches any one codon to any other. By default, it will not make
    any change to a gene that will cause a translation change in an overlapping
    gene; this behavior can be overridden with the -D and -V flags, which will 
    allow the utility to make nonsynonymous changes to ORFs marked dubious and
    verified, respectively.
    
	If a stop codon is changed to a different stop codon, the change will be 
	  marked "stop_retained_variant". Otherwise synonymous changes are marked
	  "synonymous_codon". If a stop is changed to a non stop, it is a "stop_lost".
	  If a non stop is changed to a stop it is "stop_gained"; any other change is
	  a "non_synonymous_codon".

=head1 USAGE

Required arguments:

  -O,  --OLDCHROMOSOME : The chromosome to be modified
  -E,  --EDITOR : The person responsible for the edits
  -M,  --MEMO   : Justification for the edits
  -F,  --FROM   : The codon to be replaced
  -T,  --TO     : The codon to be introduced

Optional arguments:

  -SCA, --SCALE : [genome, chrom (def)] Which version number to increment
  -SCO, --SCOPE : [seg, chrom (def)] How much sequence the edit will affect.
                  seg requires STARTPOS and STOPPOS.
  -STA, --STARTPOS : The first base for editing; ignored unless SCOPE = seg
  -STO, --STOPPOS  : The last base for editing; ignored unless SCOPE = seg
  -D,   --DUBWHACK : Allow nonsynonymous changes to dubious ORFs on behalf of 
                     non dubious ORFs
  -V,   --VERWHACK : Allow nonsynonymous changes to verified ORFs on behalf of
                     non dubious ORFs
  -O,   --OUTPUT   : [html, txt (def)] Format of reporting and output.
  -h,   --help : Display this message

=cut
