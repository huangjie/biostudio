#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::Foswiki qw(:all);
use Bio::BioStudio::GBrowse qw(&make_link);
use Time::Format qw(%time);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_PrepareWiki_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'CHROMOSOME=s'  => \$pa{CHROMOSOME},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

die ("This installation of BioStudio is not using a wiki.\n") 
  unless ($BS->{enable_wiki});

die ("The wiki root is missing - is Foswiki installed?\n")
  unless (-e $BS->{wiki_data});

if (! $pa{CHROMOSOME})
{
    print "\n ERROR: No chromosome was named.\n\n";
    pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
    die;
}

if ($pa{CHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}) = ($1, $2);
  $pa{SEQID} = "chr" . $pa{CHRNAME};  
  $pa{FILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                $pa{SEQID} . "/" . $pa{CHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n"
    unless (-e $pa{FILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

$pa{BSVERSION} = $bsversion;

$pa{DB} = fetch_database($pa{CHROMOSOME}, $BS);

die ("There's no database - has $pa{CHROMOSOME} been loaded?\n")
  unless ($pa{DB});
  
################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{COMMENTS} = get_GFF_comments($pa{CHROMOSOME}, $BS);

##Check for existance of species web
my $SPWEB = "BioStudio_" . $pa{SPECIES};
my $SPWEBPATH = $BS->{wiki_data} . $SPWEB;
unless( -e $SPWEBPATH)
{
  print "There's no species web...\n";
  my $linkreplace = undef;
  make_new_web($SPWEBPATH, $linkreplace, $BS);
}

##Check for existance of chromosome subweb
my $CHRTOPIC = "Chromosome$pa{CHRNAME}";
my $CHRPATH = $SPWEBPATH . "/" . $CHRTOPIC;
unless( -e $CHRPATH)
{
  print "There's no chromosome subweb...\n";
  my $linkreplace  = "\\\*All Features: \[\[Features\]\]\<br\>";
     $linkreplace .= "\\\*All Versions: \[\[Versions\]\]";
  make_new_web($CHRPATH, $linkreplace, $BS);
}

################################################################################
################################ LOADING  WIKI #################################
################################################################################
my @featuretypes = $pa{DB}->types;
@featuretypes = map {$_->method} @featuretypes;

my $FEATTOPICPATH = $CHRPATH . "/Features.txt";
make_new_topic($FEATTOPICPATH, "WebTopicList", 1, [], $BS);

my $VERTOPICPATH = $CHRPATH . "/Versions.txt";
make_new_topic($VERTOPICPATH, "WebTopicList", 1, [], $BS);

my $VERPATH = $CHRPATH . "/$pa{CHROMOSOME}.txt";
my @arr = @{$pa{COMMENTS}};
foreach my $line (@arr)
{
  $line =~ s/\n/\n\<br\>/;
}
my $authstr = "-- [[Main." . $BS->{wiki_user} . "]] - " 
              . $time{'dd Mon yyyy'} . "<br>";
my $verlink = "http://$BS->{this_server}/cgi-bin/gb2/gbrowse/$pa{CHROMOSOME}";
my $href = "<a href=\"$verlink\" target=\"_blank\" ";
  $href .= "style=\"text-decoration:none\">See this version in GBrowse</a><br>";
unshift @arr, $href if ($BS->{enable_gbrowse});
unshift @arr, $authstr;
make_new_topic($VERPATH, "Versions", 0, \@arr, $BS);

foreach my $type (@featuretypes)
{
  my $typetopic = $type . "s";
  substr($typetopic, 0, 1) = uc substr($typetopic, 0, 1);
  my $CHRTYPEPATH = $CHRPATH . "/$typetopic.txt";
  unless ( -e $CHRTYPEPATH)
  {
    make_new_topic($CHRTYPEPATH, "Features", 1, [], $BS);
  }
  foreach my $feature ($pa{DB}->features(-type => $type))
  {
    $pa{CHR} = $feature->seq_id;
    my $id = $feature->Tag_load_id;
    my $FEATUREPATH = $CHRPATH . "/$id.txt";
    unless ( -e $FEATUREPATH)
    {
      my @arr = ($authstr);
      if ($BS->{enable_gbrowse})
      {
        my $note = "See this feature in GBrowse ($pa{CHROMOSOME})";
        push @arr, make_link($feature, $note, \%pa, $BS) . "<br>";        
      }
      foreach my $tag ($feature->get_all_tags())
      {
        next if $tag eq "load_id";
        push @arr, "$tag: " . join(", ", $feature->get_tag_values($tag))
                  . "<br>";
      }
      my @subfeats = $feature->get_SeqFeatures();
      if (scalar (@subfeats))
      {
        push @arr, "<br>Subfeatures:<br>";
        foreach my $subfeat (@subfeats)
        {
          push @arr, "&nbsp;[[" . $subfeat->Tag_load_id . "]]<br>";
        }
      }
      make_new_topic($FEATUREPATH, $typetopic, 0, \@arr, $BS);
    }
  }
}

system ("chown -R 70:0 $BS->{wiki_data}");

exit;

__END__

=head1 NAME

  BS_PrepareWiki.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility takes a chromosome from the genome repository and creates a 
   wiki subweb for it so that it may be tracked and annotated from within
   Foswiki.
   
=head1 ARGUMENTS

Required arguments:

  -C, --CHROMOSOME : The chromosome to be loaded into the wiki. Must be present
        in the BioStudio genome repository.
  
Optional arguments:

  -h, --help : Display this message.
  
=cut
