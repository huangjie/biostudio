#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GFF3 qw(&gff3_string);
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use Bio::SeqIO;
use Perl6::Slurp;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_GFF3Split_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'FILE=s'    => \$pa{FILE},
      'LISTS=s'   => \$pa{LISTS},
      'SPECIES=s' => \$pa{SPECIES},
      'OVERWRITE=i' => \$pa{OVERWRITE},
			'help'			=> \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

$pa{BSVERSION} = $bsversion;
$pa{OVERWRITE} = 0 unless $pa{OVERWRITE};

unless($pa{FILE})
{
  print "\nERROR: No file was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
  die;
}
  
die "\n ERROR: Can't find file $pa{FILE}.\n" unless (-e $pa{FILE});

if ($pa{SPECIES})
{
  die "\n ERROR: Can't find species $pa{SPECIES} in GeneDesign.\n"
    unless (exists $SPECIES{$pa{SPECIES}});
}  

my $filename = fileparse($pa{FILE});  
$filename =~ s/\.[^.]*$//;
die "\n ERROR: Can't find species $filename in GeneDesign.\n"
  unless (exists $SPECIES{$filename} || $pa{SPECIES});
$pa{SPECIES} = $filename unless ($pa{SPECIES});

if ($pa{LISTS})
{
  foreach my $file (split(",", $pa{LISTS}))
  {
    die "\n ERROR: Can't find file $file.\n" unless (-e $file);    
  }
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
##Parse annotations from files into hashes
$pa{ANNOTATIONS} = [];
my @lists = split(",", $pa{LISTS});
foreach my $file (@lists)
{
  my %hsh; 
  $hsh{BSANNOTYPE} = fileparse($file);
  $hsh{BSANNOTYPE} =~ s/\.[^.]*$//;
  
  #Check for content;
  my $raw = slurp("$file");
  my @lines = split("\n", $raw);
  unless (scalar(@lines))
  {
    print "\n WARNING: $hsh{FILENAME} is empty.\n";
    next;
  }
  
  #Parse the target feature types and annotation key
  my %annothsh;
  my $feattypestr = shift @lines;
  my %typehsh = map {$_ => 1} split(", ", $feattypestr);
  $hsh{BSANNOFEATTYPES} = \%typehsh;
  my $annotstr = shift @lines;
  my @preannot = split(", ", $annotstr);
  foreach my $annot (@preannot)
  {
    my @rawannot = split("=", $annot);
    $annothsh{$rawannot[0]} = $rawannot[1];
  } 
  $hsh{BSANNOKEY} = \%annothsh;
  #Parse the annotation listings
  foreach my $line (@lines)
  {
    my @arr = split("\t", $line);
    $hsh{$arr[1]} = $arr[0];
  }
  $hsh{BSANNOUSED} = {};
  push @{$pa{ANNOTATIONS}}, \%hsh;
}

################################################################################
################################## SPLITTING ###################################
################################################################################
print "Loading GFF3 file...\n";
my $DBLIST = list_databases($BS);
drop_database("BSSPLITA", $BS) if exists($DBLIST->{"BSSPLITA"});
create_database("BSSPLITA", $BS);

my @args = ($BS->{bioperl_bin}."/bp_seqfeature_load.pl", "--noverbose", "-c", 
  "-d", "BSSPLITA", "$pa{FILE}", "-f", "--user", 
  "$BS->{mysql_user}", "-p", "$BS->{mysql_pass}");
#$SIG{CHLD} = 'DEFAULT';
system(@args) == 0 or die "system @args failed: $!";

my $db = fetch_database("BSSPLITA", $BS, 1);
   
my @seqids = $db->seq_ids();

# Split the features up by chromosome and add annotations where indicated
foreach my $seqid (@seqids)
{
  my @featlist = $db->features(-seq_id => $seqid);
  foreach my $feat (@featlist)
  {
    #Uniquing subfeature ids
    my @subs = $feat->get_SeqFeatures();
    if (scalar(@subs))
    {
      my %typehsh;
      $typehsh{$_}++ foreach (map {$_->primary_tag} @subs);
      my %seenhash = map {$_ => 1} keys %typehsh;
      foreach my $sub (@subs)
      {
        my $type = $sub->primary_tag;
        my $name;
        if ($typehsh{$type} > 1)
        {
          $name  = $sub->Tag_parent_id . "_$type" . "_$seenhash{$type}";
#          print $feat->Tag_load_id . 
#            " has $typehsh{$type} of $type... naming one $name!\n";
        }
        else
        {
          $name = $sub->Tag_parent_id . "_$type";
        }
        $sub->remove_tag("load_id");
        $sub->add_tag_value("load_id", $name);
        $sub->update();
        $seenhash{$type}++;
      }
    }
  }
} 

foreach my $seqid (@seqids)
{
  my @featlist = $db->features(-seq_id => $seqid);
  foreach my $feat (@featlist)
  { 
    #Adding annotations
    my $id;
    if ($feat->has_tag("load_id"))
    {
      $id = $feat->Tag_load_id;
    }
#### SC2.0 CONCESSION ####    
    if ($feat->has_tag("parent_id"))
    {
      my @temp = split("_", $feat->Tag_parent_id);
      $id = $temp[0] if (scalar (@temp));
    }
#### SC2.0 CONCESSION ####    
    unless ($id)
    {
      print "$feat has no id!, ", $feat->get_all_tags(), "\n";
    }
    my $type = $feat->primary_tag;
    foreach my $hshref (@{$pa{ANNOTATIONS}})
    {
      my %annos = %$hshref;
      my %annokeys = %{$annos{BSANNOKEY}};
      if (exists ($annos{BSANNOFEATTYPES}->{$type}) && exists ($annos{$id}))
      {
        $feat->add_tag_value($annos{BSANNOTYPE}, $annokeys{$annos{$id}});
        $feat->update();
        $hshref->{BSANNOUSED}->{$id}++;
      }
      elsif (exists ($annos{BSANNOFEATTYPES}->{$type}) && exists ($annokeys{0}))
      {
        $feat->add_tag_value($annos{BSANNOTYPE}, $annokeys{0});
        $feat->update();
        $hshref->{BSANNOUSED}->{$id}++;
      }
    }
  }
}

my $speciespath = $BS->{genome_repository} . "/" . $pa{SPECIES};
mkdir $speciespath unless (-e $speciespath);

# Grab the sequences
my %seqhsh;
$seqhsh{$_} = $db->fetch_sequence(-seqid=> $_, -bioseq=>1) foreach (@seqids);

#Process each molecule
foreach my $seqid (@seqids)
{
  
  my $molpath = $speciespath . "/" . $seqid;
  mkdir $molpath unless (-e $molpath);
  my $newname = $molpath . "/$pa{SPECIES}_" . $seqid . "_0_00.gff";
  next if (-e $newname && $pa{OVERWRITE} == 0);
  print "Splitting off $seqid...\n";
  open OUT, ">$newname";
  print OUT "##gff-version 3\n";
  print OUT "# Split from $filename by $pa{BSVERSION}\n";
  my @featlist = $db->features(-seq_id => $seqid);
  @featlist = sort {$a->start <=> $b->start 
               || (($b->end - $b->start) <=> ($a->end - $a->start))} @featlist;
  foreach my $feat (@featlist)
  {
    print OUT gff3_string($feat) . "\n";
  }
  print OUT "##FASTA\n";
  my $seqout = Bio::SeqIO->new(-fh => \*OUT, -format=> "FASTA");
  $seqout->width(80);
  $seqout->write_seq($seqhsh{$seqid});
  close OUT;
}

foreach my $hshref (@{$pa{ANNOTATIONS}})
{
  my %used = %{$hshref->{BSANNOUSED}};
  foreach (grep {! exists $used{$_}} keys %{$hshref})
  {
    next if $_ =~ /^BSANNO/;
    print $hshref->{BSANNOTYPE}, " unused key: $_\n"; 
  }
}
drop_database("BSSPLITA", $BS);

exit;

__END__

=head1 NAME

  BS_GFF3Split.pl

=head1 DESCRIPTION

  This utility splits GFF3 files containing more than one molecule into separate
   files for each molecule, properly arranged into the BioStudio genome
   repository as "wildtype" versions; it will overwrite any existing file with 
   the version number _0_00.
   
  Because BioStudio requires that every feature have a unique ID, this utility
   will also check to make sure that even subfeatures can be uniquely
   identified, and if they cannot, it will attempt to give them descriptive but
   unique names.

  This utility also offers the ability to add annotations to the features as 
   they are being split; this is useful if the GFF3 source does not curate
   all of the information necessary to edit a file - for example, SGD does not 
   explicitly note whether a yeast gene is essential to haploid yeast on glucose 
   or not. This information is easily gleaned from other sources but not so 
   easily added to SGD annotation.  This utility will take any number of
   additional annotation files to supplement the GFF3 annotation.

  The annotation syntax is as follows:
    The name of the file will be the name of the annotation type.
      essential_status.txt will become an essential_status annotation.
    The first line of the file should be a comma separated list of which feature
      feature types the annotation will be applied to, "gene, CDS"
    The second line of the file should be a comma separated list of key val
      pairs, "0=Nonessential,1=Essential,2=fast_growth".  If a zero key is
      provided, features that fit the type list but do not appear in the 
      annotation list will be given the 0 annotation, otherwise they will not be
      annotated.
    The rest of the file should be lines of annotation with an annotation key 
      followed by a tab followed by a feature identifier, for example given 
      "1    YAL001C", any feature with the proper type and the identifier 
      YAL001C will be marked essential_status = Essential.
  
=head1 ARGUMENTS

Required arguments:

  -F, --FILE : The gff3 file to be split into pieces and placed into the genome
        repository. The file should be named {species}.gff or the species flag
        should be used.
  
Optional arguments:

  -S, --SPECIES : The species designation
  -L, --LISTS : A comma separated list of files containing additional
        information about named features in the file; the information will be
        added to feature annotations as the file is split. See help for more 
        information.
  -O, --OVERWRITE : [0, 1] Overwrite existing chromosomes in the genome 
        repository; defaults to 0 (no);
  -h, --help : Display this message
  
=cut
