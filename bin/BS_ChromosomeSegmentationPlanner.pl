#!/usr/bin/perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::RestrictionEnzymes qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Config::Auto;
use Getopt::Long;
use Pod::Usage;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeSegmentationPlanner_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'OLDCHROMOSOME=s'	  => \$pa{OLDCHROMOSOME},
      'WTCHR=s'           => \$pa{WTCHR},
      'RESET=s'           => \$pa{RESET},
      'MARKERS=s'         => \$pa{MARKERS},
      'LASTMARKER=s'      => \$pa{LASTMARKER},
      'SCOPE=s'           => \$pa{SCOPE},
      'STARTPOS=i'        => \$pa{STARTPOS},
      'STOPPOS=i'         => \$pa{STOPPOS},
      'ISSMIN=i'          => \$pa{ISSMIN},
      'ISSMAX=i'          => \$pa{ISSMAX},
      'CHUNKLENMIN=i'     => \$pa{CHUNKLENMIN},
      'CHUNKLENMAX=i'     => \$pa{CHUNKLENMAX},
      'CHUNKNUM=i'        => \$pa{CHUNKNUM},
      'CHUNKNUMMIN=i'     => \$pa{CHUNKNUMMIN},
      'CHUNKNUMMAX=i'     => \$pa{CHUNKNUMMAX},
      'CHUNKOLAP=i'       => \$pa{CHUNKOLAP},
      'FPUTRPADDING=i'    => \$pa{FPUTRPADDING},
      'TPUTRPADDING=i'    => \$pa{TPUTRPADDING},
      'FIRSTLETTER=s'     => \$pa{FIRSTLETTER},
      'REDOREDB'          => \$pa{REDOREDB},
			'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
############################### SANITY CHECKING ################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");
my $BS_MARKERS = fetch_custom_markers($BS);

$pa{SCOPE} = "chrom" unless $pa{SCOPE};
$pa{CHUNKLENMIN} = 6000 unless $pa{CHUNKLENMIN};
$pa{CHUNKLENMAX} = 9920 unless $pa{CHUNKLENMAX};
$pa{CHUNKNUM} = 4 unless $pa{CHUNKNUM};
$pa{CHUNKNUMMIN} = 3 unless $pa{CHUNKNUMMIN};
$pa{CHUNKNUMMAX} = 5 unless $pa{CHUNKNUMMAX};
$pa{CHUNKOLAP} = 40 unless $pa{CHUNKOLAP};
$pa{ISSMIN} = 900 unless $pa{ISSMIN};
$pa{ISSMAX} = 1500 unless $pa{ISSMAX};
$pa{FPUTRPADDING} = 500 unless $pa{FPUTRPADDING};
$pa{TPUTRPADDING} = 100 unless $pa{TPUTRPADDING};
$pa{FIRSTLETTER} = "A" unless $pa{FIRSTLETTER};
$pa{REDOREDB} = 0 unless $pa{REDOREDB};

unless ($pa{OLDCHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
if ($pa{OLDCHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{OLDFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" 
                   . $pa{SEQID} . "/" . $pa{OLDCHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n"
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

unless ($pa{WTCHR})
{
  die "\n ERROR: No target chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{WTCHR} =~ $VERNAME)
{
  my ($SPECIES, $CHRNAME) = ($1, $2);
  die "\n ERROR: The target chromosome is not the same species or chromosome as the edited chromosome.\n"
    if ($pa{SPECIES} ne $SPECIES || $pa{CHRNAME} ne $CHRNAME);
  $pa{WTFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" 
                  . $pa{SEQID} . "/" . $pa{WTCHR} . ".gff";
  die "\n ERROR: Can't find the target chromosome in the repository.\n" 
    unless (-e $pa{WTFILE});
}
else
{
  print "\n ERROR: Target chromosome name doesn't parse to ";
  print "BioStudio standard.\n";
  die();
}

unless ($pa{RESET})
{    
  print "\n ERROR: No restriction enzyme set was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
        
die "\n ERROR: The start and stop coordinates do not parse.\n"          
  if ($pa{SCOPE} && $pa{SCOPE} eq "seg" && 
  ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS}));

die "\n ERROR: Cannot find restriction enzyme list $pa{RESET}.\n"
  unless (-e $BS->{enzyme_dir} . "/" . $pa{RESET} );

unless ($pa{MARKERS})
{
  print "\n ERROR: No markers were specified.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

my %markers = map {$_ => -1} split(",", $pa{MARKERS});
foreach my $marker (keys %markers)
{
  die "\n ERROR: Cannot find marker $marker.\n"
    unless (-e $BS->{marker_dir} . "/" . $marker . ".gff");
}
$pa{MARKERHSH} = \%markers;

die "\n ERROR: Cannot find last marker $pa{LASTMARKER}.\n"
  if ($pa{LASTMARKER} && ! -e $BS->{marker_dir} . "/" . $pa{LASTMARKER} . ".gff");
  
die "\n ERROR: Both a chunk minimnum and maximum size must be defined.\n"
  unless ($pa{CHUNKLENMIN} && $pa{CHUNKLENMAX});

die "\n ERROR: The chunk length parameters do not parse.\n"     
  if ($pa{CHUNKLENMAX} < $pa{CHUNKLENMIN});

die "\n ERROR: The ISS length parameters do not parse.\n"  
  if ($pa{ISSMAX} < $pa{ISSMIN});

################################################################################
################################# CONFIGURING ##################################
################################################################################
$pa{BSVERSION} = $bsversion;
print "CONFIGURING...\n";
my $DBLIST = list_databases($BS);

#Create a global restriction enzyme database, if one does not exist
$pa{REDBNAME} = $pa{OLDCHROMOSOME} . "_RED";
if ($pa{REDOREDB} || ! exists $DBLIST->{$pa{REDBNAME}})
{
  print "CREATING GLOBAL MARKUP DATABASE...\n";
  my $program = "$BS->{bin}/BS_GlobalREMarkup.pl";
  my $redodb = $pa{REDOREDB}  ? "--REDOREDB"  : "";
  $SIG{CHLD} = 'DEFAULT';
  system($program, "--CHROMOSOME", $pa{OLDCHROMOSOME}, "--RESET", 
         $pa{RESET}, "--CHUNKLENMIN", $pa{CHUNKLENMIN}, "--CHUNKLENMAX", 
         $pa{CHUNKLENMAX}, "--CULLINC", 2000, $redodb) 
        == 0 || print "Cannot execute GlobalREMarkup: $! $?\n";
}

$pa{REPATH} = $BS->{enzyme_dir} . $pa{RESET};
$pa{CODON_TABLE} = define_codon_table($SPECIES{$pa{SPECIES}});

$pa{VERNAME}++;
$pa{VERNAME} = "0" . $pa{VERNAME} while (length($pa{VERNAME}) < 2);

$pa{INTRO}    = $pa{GENVER} . "_" . $pa{VERNAME};
$pa{NEWNAME}  = $pa{SPECIES} . "_" . $pa{SEQID} . "_" . $pa{INTRO};
$pa{PLANDBNAME} = $pa{NEWNAME} . "_PLAN";

unless (exists $DBLIST->{$pa{WTCHR}})
{
  create_database($pa{WTCHR}, $BS);
  load_database($pa{WTCHR}, $BS);
}
my $wt = fetch_database($pa{WTCHR}, $BS);
    
print "CREATING PLANNING DATABASE...\n";
drop_database($pa{PLANDBNAME}, $BS) 
  if (exists $DBLIST->{$pa{PLANDBNAME}});
create_database($pa{PLANDBNAME}, $BS);
load_database($pa{PLANDBNAME}, $BS, $pa{OLDCHROMOSOME});
my $plan = fetch_database($pa{PLANDBNAME}, $BS, 1);
$plan->index_subfeatures("true");
if ($BS->{enable_gbrowse})
{
  write_conf_file($BS, $pa{PLANDBNAME}, "") 
    unless (-e $BS->{conf_repository}  . "/" . $pa{PLANDBNAME} . ".conf");
}
  
## Initialize parameters and constants
my $RE_DATA = define_sites($BS->{enzyme_dir} ."/" . $pa{RESET});
my @enzlist = keys %{$RE_DATA->{CLEAN}};
$pa{ENZLIST} = \@enzlist;
my $prevref = {};
my $MAX_MEGA_CHUNK = $pa{CHUNKNUM} * $pa{CHUNKLENMAX};
my $MIN_MEGA_CHUNK = $pa{CHUNKNUM} * $pa{CHUNKLENMAX};

## Order the markers and determine which is the smallest
my ($x, $size, $smallest) = (0, undef, undef);
$pa{MARKERORDER} = {};
if ($pa{LASTMARKER} && exists ($pa{MARKERHSH}->{$pa{LASTMARKER}}))
{
  $pa{MARKERHSH}->{$pa{LASTMARKER}} = $x;
  $pa{MARKERORDER}->{$x} = $pa{LASTMARKER};
  $x++;
}
foreach my $marker (keys %{$pa{MARKERHSH}})
{
  next if ($pa{MARKERHSH}->{$marker} != -1);
  $pa{MARKERORDER}->{$x} = $marker ;
  my $markersize = length($BS_MARKERS->{$marker}->{SEQ});
  $smallest = $marker if (! $size || $markersize < $size);
  $x++;
}
$pa{SMALLESTMARKER} = $smallest;
$pa{SMALLESTMARKERLEN} = length($BS_MARKERS->{$pa{SMALLESTMARKER}}->{SEQ});
$pa{MARKERCOUNT} = scalar(keys %{$pa{MARKERHSH}});

## Gather genes and check for essentiality.  Mask the UTRs of essential
## and fast growth genes by modifying their start and stop sites. 
## Find the right telomere and make the junction just before it
$pa{WTSEQ}     = $wt->fetch_sequence($pa{SEQID});
my $chromosome = $plan->fetch_sequence($pa{SEQID});
my $chrlen = length($chromosome);
my @UTCS = $plan->features(-seq_id => $pa{SEQID}, -type => "UTC");
@UTCS = sort {$b->start <=> $a->start} @UTCS;
my $rutc = $UTCS[0];
my $chrend = scalar(@UTCS)  ? $rutc->start -1 : $chrlen;

my @genes = $plan->get_features_by_type("gene");
$pa{GENEMASK} = make_mask(length($chromosome), \@genes);
my @efgenes = ();
foreach my $gene (@genes)
{
  my $status = $gene->Tag_essential_status;
  if ($status ne "Nonessential")
  {
    my $newstart = $gene->strand == 1  
        ? $gene->start - $pa{"FPUTRPADDING"}  
        : $gene->start - $pa{"TPUTRPADDING"};
    $newstart = 1 if ($newstart < 1);
    $gene->start($newstart);
    my $newstop = $gene->strand == 1 
        ? $gene->stop + $pa{"TPUTRPADDING"} 
        : $gene->stop + $pa{"FPUTRPADDING"};
    $newstop = $chrend if ($newstop > $chrend);
    $gene->stop($newstop);
    push @efgenes, $gene;
  }
}
my @CDSes = $plan->get_features_by_type("CDS");
my %ESSENTIALS = map {$_->display_name => $_->Tag_essential_status} @CDSes;

## Mask the regions that are not viable ISS locations:  Essential / Fast-Growth 
##   genes, regions smaller than the smallest possible ISS site, 
##   and regions with no non-essential genes in them
my $altmask = make_mask(length($chromosome), \@efgenes);
my %intergen = @{mask_filter($altmask)};
foreach (keys %intergen)
{
  my ($start, $end) = ($_, $intergen{$_});
	if ($end - $start < $pa{ISSMIN})
	{
		substr($altmask, $_, 1) = substr($altmask, $_, 1) + 1 for ($start .. $end-1);
	}
}
my %ban = @{mask_filter($altmask)};
my @ess = sort {abs($b - $ban{$b}) <=> abs($a - $ban{$a})} keys %ban;

#fbound and tbound are the 5' and 3' bounds of segmentation, where we start and 
# stop. default is the first and last base of the chromosome, respectively.
# lastpos is where we actually start counting from.
my $tbound = $pa{STOPPOS}  ? $pa{STOPPOS}  : $chrend;
$tbound = $chrend if ($tbound > $chrend);
my $lastpos = $pa{LASTMARKER} 
  ? $tbound + length($BS_MARKERS->{$pa{LASTMARKER}}->{SEQ})
  : $tbound + length($BS_MARKERS->{$pa{MARKERORDER}->{0}}->{SEQ});
my $fbound = $pa{STARTPOS} 
  ? $pa{STARTPOS} + ($pa{CHUNKLENMAX} - ($pa{SMALLESTMARKERLEN}+ 0.5*$pa{ISSMIN})) 
  : 1;

my $markercount = 0;
my $firstmarker = 0;
my $foundcand = undef;
my $doISS = 0;
my $mchrollback = 0;
my $redomch = undef;
my $excisor = undef;
my $lastISSseq = undef;
my %usedsites;
my %deadends;

################################################################################
################################# SEGMENTING  ##################################
################################################################################
print "SEGMENTING...\n";
my @mchs;

while (! $lastpos || ($lastpos-$fbound) >= $pa{CHUNKLENMIN} * 2)
{ 
  my $mch = $redomch  ? $redomch  : {};
  $lastpos = $mch->{END}  ? $mch->{END} : $lastpos;
  
  unless ($mch->{EXCISOR})
  {
    $mch->{EXCISOR} = $excisor;
  }
  
  #decide where the start and stop of this megachunk will be.
  # frange and trange are the 5' and 3' boundaries of the megachunk.
  $mch->{FRANGE} = $lastpos - $MAX_MEGA_CHUNK >= $fbound  
    ? $lastpos - $MAX_MEGA_CHUNK 
    : $fbound;
  $mch->{TRANGE} = $lastpos - $MIN_MEGA_CHUNK;
  
  #eligible regions have no masking - that is, no essential or fast growth genes
  # sort by distance from the last position and choose a start inside frange
  unless ($mch->{START})
  {
    my @eligibles = grep { $_ >= $mch->{FRANGE} && $_ <= $mch->{TRANGE}} 
                    sort {abs($b-$lastpos) <=> abs($a-$lastpos)} @ess;
    $mch->{START} = $eligibles[0] ? $eligibles[0] : $mch->{FRANGE};
    $mch->{START} = $fbound if ($lastpos < $MIN_MEGA_CHUNK);
    $mch->{END} = $lastpos;
  }
  
  #pick a marker for the megachunk and decide which marker will be next.
  # If lastmarker is specified but not in the marker group, take care
  $markercount = $mch->{MARKERCOUNT}  ? $mch->{MARKERCOUNT} : $markercount;
  unless ($mch->{MARKER})
  { 
    $mch->{MARKERCOUNT} = $markercount;
    $mch->{FIRSTMARKER} = $firstmarker;
    my ($markername, $omarkername);
    if ($pa{LASTMARKER} && $firstmarker == 0 
      && (! exists $pa{MARKERHSH}->{$pa{LASTMARKER}}))
    { 
      $markername = $pa{LASTMARKER};
      $omarkername = $pa{MARKERORDER}->{$markercount};
      ($firstmarker, $markercount) = (1, 0);
    }
    else
    {
      $markername = $pa{MARKERORDER}->{$markercount % $pa{MARKERCOUNT}};
      $omarkername = $pa{MARKERORDER}->{($markercount+1) % $pa{MARKERCOUNT}};
    }
    $mch->{MARKER} = $BS_MARKERS->{$markername};
    $mch->{OMARKER} = $BS_MARKERS->{$omarkername};
  }
  
   
  #If there was a previous megachunk, its 5' enzyme becomes the 3' enzyme of 
  # this megachunk. Mark it, its exclusions, and its creations, as used
  my @borders = ();
  if (! $mch->{PREVENZ} && $foundcand)
  {
    %usedsites = ();
    $mch->{PREVENZ} = $foundcand;
    push @borders, $foundcand;
    %usedsites = ($foundcand->{ENZ} => 1);
    $usedsites{$_}++ foreach (@{$RE_DATA->{EXCLUDE}->{$foundcand->{ENZ}}});
    if ($foundcand->{CREATES})
    {
      $usedsites{$_}++ foreach (@{$foundcand->{CREATES}});
    }
  }
  
my $mchlen = $mch->{END} - $mch->{START} + 1;
print "Making a megachunk $mch->{START}..$mch->{END} ~$mchlen bp ";
print "with $markercount $mch->{MARKER}->{NAME}\n";
  
  $mch->{CHUNKLIST} = $mch->{CHUNKLIST} ? $mch->{CHUNKLIST} : [];
  my %usedhangs = ();
  my $chunknum = $mch->{CHUNKNUM} ? $mch->{CHUNKNUM}  : 1;
  my $firsterr = 0;
  my $redoch = $mchrollback ? pop @{$mch->{CHUNKLIST}}  : undef;
  
  #Start to make chunks.
  while (abs($lastpos - $mch->{START}) > $pa{CHUNKLENMIN} || $chunknum <= $pa{CHUNKNUMMAX})
  {
    my $ch = $redoch  ? $redoch : {};
    $ch->{CHUNKNUM} = $chunknum;
    
    $ch->{PREVCAND} = $ch->{PREVCAND} ? $ch->{PREVCAND} : $foundcand;
    $lastpos = $ch->{PREVCAND}->{END} ? $ch->{PREVCAND}->{END}  : $lastpos;
    $mch->{LASTLASTPOS} = $lastpos;
    
    #Keep track of which overhangs and which sites are off limits
    my %tempsites = %usedsites;
    my %temphangs = %usedhangs;
    $ch->{USITES} = $ch->{USITES} ? $ch->{USITES} : \%tempsites;
    $ch->{UHANGS} = $ch->{UHANGS} ? $ch->{UHANGS} : \%temphangs;
    
    #
    my $issflag = $doISS;
    $issflag++ if ($chunknum == $pa{CHUNKNUM} && $firsterr == 0) 
                || $chunknum == $pa{CHUNKNUMMAX};
    $issflag-- if ($mch->{START} <= $fbound);

    #Get the list of viable candidates for enzyme border
    # If this is the first (3' most) chunk, allow for the ISS sequence
    my $isslen = $chunknum == 1 
          ? $pa{CHUNKLENMAX} - (length($mch->{MARKER}->{SEQ}) + 0.5*$pa{ISSMIN}) 
          : 0;

    #Create the candidate list
    my @candlist;
    unless ($ch->{FCANDLIST})
    {
      $ch->{RCANDLIST} = ProposeCandidates($lastpos, \%pa, $isslen);
      
      #Filter candidates by position and sort by score, with lower scores first.
      # If this is the 3' most chunk of the megachunk, don't pick REs that can't 
      # be removed from the marker. If an ISS sequence is 3' of the chunk, don't 
      # pick REs that are present in the wildtype sequence of the ISS sequence.
      @candlist = @{$ch->{RCANDLIST}};
      @candlist = grep {abs($_->{START} - $lastpos) >= $pa{CHUNKLENMIN}} @candlist;
      @candlist = grep {abs($_->{END}   - $lastpos) <= $pa{CHUNKLENMAX}} @candlist;
      @candlist = grep {$_->{START} > $mch->{START}} @candlist;
      @candlist = sort {$a->{SCORE} <=> $b->{SCORE}} @candlist;
      if ($chunknum == 1)
      {  
        @candlist = grep {! exists($mch->{MARKER}->{IRR}->{$_->{ENZ}})} @candlist;
        if ($lastISSseq)
        {
          my $ihsh = define_site_status($lastISSseq, $RE_DATA->{REGEX});
          @candlist = grep {$ihsh->{$_->{ENZ}} == 0} @candlist;
        }
      }
      my @cutlist = @candlist;
      $ch->{ICANDLIST} = \@cutlist;
    }
    else
    {
      @candlist = @{$ch->{FCANDLIST}};
    }
  
    #Take a survey of how often overhang sequences appear among candidates
    # We will want to use overhangs that appear the least first
    my %ohangsurvey;
    foreach my $cand (@candlist)
    {
      my $enz = $cand->{ENZ};
      my $enztype = $RE_DATA->{TYPE}->{$enz};
      $ohangsurvey{"$_.$enztype"}++ foreach (split(",", $cand->{HANG}));
    } 

my @nonos = keys %{$ch->{USITES}};
my @nohgs = keys %{$ch->{UHANGS}};
print "\t ch $chunknum \t", scalar(@candlist), " possibilities\tflag $issflag";
    print "\t firsterr $firsterr \t nonos: @nonos / @nohgs";
print "\t($ch->{PREVCAND}->{ENZ} @ $lastpos)\n";
        
    #Pick a candidate
    %usedhangs = %{$ch->{UHANGS}};
    %usedsites = %{$ch->{USITES}};
    $foundcand = NextCandidate(\@candlist, \%usedhangs, \%ohangsurvey, 
      \%usedsites, $issflag, $ch->{PREVCAND}->{ENZ}, $excisor, $mch->{OMARKER}, \%pa);
    
    #If no candidate can be picked, we will either have to move forward or back
    if (! $foundcand)
    {
      #if we've at least made the minimum number of chunks and this is the first 
      # error, we will try relaxing the standards and moving up.
      if ($chunknum >= $pa{CHUNKNUMMIN} && $firsterr == 0)
      {
        $firsterr++;
        $ch->{FCANDLIST} = $ch->{ICANDLIST};
        $redoch = $ch;
        next;
      }
      #Otherwise, we need to reduce the number of possible chunks and rechoose.
      # we will restore a previous candidate and its workspace
      elsif ($chunknum > 1)
      {
        $redoch = pop @{$mch->{CHUNKLIST}};
        $chunknum--;
        next;
      }
      #If we've backed all the way up to chunk 1, we need to redo the previous 
      # megachunk entirely.
      elsif ($chunknum == 1)
      {
         last;
      }
      else
      {
        die "sorry, boss - ran into a hole!\n\n";
      }
    }    
    $ch->{FCANDLIST} = \@candlist;
    $ch->{CHOICE} = $foundcand;
    if (exists $deadends{$foundcand->{NAME}} 
            && $deadends{$foundcand->{NAME}} == $chunknum)
    {
      print "Been down this road... ";
      last;
    }
    if ($chunknum == 1 && ! $redoch)
    {
      $mch->{FIRSTCHUNK} = $mch->{FIRSTCHUNK} ? $mch->{FIRSTCHUNK} : $foundcand;
    }
    
##DEBUG UPDATE
print "\t  picked ", $foundcand->{NAME}, " ";
print $foundcand->{WANTHANG}, " ", $foundcand->{SCORE};
print "\n";
    
    #Finish picking.  Reset used sites, push the candidate onto the stack.
    push @{$mch->{CHUNKLIST}}, $ch;
    $redoch = undef;
    %usedsites = ();
    $usedsites{$_}++ foreach (@{$RE_DATA->{EXCLUDE}->{$foundcand->{ENZ}}});
    $usedsites{$_}++ foreach (@{$foundcand->{CREATES}});
    $lastpos = $foundcand->{END};
    last if $chunknum == $pa{CHUNKNUM} && $firsterr == 0;
    last if $chunknum == $pa{CHUNKNUMMAX};
    last if ($lastpos < $pa{CHUNKLENMIN});
    $chunknum++;
  #End chunk picking
  }
  
  #If fewer chunks are picked than allowed, we have to redo the whole megachunk.
  if ($chunknum < $pa{CHUNKNUMMIN} && $lastpos > $pa{CHUNKLENMIN})
  {
    $mchrollback = 1;
    $deadends{$mch->{FIRSTCHUNK}->{NAME}} = 1 if ($mch->{FIRSTCHUNK});
    $redomch = pop @mchs;
    $redomch->{PEXICISOR} = undef;
    next;
  }
  if (! $mch->{PEXCISOR} && $lastpos > $pa{CHUNKLENMIN})
  {
    my $rtest = substr($altmask, $lastpos, $pa{ISSMIN});
    die ("wtf\n\n$rtest\n\n")  if ($rtest =~/[^0]/);
    my $plist = ProposeExcisors($lastpos, $mch->{OMARKER}, \%pa);
    my @partnerlist = sort {$a->{SCORE} <=> $b->{SCORE}} @$plist;
    $excisor = $partnerlist[0];
    die ("can't find IIB site!!!\n") unless $excisor;
my $rISSbound = $lastpos + $pa{ISSMIN};
print "\t  next excisor should be ", $excisor->{ENZ}, " ";
print $excisor->{START}, " ", $excisor->{SCORE}, " @ $rISSbound\n";
    $lastISSseq = wtISS($lastpos, $excisor->{START});
    $lastpos = $excisor->{START};
    $mch->{PEXCISOR} = $excisor;
  }
  $mch->{CHUNKNUM} = $chunknum;
  push @mchs, $mch;
  $redomch = undef;
  $mchrollback = 0;
  $markercount++;
  print "\n";
#End megachunk picking
}

################################################################################
########################### COMMITTING TO DATABASE  ############################
################################################################################
print "SUMMARIZING...\n";
my $letter = $pa{FIRSTLETTER};
@mchs = reverse @mchs;
foreach my $mch (@mchs)
{
  my $megachunkname = $pa{NEWNAME} . ".$letter";
  my @chlist = sort {$a->{START} <=> $b->{START}}
                map {$_->{CHOICE}} @{$mch->{CHUNKLIST}};
  print "\t", $_->{ENZ}, " ", $_->{PRE}, " ", $_->{START}, "\n" foreach (@chlist);
  my $excisor = $mch->{EXCISOR} ? $mch->{EXCISOR} : undef;
  my $mcstart = $letter eq $pa{FIRSTLETTER}  ? $fbound : $chlist[0]->{START};
  my $mcend = $excisor ? $excisor->{START} : $tbound;
  my $d = $plan->new_feature(
      -index        => 1,
      -seq_id       => $pa{SEQID},
      -start        => $mcstart,
      -end          => $mcend,
      -primary_tag  => "megachunk",
      -display_name => $megachunkname,
      -source       => "BIO",
      -attributes   => {
        "load_id"   => $megachunkname,
        "intro"     => $pa{INTRO}, 
        "marker"    => $mch->{MARKER}->{NAME}, 
        "bsversion" => $pa{BSVERSION}}
  );
  if ($excisor)
  {
    $d->add_tag_value("excisor", $excisor->{ENZ});
    $d->update();
  }
  print $d->Tag_load_id, " (", $d->start, "..", $d->end, ") ", $d->Tag_marker;
  print " ", $d->Tag_excisor, " ", $excisor->{START} if ($excisor);
  print "\n";
  
  my $chunknum = 1;
  my $lastenz = undef;
  my $chlim = scalar(@chlist);
  $chlim++ if ($letter eq $pa{FIRSTLETTER});
  while ($chunknum <= $chlim)
  {
    my ($chnum, $nchnum) = ($chunknum, $chunknum+1);
    my $chunkname = $megachunkname . $chnum;
    my $nchunkname = $megachunkname . $nchnum;
    my $left = $chunknum == 1 && $letter ne $pa{FIRSTLETTER}  ? shift @chlist : undef;
    my $right = shift @chlist;
    if ($left)
    {
      my $l = $plan->new_feature(
        -index        => 1,
        -seq_id       => $pa{SEQID},
        -start        => $left->{START},
        -end          => $left->{END},
        -score        => sprintf("%.3f", $left->{SCORE}),
        -display_name => $left->{NAME},
        -primary_tag  => "enzyme_recognition_site",
        -source       => "BIO",
        -strand       => $left->{STRAND},
        -attributes   => {
          "presence"    => $left->{PRE}, 
          "infeat"      => $left->{FEAT}, 
          "enzyme"      => $left->{ENZ}, 
          "load_id"     => $left->{NAME}, 
          "wanthang"    => $left->{WANTHANG}, 
          "peptide"     => $left->{PEP},
          "ohangoffset" => $left->{OFFSET}, 
          "megachunk"   => $megachunkname,
          "chunks"      => $chunkname}
      );
      if ($left->{MOVERS})
      {
        $l->add_tag_value("remove", $_) foreach (@{$left->{MOVERS}});
      }
      $l->update();
#      print $l->Tag_enzyme, " ", $l->start, "\n";
    }
    if ($right)
    {
      my $r = $plan->new_feature(
        -index        => 1,
        -seq_id       => $pa{SEQID},
        -start        => $right->{START},
        -end          => $right->{END},
        -score        => sprintf("%.3f", $right->{SCORE}),
        -display_name => $right->{NAME},
        -primary_tag  => "enzyme_recognition_site",
        -source       => "BIO",
        -strand       => $right->{STRAND},
        -attributes   => {
          "presence"    => $right->{PRE}, 
          "infeat"      => $right->{FEAT}, 
          "enzyme"      => $right->{ENZ}, 
          "load_id"     => $right->{NAME}, 
          "wanthang"    => $right->{WANTHANG}, 
          "peptide"     => $right->{PEP},
          "ohangoffset" => $right->{OFFSET}, 
          "megachunk"   => $megachunkname,
          "chunks"      => [$chunkname, $nchunkname]}
      );
      if ($right->{MOVERS})
      {
        $r->add_tag_value("remove", $_) foreach (@{$right->{MOVERS}});
      }
      $r->update();
#      print $r->Tag_enzyme, " ", $r->start, "\n";
    }
    
    #Make a chunk
    my $cstart = $left  ? $left->{START}  : $mcstart;
       $cstart = $lastenz ? $lastenz->{START}  : $mcstart;
    my $cend = $right ? $right->{END} : $mcend;
    my $c = $plan->new_feature(
          -seq_id       => $pa{SEQID},
          -start        => $cstart,
          -end          => $cend,
          -primary_tag  => "chunk",
          -display_name => $chunkname,
          -source       => "BIO",
          -index        => 1,
          -attributes   => {
            "load_id"     => $chunkname, 
            "intro"       => $pa{INTRO}, 
            "bsversion"   => $pa{BSVERSION}, 
            "megachunk"   => $megachunkname}
    );
    $c->add_tag_value("enzymes", $left->{ENZ}) if ($left);
    $c->add_tag_value("enzymes", $lastenz->{ENZ}) if ($lastenz);
    $c->add_tag_value("enzymes", $right->{ENZ}) if ($right);
    $c->update;
    print "\t$chunkname (", $c->start, "..", $c->end, ")\n";
    $lastenz = $right ? $right  : undef;
    $chunknum++;
  }
  print "\n\n";
  $letter++;
  $letter = substr($letter, -1) x length($letter) if (length($letter) > 1);
}

if ($pa{LASTENZ} && $pa{FINISHAT} && $pa{LASTLETTER})
{
  my $oletter = $pa{LASTLETTER};
  my $mchname = $pa{NEWNAME} . ".$oletter";
  my $chname = $mchname . $pa{FINISHAT};
  my $leftname = $pa{LASTENZ};
  my @prepos = split("_", $leftname);
  my $pos = $prepos[1];
  my $res = SelectPositions($pos-16, $pos+16);
  my @rearr = grep {$_->{NAME} eq $leftname} @$res;
  die ("oops, no lastenz?!?\n") unless (scalar @rearr == 1);
  my $c = $plan->new_feature(  
    -index        => 1,
    -seq_id       => $pa{SEQID},
    -start        => $rearr[0]->{START},
    -end          => $excisor->{START},
    -primary_tag  => "chunk",
    -display_name => $chname,
    -source       => "BIO",
    -attributes   => {
      "load_id"     => $chname, 
      "intro"       => $pa{INTRO}, 
      "bsversion"   => $pa{BSVERSION}, 
      "megachunk"   => $mchname}
  );
  print "\t$chname (", $c->start, "..", $c->end, ")\n";
}

exit;

################################################################################
################################# SUBROUTINES ##################################
################################################################################
sub suitable_overhang
{
	my ($used_overhangs_ref, $overhang, $type) = @_;
  return 0 if (! $overhang);
	my $gnahrevo = complement($overhang, 1);
  my %hsh = %$used_overhangs_ref;
	return 0 if (	$overhang eq $gnahrevo	);
	return 0 if (	exists( $hsh{$overhang} )	&& exists($hsh{$overhang}->{$type}));
	return 0 if (	exists( $hsh{$gnahrevo} )	&& exists($hsh{$gnahrevo}->{$type}));
	return 1;
}

sub wtISS
{
  my ($left, $right) = @_;
  ($left, $right) = ($right, $left) if ($left > $right);
  my $size = abs($right - $left + 1);
  
  my @lefngenes  = sort {abs($a->end - $left)   <=> abs($b->end - $left)  } @genes;
  my @rigngenes  = sort {abs($a->start - $right) <=> abs($b->start - $right)} @genes;
  my ($lefgene, $riggene) = ($lefngenes[0], $rigngenes[0]);
  
  my @wtlefgenes = $wt->features(
      -seq_id => $pa{SEQID}, 
      -type => "gene", 
      -name => $lefgene->display_name);
  my @wtriggenes = $wt->features(
      -seq_id => $pa{SEQID}, 
      -type => "gene", 
      -name => $riggene->display_name);
  my ($wtlefgene, $wtriggene) = ($wtlefgenes[0], $wtriggenes[0]);
  
  my $lefendoffset   = $lefgene->end - $left;
  my $rigstartoffset = $right - $riggene->start;
  
  my $wtstart = $wtlefgene->end - $lefendoffset + 1;
  my $wtend   = $wtriggene->start + $rigstartoffset - 1;
  
  my $wtISS = lc substr($pa{WTSEQ}, $wtstart - 1, $wtend - $wtstart + 1);
  my $checkseq = substr($wtISS, $size/2);
  
  return $checkseq;
}

#Given a position along the chromosome, nominates IIB sites
sub ProposeExcisors
{
  my ($lastpos, $marker, $pa) = @_;
  my @IIBs = grep {$RE_DATA->{CLASS}->{$_} eq "IIB"} @{$pa->{ENZLIST}};
  @IIBs  = grep {! exists($marker->{IRR}->{$_})} @IIBs;
  my %lookfors = map {$_ => 1} @IIBs;
  my $right = $lastpos + $pa->{ISSMIN};
  my $left = $right - $pa->{CHUNKLENMAX};
  my $rawgrab = SelectPositions($left, $right);
  my @pool = grep {exists $lookfors{$_->{ENZ}}} @$rawgrab;
  my %igenics = map {$_->{ENZ} => 1} grep {$_->{PRE} eq "i"} @pool;
  @IIBs = grep {! exists $igenics{$_}} @IIBs;
  my $wtISS = wtISS($lastpos, $right);
  my @finalists;
  foreach my $enz (@IIBs)
  {
    my $checkhsh = siteseeker($wtISS, $enz, $RE_DATA->{REGEX}->{$enz});
    next if (scalar(keys %$checkhsh) != 0);
    my ($ess_flag, $fgw_flag, $lap_flag) = (0, 0, 0);
    my $size = length($RE_DATA->{CLEAN}->{$enz});
    my @movers;
    my @exonics = grep {$_->{PRE} eq "e" && $_->{ENZ} eq $enz} @pool;
    my @igenics = grep {$_->{PRE} eq "i" && $_->{ENZ} eq $enz} @pool;
    next if (scalar(@igenics));
    foreach my $ex (@exonics)
    {
      my $egene = $ex->{FEAT};
      my $emaskbit = substr($pa->{GENEMASK}, $ex->{START} - 1, $size);
      $lap_flag++ if ($emaskbit =~/[^1]/);
      $ess_flag++ if ($ESSENTIALS{$egene} eq "Essential");
      $fgw_flag++ if ($ESSENTIALS{$egene} eq "fast_growth");
      push @movers, $ex->{NAME};
    }
    next if ($lap_flag != 0);
    my $score = 0;
    $score   += log($RE_DATA->{SCORE}->{$enz});
    $score   += 0.1 * scalar(@movers);
    $score   += 0.5 * $fgw_flag;
    $score   += 1.0 * $ess_flag;
    my $newfeat = {};
    $newfeat->{ENZ} = $enz;
    $newfeat->{SCORE} = $score;
    $newfeat->{START} = $right;
    $newfeat->{PRE} = "x";
    $newfeat->{MOVERS} = join(",", @movers) if (scalar(@movers));
    push @finalists, $newfeat;
  }
  return \@finalists;
}

#uses globals $CHUNKLENMAX, $RE_DATA, and $ESSENTIALS
#Given a position along the chromosome, nominates low penalty restriction sites
sub ProposeCandidates
{
	my ($lastpos, $pa, $flag) = @_;
  my ($min, $max) = $flag ==  0 ? ($pa->{CHUNKLENMIN}, $pa->{CHUNKLENMAX})  : ($pa->{CHUNKLENMIN}, $flag);
  my $rcpos = $lastpos - $min > 0 ?  $lastpos - $min  : 1;
  my $lcpos = $lastpos - $max > 0 ?  $lastpos - $max  : 1;
  my $lef = $lcpos - $pa->{CHUNKLENMAX} > 0  ? $lcpos - $pa->{CHUNKLENMAX}  : 1;
  my $pool = SelectPositions ($lef - $pa->{CHUNKOLAP}, $lastpos + $pa->{CHUNKOLAP});
  my @candidates = grep {$_->{START} >= $lcpos && $_->{END} <= $rcpos} @$pool;
  my @finalists;
  foreach my $re (@candidates)
  {
    my $enz = $re->{ENZ};
    my $pre = $re->{PRE};
    my $size = $re->{END} - $re->{START} + 1;
    my $maskbit = substr($pa->{GENEMASK}, $re->{START} - 1, $size);
    #Drop if exonic in exon overlap
    next if ($pre ne "i" && $maskbit =~/[^1]/);
    my @buds = grep {abs($_->{START} - $re->{START}) <= $pa->{CHUNKLENMAX} 
                && $_->{PRE}  ne "p" && $_->{ENZ} eq $enz 
                && $_->{NAME} ne $re->{NAME}} 
               @$pool;
    my @igenics = grep {$_->{PRE} eq "i"} @buds;
    #Drop if too many intergenics around    
    next if (scalar(@igenics));
    my $gene = $pre ne "i"  ? $re->{FEAT}  : undef;
    my @exonics = grep {$_->{PRE} eq "e"} @buds;
    my $ess_flag = $gene && $ESSENTIALS{$gene} eq "Essential"   ? 1 : 0;
    my $fgw_flag = $gene && $ESSENTIALS{$gene} eq "fast_growth" ? 1 : 0;
    my $lap_flag = 0;
    my @movers;
    foreach my $ex (@exonics)
    {
      next if ($ex->{START} > $lastpos + $pa->{CHUNKOLAP});
      my $egene = $ex->{FEAT};
      my $emaskbit = substr($pa->{GENEMASK}, $ex->{START} - 1, $size);
      $lap_flag++ if ($emaskbit =~/[^1]/);
      last if $lap_flag;
      $ess_flag++ if ($ESSENTIALS{$egene} eq "Essential");
      $fgw_flag++ if ($ESSENTIALS{$egene} eq "fast_growth");
      push @movers, $ex->{NAME};
    }
    #Drop if it requires modification in exon overlap    
    next if ($lap_flag != 0);
    my $distance = abs($re->{START} - $lcpos);
    
    # Score is a function of distance from largest possible mark
    my $score = $distance <= 5000 ?	0	:	0.0008  * $distance - 4;
    # Plus the log of the price per unit
    $score   += log($RE_DATA->{SCORE}->{$enz});
    # Plus one tenth point for each orf modified
    $score   += 0.1 * scalar(@movers);
    # Plus one half point for each fast growth orf modified
    $score   += 0.5 * $fgw_flag;
    # Plus one point for each essential orf modified
    $score   += 1.0 * $ess_flag;
    # Ignore if score is too high
    next if ($score > 1);
    $re->{SCORE} = $score;
    $re->{MOVERS} = \@movers if (scalar(@movers));
    push @finalists, $re;
  }
  return \@finalists;
}

sub SelectPositions 
{
  my ($left, $right) = @_;
  #  print "\t\tlooking from $left to $right...\n";
  my $dbh = DBI->connect("dbi:mysql:$pa{REDBNAME}", $BS->{"mysql_user"}, 
                      $BS->{"mysql_pass"}, { RaiseError => 1, AutoCommit => 1});
  $dbh->{'mysql_auto_reconnect'} = 1;
  my $sth = $dbh->prepare
  (
    "SELECT name, presence, eligible, start, end, enzyme, feature, overhangs, 
            strand, peptide, overhangoffset 
    FROM positions 
    WHERE start >= $left AND end <= $right"
  ) or die "Unable to prepare command: ". $dbh->errstr."\n";
  
  $sth->execute or die "Unable to exec cmd: ". $dbh->errstr."\n";
  my @res = ();
  while (my $aref = $sth->fetchrow_arrayref)
  {
    my ($name, $presence, $eligible, $start, $end, 
        $enzyme, $feature, $overhangs, $strand, $peptide, $offset) = @$aref;
    my $newfeat = {};
    $newfeat->{NAME} = $name;
    $newfeat->{PRE} = $presence;
    $newfeat->{IGNORE} = $eligible;
    $newfeat->{START} = $start;
    $newfeat->{END} = $end;
    $newfeat->{ENZ} = $enzyme;
    $newfeat->{FEAT} = $feature;
    $newfeat->{HANG} = $overhangs;
    $newfeat->{STRAND} = $strand;
    $newfeat->{PEP} = $peptide;
    $newfeat->{OFFSET} = $offset;
    push @res, $newfeat;
  }
  $sth->finish;
  $dbh->disconnect();
  return \@res;
}

sub NextCandidate
{
  my ($candlist, $usedhangref, $hangsurvey, $usedsiteref, 
      $flag, $prevenz, $excisor, $omarker, $pa) = @_;
  my $foundcand = undef;
  while (! $foundcand && scalar(@$candlist))
  {
    my $candidate = shift @$candlist;
    my $enz = $candidate->{ENZ};
    next if (exists $usedsiteref->{$enz});
    if ($flag)
    {
      my $ISSseq = substr($altmask, $candidate->{END}, $pa{ISSMIN});
      next if ($ISSseq =~ /[^0]/);
      my $wtISSseq = wtISS($candidate->{START}, $candidate->{START} + $pa{ISSMIN});
      my $ihsh = siteseeker($wtISSseq, $enz, $RE_DATA->{REGEX}->{$enz});
      next if (scalar(keys %$ihsh));
      next if (exists($omarker->{IRR}->{$enz}));
    }
    my $enztype = $RE_DATA->{TYPE}->{$enz};
    my @changs = split(",", $candidate->{HANG});
    my %hanghsh = map {$_ => 1} @changs;
    @changs = keys %hanghsh;
    @changs = sort {$hangsurvey->{"$a.$enztype"} <=> $hangsurvey->{"$b.$enztype"}} @changs;
    @changs = grep { suitable_overhang( $usedhangref, $_, $enztype ) && $_} @changs;
    next unless (scalar @changs);
    my $hang = undef;
    if (! $prevenz || $candidate->{PRE} eq "i")
    {
      $hang = $changs[0];
    }
    else
    {
      #      print "\t\tchecking for presence of $prevenz in substitutions:\n";
      foreach my $phang (@changs)
      {
        my ($oldseq, $newseq) = NewSequence($candidate, $phang, $pa);
        my $oSTATUS = define_site_status($oldseq, $RE_DATA->{REGEX});
        my $nSTATUS = define_site_status($newseq, $RE_DATA->{REGEX});
        next if ($nSTATUS->{$prevenz} != $oSTATUS->{$prevenz} && $prevenz ne $enz);
        next if ($nSTATUS->{$enz} != 1);
        next if ($excisor && $nSTATUS->{$excisor->{ENZ}} != 0);
        $hang = $phang;
        my @createds = grep {$nSTATUS->{$_} > $oSTATUS->{$_} && $_ ne $enz} @{$pa->{ENZLIST}};
        $candidate->{CREATES} = \@createds if (scalar @createds);
        last;
      }
    }
    next unless ($hang);
    $candidate->{WANTHANG} = $hang;
    my $gnah = complement($hang, 1);
    $usedhangref->{$hang} = {} unless (exists $usedhangref->{$hang});
    $usedhangref->{$gnah} = {} unless (exists $usedhangref->{$gnah});
    $usedhangref->{$hang}->{$enztype}++;
    $usedhangref->{$gnah}->{$enztype}++;
    $foundcand = $candidate;
  }
  return $foundcand ? $foundcand  : undef;
}

sub NewSequence
{
  my ($candidate, $ohang, $pa) = @_;
  my $start = $candidate->{START};
  my $end = $candidate->{END};
  my $strand = $candidate->{STRAND};
  my $enz = $candidate->{ENZ};
  my $site = $RE_DATA->{CLEAN}->{$enz};
  my $cut = $RE_DATA->{TABLE}->{$enz};
  my ($lef, $rig) = (0, 0);
  ($lef, $rig) = ($1, $2) if ($cut =~ $IIAreg);
  ($lef, $rig) = ($rig, $lef) if ($rig < $lef);
  $rig = 0 if ($rig < 0);
  my @CDSes = $plan->get_features_by_name($candidate->{FEAT});
  my $CDS = $CDSes[0];
  my $peptide = $candidate->{PEP};
  my $offset = $candidate->{OFFSET} || 0;
  if ($CDS->strand == -1)
  {
    $end ++ while (($CDS->end - $end) % 3 != 0);
    $start -- while (($end - $start + 1) /3  < length($peptide));
  }
  else
  {
    $start -- while (($start - $CDS->start) % 3 != 0);
    $end ++ while (($end - $start + 1) /3  < length($peptide));
  }
  my $testseq = substr($chromosome, $start - 1, $end - $start + 1);
  $testseq = complement($testseq, 1) if ($CDS->strand == -1);
  my $aa = translate($testseq, 1, $pa->{CODON_TABLE});
  $site = complement($site, 1) if ($strand == -1);
  my $alnmat = pattern_aligner($testseq, $site, $aa, $pa->{CODON_TABLE}, 0, $prevref);
  unless ($RE_DATA->{TYPE}->{$enz} =~ /b/)
  {
    substr($alnmat, $offset, length($ohang)) = $ohang;
  }
  my $newpatt = pattern_adder($testseq, $alnmat, $pa->{CODON_TABLE}, $prevref);
  $newpatt = complement($newpatt, 1) if ($CDS->strand == -1);
  $testseq = complement($testseq, 1) if ($CDS->strand == -1);
  my $pattlen = length($newpatt);
  my $contextlen = 60 + $pattlen;
  $start -=20;
  my $context = substr($chromosome, $start -1, $contextlen);
  my $oldtext = $context;
  substr($context, 20, $pattlen) = $newpatt;
  return ($oldtext, $context);
}

__END__

=head1 NAME

  BS_ChromosomeSegmentationPlanner.pl
  
=head1 VERSION

    Version 1.00

=head1 DESCRIPTION

  This utility proposes a set of restriction enzyme recognition site changes to
    a chromsome. The goal is to make the chromosome assemblable from 
    multikilobase pieces called chunks, which in groups of roughly CHUNKNUM form
    larger pieces called megachunks. Megachunks end in special regions called
    InterSiteSequences (ISS), which consist of synthetic sequence, followed by a 
    marker, followed by wildtype sequence, followed by a type IIB restriction
    enzyme recognition site.  The wildtype sequence targets the megachunk to its
    target chromosome for homologous recombination (thus wildtype here can be
    any homologous chromosome, as long as gene order is the same). Megachunks
    alternate markers to allow a simple selection for successful integration.
    Markers should be defined in config/markers.
  
  No edits will be made, this utility merely proposes a plan for you to vet.
    If the plan meets your requirements, run BS_ChromosomeSegmenter to implement
    it.
  
=head1 ARGUMENTS

Required arguments:

  -O,  --OLDCHROMOSOME : The chromosome to be modified
  -W,  --WTCHR  : The chromosome that will receive chunks (usually wildtype)
  --RESET  : Which list of restriction enzymes to use (config/enzymes)
  -M,  --MARKERS : Comma separated list which will be alternately inserted
      into megachunk ISS sequences (must be defined in config/markers)
  
Optional arguments:

  -SC,  --SCOPE : [seg, chrom (def)] How much sequence will the edit 
                  affect. seg requires STARTPOS and STOPPOS.
  -STA, --STARTPOS : The first base for analysis;ignored unless SCOPE = seg
  -STO, --STOPPOS  : The last base for analysis;ignored unless SCOPE = seg
  --CHUNKLENMIN : The minimum size for chunks (default 6000 bp)
  --CHUNKLENMAX : The maximum size for chunks (default 9920 bp)
  --CHUNKNUM : The target number of chunks per megachunk (default 4)
  --CHUNKNUMMIN : The minimum number of chunks per megachunk (default 3)
  --CHUNKNUMMAX : The maximum number of chunks per megachunk (default 5)
  --CHUNKOLAP : The number of bases each chunk must overlap (default 40)
  --ISSMIN : Minimum size of the homologous intersite sequence (default 900)
  --ISSMAX : Maximum size of the homologous intersite sequence (default 1500)
  --FIRSTLETTER : The first letter to assign a megachunk (default 'A')
  -FP, --FPUTRPADDING : No edit zone upstream of the five prime end of 
            essential/fast growth genes when no UTR is annotated (default 500)
  -TP, --TPUTRPADDING : No edit zone downstream of the three prime end of 
            essential/fast growth genes when no UTR is annotated (default 100)
  -L, --LASTMARKER : Which marker should be the last marker inserted (must be
            defined in config/markers)
  --REDOREDB : Force recreation of the restriction enzyme database. Useful if
            the chromosome sequence or the chunk size parameters change between 
            uses of the Segmentation Planner
  -h,   --help : Display this message
  
=cut
  