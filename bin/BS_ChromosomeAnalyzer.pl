#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::Codons qw(:all);
use Bio::GeneDesign::RestrictionEnzymes qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::GBrowse qw(&make_link gbrowse_gene_names);
use Bio::SeqFeature::Generic;
use Config::Auto;
use Getopt::Long;
use Pod::Usage;
use CGI qw(:all);

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeAnalyzer_$VERSION";

$| = 1;

my %pa;
GetOptions (
			'CHROMOSOME=s'  => \$pa{CHROMOSOME},
			'PCG'           => \$pa{PCG},
			'NPCG'          => \$pa{NPCG},
			'TR'            => \$pa{TR},
			'CF'            => \$pa{CF},
			'RE'            => \$pa{RE},
			'RESET=s'       => \$pa{RESET},
			'SCOPE=s'       => \$pa{SCOPE},
			'STARTPOS=i'    => \$pa{STARTPOS},
			'STOPPOS=i'     => \$pa{STOPPOS},
			'output=s'      => \$pa{OUTPUT},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

$pa{SCOPE} = "chrom" unless $pa{SCOPE};
$pa{OUTPUT} = "txt" unless $pa{OUTPUT};
$pa{BSVERSION} = $bsversion;

unless ($pa{CHROMOSOME})
{
  print "\nERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{CHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}) = ($1, $2);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{FILEPATH} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                    $pa{SEQID} . "/" . $pa{CHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n"
    unless (-e $pa{FILEPATH});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

unless  ($pa{CF} || $pa{RE} || $pa{TR} || $pa{PCG} || $pa{NPCG})
{
  die "\n ERROR: no analyses were indicated.\n";
}

if ($pa{SCOPE} && $pa{SCOPE} eq "seg" && 
  ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS}))
{
  die "\n ERROR: The start and stop coordinates do not parse.\n";
}

if ($pa{RE} && ! $pa{RESET})
{
  die "\n ERROR: no restriction enzyme list was specified.\n";
}
elsif ($pa{RESET})
{
  die "\n ERROR: cannot find restriction enzyme list.\n"
    unless (-e $BS->{enzyme_dir} . "/" . $pa{RESET} );
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
my @pcgs = qw(gene mRNA CDS intron five_prime_UTR_intron);
push @pcgs, qw(three_prime_UTR_intron);

my @pcgmods = qw(PCR_product tag stop_retained_variant);
push @pcgmods, qw(synonymous_codon non_synonymous_codon);

my @npcgs = qw(tRNA rRNA snoRNA snRNA ncRNA pseudogene noncoding_exon);

my @trs = qw(LTR_retrotransposon transposable_element_gene);
push @trs, qw(long_terminal_repeat repeat_region);

my $DBLIST = list_databases($BS);
unless (exists($DBLIST->{$pa{CHROMOSOME}}))
{
  create_database($pa{CHROMOSOME}, $BS);
  load_database($pa{CHROMOSOME}, $BS);
}
$pa{DB} = fetch_database($pa{CHROMOSOME}, $BS);

my $CODON_TABLE = define_codon_table($SPECIES{$pa{SPECIES}});

my @genes     = $pa{DB}->get_features_by_type("gene");
$pa{GENES}    = \@genes;
$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
    
# compile CDSes to individual gene masks and a big chromosome mask
my @CDSes;
foreach my $gene (@{$pa{GENES}})
{
  push @CDSes, grep {$_->type eq "CDS"} flatten_subfeats($gene);
}
$pa{CHRMASK} = make_mask(length($pa{ORIGCHR}), \@CDSes);

my %type_list = map {$_->method => 1} $pa{DB}->types;
my @pcgtypes    = grep {exists $type_list{$_}} @pcgs;
my @pcgmodtypes = grep {exists $type_list{$_}} @pcgmods;
my @npcgtypes   = grep {exists $type_list{$_}} @npcgs;
my @trtypes     = grep {exists $type_list{$_}} @trs;
my %ktypehsh = map {$_ => 1} @pcgtypes, @pcgmodtypes, @npcgtypes, @trtypes;
my @otypes = grep {! exists $ktypehsh{$_}} keys %type_list;
my $disc = "";
   $disc = " in the region $pa{STARTPOS}..$pa{STOPPOS}"
    if ($pa{SCOPE} eq "seg");

print "Processing chromosome $pa{CHROMOSOME}...\n";
if ($pa{OUTPUT} eq "html")
{
  print "<a href=\"\#PCG\">Protein Coding Genes</a>\n" if ($pa{PCG});
  print "<a href=\"\#NPCG\">Non-Protein Coding Genes</a>\n" if ($pa{NPCG});
  print "<a href=\"\#TR\">Transposons and Repeat Features</a>\n" if ($pa{TR});
  print "<a href=\"\#CF\">Chromosomal Features</a>\n" if ($pa{CF});
  print "<a href=\"\#RE\">Restriction Enzymes</a>\n" if ($pa{RE});
  print "<pre>\n";
}
my $range = Bio::Range->new(-start => $pa{STARTPOS}, -end => $pa{STOPPOS})
  if ($pa{SCOPE} eq "seg");

################################################################################
############################ Protein Coding Genes  #############################
################################################################################
if ($pa{PCG})
{
  my $head = "Protein Coding Genes";
  print $pa{OUTPUT} eq "html" ? h1("<a name=\"PCG\">$head</a>") : "$head\n";
  if ($pa{SCOPE} eq "seg")
  {
    @{$pa{GENES}} = grep {$range->contains($_)} @{$pa{GENES}};
  }
  my %genehsh = map {$_->id => $_} @{$pa{GENES}};
  my %genelengths  = map {$_->id => $_->stop - $_->start + 1} @{$pa{GENES}};
  my $genedisplayref = $BS->{enable_gbrowse} 
    ? gbrowse_gene_names($pa{GENES}, \%pa, $BS)
    : gene_names($pa{GENES}, $BS);
  my %genedisplays = %$genedisplayref;
  my %nakeddisplays = %{gene_names($pa{GENES}, $BS)};
  my @essentialgenes;
  my @fastgrowthgenes;
  my $colwidth = 0;
  $colwidth+=3;
  my $pcg1  = " $pa{CHROMOSOME} has " . scalar(@{$pa{GENES}});
     $pcg1 .= " protein coding genes$disc.\n";
  print $pa{OUTPUT} eq "html" ? h3($pcg1) : $pcg1;
    
  my @largestgenes = sort {$genelengths{$b->id} <=> $genelengths{$a->id}}
                      @{$pa{GENES}};
  my @smallestgenes = reverse @largestgenes;
                
  my $smallestgene = $smallestgenes[0];
  my @vgenes = grep { $_->Tag_orf_classification eq "Verified" } @smallestgenes;
  my $smallestvgene = $vgenes[0];
  
  print " The smallest protein coding gene is ";
  print $genedisplays{$smallestgene->id} . " at ";
  print $genelengths{$smallestgene->id} . " bp.\n";
  
  print " The smallest verified protein coding gene is ";
  print $genedisplays{$smallestvgene->id} . " at ";
  print $genelengths{$smallestvgene->id} . " bp.\n";
  
  my @toplargestgenes = map {
    " " . $genedisplays{$_->id} . 
    " " x ($colwidth - length($nakeddisplays{$_->id})) 
    . $genelengths{$_->id} . "bp\n" } @largestgenes[0..4];
  print" The five largest protein coding genes are:\n", @toplargestgenes, "\n";

  @essentialgenes = map {" " . $genedisplays{$_->id} . "\n"} @essentialgenes;
  print "\nThere are " . scalar(@essentialgenes);
  print " protein coding essential genes:\n", @essentialgenes, "\n";
  
  @fastgrowthgenes = map {" " . $genedisplays{$_->id} . "\n"} @fastgrowthgenes;
  print "\nThere are " . scalar(@fastgrowthgenes);
  print " protein coding genes required for fast growth:\n";
  print @fastgrowthgenes, "\n";
  
  my %igen = @{mask_filter($pa{CHRMASK})};
  my @ordereddesertstarts = sort {$igen{$b} - $b <=> $igen{$a}-$a} keys %igen;
  my @toptendeserts;
  my $counter = 0;
  foreach my $desert (@ordereddesertstarts)
  {
    my $start = $desert+1;
    my $end = $igen{$desert}+1;
    my $len = $end - $start + 1;
    my $feat = Bio::SeqFeature::Generic->new(
        -start  => $start, 
        -end    => $end, 
        -seq_id => $pa{CHR});
    if ($pa{SCOPE} eq "seg")
    {
      next unless $range->contains($feat);
    }
    my $desc = "$len bp ($start..$end)";
    if ($pa{OUTPUT} eq "html" && $BS->{enable_gbrowse})
    {
      $desc = make_link($feat, $desc, \%pa, $BS);
    }
    push @toptendeserts, " " . $desc . "\n";
    $counter++;
    last if ($counter == 9);
  }
  print " The top ten gene deserts are:\n", @toptendeserts, "\n";
    
  my @geneswithintrons;
  my @ilist = qw(intron five_prime_UTR_intron three_prime_UTR_intron);
  my %ilisthsh = map {$_ => 1} @ilist;
  foreach my $gene (sort {$a->start <=> $b->start} @{$pa{GENES}})
  {
    my @subfeats = $gene->get_SeqFeatures();
    my @mrnas = grep {$_->primary_tag() eq "mRNA"} @subfeats;
    my @introns = grep {exists($ilisthsh{$_->primary_tag()})} @subfeats;
    foreach my $mrna (@mrnas)
    {
      my @msubfeats = $mrna->get_SeqFeatures();
      push @introns, grep {exists($ilisthsh{$_->primary_tag()})} @msubfeats;
    }
    if (scalar(@introns))
    {
      my $number = scalar(@introns) > 1 ? "s" : "";
      my $spaces = " " x ($colwidth - length($nakeddisplays{$gene->id}));
      my $out  = " " . $genedisplays{$gene->id} . $spaces . scalar(@introns);
         $out .= " intron$number\n";
      push @geneswithintrons, $out;
    }
  }
  if (scalar(@geneswithintrons))
  {
    print " There are " . scalar(@geneswithintrons);
    print " protein coding genes with introns:\n", @geneswithintrons, "\n";
  }
  else
  {
    print " No genes have introns. ";
  }
  
  my $allorfs = ORF_compile($pa{GENES});
  my @ORFS = values %{$allorfs};
  my $codoncount = codon_count(\@ORFS, $CODON_TABLE);
  my @codvalsort = sort {$b <=> $a} values %$codoncount;
  my $maxcodnum = $codvalsort[0];
  my $RSCUVal = generate_RSCU_values($codoncount, $CODON_TABLE);
  my %aalist = map {$_ => 1} values %$CODON_TABLE;
  
  my %overlaps;
  foreach my $gene (@{$pa{GENES}})
  {
    my %olaps;
    my @ogs = grep {$_->overlaps($gene) && $_->id ne $gene->id} @{$pa{GENES}};
    foreach my $lapgene (@ogs)
    {
      my ($left, $common, $right) = $lapgene->overlap_extent($gene);
      $olaps{$lapgene->id} = $common;
    }
    $overlaps{$gene->id} = \%olaps if (scalar(keys %olaps));
  }
  if (scalar(keys %overlaps))
  {
    print " ", scalar(keys %overlaps) . " protein coding genes overlap ";
    print "one another in some way:\n";
    my @ols = sort {$genehsh{$a}->start <=> $genehsh{$b}->start} keys %overlaps;
    foreach my $olgene (@ols)
    {
      my %olap = %{$overlaps{$olgene}};
      my $aggr;
      my @laps = sort {$genehsh{$a}->start <=> $genehsh{$b}->start} keys %olap;
      foreach my $lapgene (@laps)
      {
        my $spaces = " " x ($colwidth - length($nakeddisplays{$lapgene}));
        $aggr .= " $genedisplays{$lapgene} ($olap{$lapgene} bp)$spaces";
      }
      my $spaces = " " x ($colwidth - length($nakeddisplays{$olgene}));
      print " " . $genedisplays{$olgene} . $spaces . $aggr . "\n";
    }
  }
  else
  {
    print " No protein coding genes overlap one another.\n";
  }

  print "\nCodon Usage";
  my @arr = qw(T C A G);
  print " Codon counts and RSCU values:";
  
  
  foreach my $a ( @arr)
  {
    print " \n";
    foreach my $c ( @arr )
    {
      foreach my $b ( @arr )
      {
        my $codon = $a . $b . $c;
        my $count = $codoncount->{$codon};
        my $spacer = " " x (length($maxcodnum) - length($count));
        print "$codon ($CODON_TABLE->{$codon}) $spacer$count ";
        print "$RSCUVal->{$codon}" . " " x 5;
      }
      print  " \n";
    }
    print  "\n";
  }  
  print " highest and lowest value RSCU table:\n";
  foreach my $aa (sort keys %aalist)
  {
    my ($max, $min, $hcod, $lcod) = (0, 1, undef, undef);
    foreach (grep {$CODON_TABLE->{$_} eq $aa} keys %$RSCUVal)
    {
      ($max, $hcod) = ($RSCUVal->{$_}, $_) if ($RSCUVal->{$_} > $max);
      ($min, $lcod) = ($RSCUVal->{$_}, $_) if ($RSCUVal->{$_} <= $min);
    }
    print  " " .  "$aa: $hcod $max" . " " x 5, "$lcod $min\n";
  }
  
  print "\nModifications to Protein Coding Genes:\n" if scalar(@pcgmodtypes);
  foreach my $type (@pcgmodtypes)
  {
    my @feats = $pa{SCOPE} eq "seg"
      ? $pa{DB}->features(
          -primary_tag => $type, 
          -start => $pa{STARTPOS}, 
          -end   => $pa{STOPPOS}, 
          -range_type => 'contains', 
          -seq_id => $pa{SEQID})
      : $pa{DB}->features(-primary_tag =>$type);
    my @list;
    my $typedisplay = scalar(@feats) != 1  ? $type . "s" : $type;
    foreach my $feat (sort {$a->start <=> $b->start} @feats)
    {
      my $desc = $feat->id . " (" . $feat->start . ".." . $feat->end . ")";
      if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html")
      {  
        $desc = " " . make_link($feat, $desc, \%pa, $BS);
      } 
      push @list, $desc . "\n";
    }
    print " $pa{CHROMOSOME} has " . scalar(@feats);
    print " $typedisplay$disc.\n", @list, "\n";
  }
}



################################################################################
########################## non-Protein Coding Genes  ###########################
################################################################################
if ($pa{NPCG})
{
  print "\n\n";
  my $head = "Non-Protein Coding Genes";
  print $pa{OUTPUT} eq "html" ? h1("<a name=\"NPCG\">$head</a>") : "$head\n";
  foreach my $type (@npcgtypes)
  {
    my @feats = $pa{SCOPE} eq "seg"
      ? $pa{DB}->features(
          -primary_tag => $type,
          -start => $pa{STARTPOS}, 
          -end   => $pa{STOPPOS}, 
          -range_type => 'contains', 
          -seq_id => $pa{SEQID})
      : $pa{DB}->features(-primary_tag =>$type);
    my @list;
    print "\t@feats\n\n";
    my $typedisplay = scalar(@feats) != 1  ? $type . "s" : $type;
    foreach my $feat (sort {$a->start <=> $b->start} @feats)
    {
      my $desc = $feat->id . " (" . $feat->start . ".." . $feat->end . ")\n";
      if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html")
      {
        $desc = " " . make_link($feat, $desc, \%pa, $BS);
      }
      push @list, $desc;
    }
    my $npcg = " $pa{CHROMOSOME} has " . scalar(@feats) 
      . " $typedisplay$disc.\n";
    print $pa{OUTPUT} eq "html" ? h3($npcg) : $npcg;
    print @list, "\n";
  }
}

################################################################################
####################### Transposons and Repeat Features  #######################
################################################################################
if ($pa{TR})
{
  print "\n\n";
  my $head = "Transposon and Repeat Features";
  print $pa{OUTPUT} eq "html" ? h1("<a name=\"TR\">$head</a>") : "$head\n";
  foreach my $type (@trtypes)
  {
    my @feats = $pa{SCOPE} eq "seg"
      ? $pa{DB}->features(
          -primary_tag => $type, 
          -start => $pa{STARTPOS}, 
          -end   => $pa{STOPPOS}, 
          -range_type => 'contains', 
          -seq_id => $pa{SEQID})
      : $pa{DB}->features(-primary_tag =>$type);
    my @list;
    my $typedisplay = scalar(@feats) != 1  ? $type . "s" : $type;
    foreach my $feat (sort {$a->start <=> $b->start} @feats)
    {
      my $desc = $feat->id . " (" . $feat->start . ".." . $feat->end . ")\n";
      if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html")
      {
        $desc = " " . make_link($feat, $desc, \%pa, $BS);
      } 
      push @list, $desc;
    }
    my $tr = " $pa{CHROMOSOME} has " . scalar(@feats) . " $typedisplay$disc.\n";
    print $pa{OUTPUT} eq "html" ? h3($tr) : $tr;
    print @list, "\n";
  }
} 

################################################################################
############################# Chromosomal Features #############################
################################################################################
if ($pa{CF})
{
  print "\n\n";
  my $head = "Chromosome Features";
  print $pa{OUTPUT} eq "html" ? h1("<a name=\"CF\">$head</a>")  : "$head\n";
  foreach my $type (@otypes)
  {
    my @feats = $pa{SCOPE} eq "seg"
      ? $pa{DB}->features(
          -primary_tag =>$type, 
          -start => $pa{STARTPOS}, 
          -end => $pa{STOPPOS}, 
          -range_type => 'contains', 
          -seq_id => $pa{SEQID})
      : $pa{DB}->features(-primary_tag =>$type);
    my @list;
    my $typedisplay = scalar(@feats) != 1  ? $type . "s" : $type;
    foreach my $feat (sort {$a->start <=> $b->start} @feats)
    {
      my $desc = $feat->id . " (" . $feat->start . ".." . $feat->end . ")\n";
      if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html")
      {
        $desc = " " . make_link($feat, $desc, \%pa, $BS);
      }
      push @list, $desc;
    }
    my $cf = " $pa{CHROMOSOME} has " . scalar(@feats) . " $typedisplay$disc.\n";
    print $pa{OUTPUT} eq "html" ? h3($cf) : $cf;
    print @list, "\n";
  }
} 

################################################################################
############################# Restriction Enzymes  #############################
################################################################################
if ($pa{RE})
{
  my $RE_DATA = define_sites($BS->{enzyme_dir} . "/" . $pa{RESET});
  print "\n\n";
  my $head = "Restriction Enzyme Features";
  print $pa{OUTPUT} eq "html" ? h1("<a name=\"RE\">$head</a>")  : "$head\n";
  my $seq = $pa{ORIGCHR};
  $seq = substr($seq, $pa{STARTPOS} + 1, $pa{STOPPOS} - $pa{STARTPOS} + 1) 
    if ($pa{SCOPE} eq "seg");
  my $SITE_STATUS = define_site_status($seq, $RE_DATA->{REGEX});
  my @absents = map {" " . $_ . " (" .($SITE_STATUS->{$_}) . ")\n" }
                grep {$SITE_STATUS->{$_} == 0}
                keys %$SITE_STATUS;
  my @uniques = map {" " . $_ . " (" .($SITE_STATUS->{$_}) . ")\n" }
                grep {$SITE_STATUS->{$_} == 1} 
                keys %$SITE_STATUS;
  my @rares = map {" " . $_ . " (" .($SITE_STATUS->{$_}) . ")\n" }
              grep {$SITE_STATUS->{$_} > 1 && $SITE_STATUS->{$_} <= 10} 
              keys %$SITE_STATUS;
  my ($absverb, $anum) = scalar(@absents) != 1 ? ("are", "s") : ("is", "");
  my $abs = "There $absverb " . scalar(@absents) . " absent site$anum:\n";
  my ($univerb, $unum) = scalar(@uniques) != 1 ? ("are", "s") : ("is", "");
  my $uni = "There $univerb " . scalar(@uniques) . " unique site$unum:\n";
  my ($rarverb, $rnum) = scalar(@rares) != 1 ? ("are", "s") : ("is", "");
  my $rar = "There $rarverb " . scalar(@rares) . " rare site$rnum (2 to 10):\n";
  
  print $pa{OUTPUT} eq "html" ? h3($abs) : $abs;
  print @absents, "\n";
  print $pa{OUTPUT} eq "html" ? h3($uni) : $uni;
  print @uniques, "\n";
  print $pa{OUTPUT} eq "html" ? h3($rar) : $rar;
  print @rares, "\n";
}

print "\n\n", "Report generated by $bsversion\n\n";
print "</pre>\n" if ($pa{OUTPUT} eq "html");

exit;

__END__

=head1 NAME

  BS_ChromosomeAnalyzer.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility provides a broad summary of features in a chromosome. If you ask
   for html output and have GBrowse enabled, every feature will have a link to
   itself in GBrowse.
  
  If you choose to analyze protein coding genes, this utility will tell you 
   which genes are the smallest or the largest, which genes are essential,
   which genes overlap, which genes have introns, and where the biggest gene
   deserts are. It will also create a codon table and an RSCU value table and 
   list any modifications to protein coding genes.
   
  If you choose to analyze non protein coding genes, transposons and repeat 
   features, or chromosome features, you will get a list of features and their
   coordinates.
   
  If you choose to analyze restriction enzyme recognition sites, you will get a 
   list of absent, unique, and rare (2-10 occurences) restriction enzyme 
   recognition sites.
   
=head1 ARGUMENTS

Required arguments:

  -CH,   --CHROMOSOME : The name of the chromosome to be analyzed 

Optional arguments:

  -P,   --PCG   : Analyze protein coding genes
  -N,   --NPCG  : Analyze non protein coding genes
  -T,   --TR    : Analyze transposons and repeat features
  -CF,  --CF    : Analyze other chromosome features
  -RE,  --RE    : Analyze restriction enzyme recognition sites
  -RES, --RESET : Which list of restriction enzymes to use (config/enzymes)
  -S,   --SCOPE : [seg, chrom (default) The scope of analysis. seg requires 
                  startpos and stoppos. 
  -STA, --STARTPOS : The first base for analysis;ignored unless SCOPE = seg
  -STO, --STOPPOS  : The last base for analysis;ignored unless SCOPE = seg
  -O,   --OUTPUT   : Determines if output comes as html or txt (default)
  -h,   --help     : Display this message

=cut
