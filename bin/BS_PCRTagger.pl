#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(melt regres complement compare_sequences);
use Bio::GeneDesign::Codons qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::BLAST qw(:all);
use Bio::BioStudio::GFF3 qw(:all);
use Bio::BioStudio::GBrowse qw(:all);
use Bio::SeqFeature::Generic;
use Time::Format qw(%time);
use Getopt::Long;
use Pod::Usage;
use Config::Auto;

use strict;

use vars qw($VERSION);
$VERSION = '2.00';
my $bsversion = "BS_PCRTagger_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'OLDCHROMOSOME=s'   => \$pa{OLDCHROMOSOME},
      'EDITOR=s'          => \$pa{EDITOR},
      'MEMO=s'            => \$pa{MEMO},
      'SCALE=s'           => \$pa{SCALE},
      'SCOPE=s'           => \$pa{SCOPE},
      'STARTPOS=i'        => \$pa{STARTPOS},
      'STOPPOS=i'         => \$pa{STOPPOS},
      'MINTAGMELT=i'      => \$pa{MINTAGMELT},
      'MAXTAGMELT=i'      => \$pa{MAXTAGMELT},
      'MINTAGLEN=i'       => \$pa{MINTAGLEN},
      'MAXTAGLEN=i'       => \$pa{MAXTAGLEN},
      'MINAMPLEN=i'       => \$pa{MINAMPLEN},
      'MAXAMPLEN=i'       => \$pa{MAXAMPLEN},
      'MAXAMPOLAP=i'      => \$pa{MAXAMPOLAP},
      'MINPERDIFF=i'      => \$pa{MINPERDIFF},
      'MINORFLEN=i'       => \$pa{MINORFLEN},
      'FIVEPRIMESTART=i'  => \$pa{FIVEPRIMESTART},
      'MINRSCUVAL=f'      => \$pa{MINRSCUVAL},
      'OUTPUT=s'          => \$pa{OUTPUT},
			'help'			        => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

$pa{SCALE} = "chrom" unless $pa{SCALE};
$pa{SCOPE} = "chrom" unless $pa{SCOPE};
$pa{OUTPUT} = "txt" unless $pa{OUTPUT};
$pa{MINTAGMELT} = 58 unless $pa{MINTAGMELT};
$pa{MAXTAGMELT} = 60 unless $pa{MAXTAGMELT};
$pa{MINPERDIFF} = 33 unless $pa{MINPERDIFF};
$pa{MINTAGLEN} = 19 unless $pa{MINTAGLEN};
$pa{MAXTAGLEN} = 28 unless $pa{MAXTAGLEN};
$pa{MINAMPLEN} = 200 unless $pa{MINAMPLEN};
$pa{MAXAMPLEN} = 500 unless $pa{MAXAMPLEN};
$pa{MAXAMPOLAP} = 25 unless $pa{MAXAMPOLAP};
$pa{MINORFLEN} = 501 unless $pa{MINORFLEN};
$pa{FIVEPRIMESTART} = 101 unless $pa{FIVEPRIMESTART};
$pa{MINRSCUVAL} = 0.06 unless $pa{MINRSCUVAL};
$pa{BSVERSION} = $bsversion;

unless ($pa{OLDCHROMOSOME})
{
  print "\n ERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{OLDCHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}, $pa{GENVER}, $pa{VERNAME}) 
    = ($1, $2, $3, $4);
  $pa{SEQID} = "chr" . $pa{CHRNAME};
  $pa{OLDFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                   $pa{SEQID} . "/" . $pa{OLDCHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n\n" 
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n\n");
}

unless ($pa{EDITOR} && $pa{MEMO})
{
  print "\n ERROR: Both an editor's id and a memo must be supplied.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}

if ($pa{SCOPE} && $pa{SCOPE} eq "seg" && 
  ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS}))
{
  die "\n ERROR: The start and stop coordinates do not parse.\n\n";
}

if ( $pa{MAXTAGMELT} < $pa{MINTAGMELT} 
  || $pa{MAXTAGMELT} >= 90 || $pa{MINTAGMELT} <= 20)
{
  die "\n ERROR: The tag melting parameters do not parse.\n\n";
}

if ($pa{MAXTAGLEN} < $pa{MINTAGLEN} || $pa{MINTAGLEN} <= 0 ||
  ((($pa{MAXTAGLEN} - 1) % 3 != 0 ) || (($pa{MINTAGLEN} - 1) % 3 != 0)))
{
  print "\n ERROR: The tag length parameters do not parse. ";
  print "Tag lengths must be multiples of 3 plus 1.\n\n";
  die();
}

if ($pa{MAXAMPLEN} < $pa{MINAMPLEN} 
  || $pa{MAXAMPLEN} >= 1000 || $pa{MINAMPLEN} <= 0)
{
  die "\n ERROR: The amplicon length parameters do not parse.\n\n";
}

if ($pa{MINPERDIFF} > 66 || $pa{MINPERDIFF} <= 20 )
{
  die "\n ERROR: The minimum percent difference does not parse.\n\n";
}

if ($pa{MINRSCUVAL} > .6 || $pa{MINRSCUVAL} <= 0 )
{
  die "\n ERROR: The minimum RSCU value parameter does not parse.\n\n";
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
my $BLASTHIT =	qr/^([rcwtmd]+)\_([\w\d\W]+)\s+([\w]+)+\s+([\d\.]+)\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)\s+/;
my $TAGNAME  =	qr/^([rcwtmd]+)\_([\w\d\W]+)/;

my $CODON_TABLE = define_codon_table($SPECIES{$pa{SPECIES}});
my $RSCU_TABLE = define_RSCU_values($SPECIES{$pa{SPECIES}});
   $RSCU_TABLE = RSCU_filter($RSCU_TABLE, $pa{MINRSCUVAL});
$pa{INTRO}    = $pa{GENVER} . "_" . $pa{VERNAME};
$pa{COMMENTS} = get_GFF_comments($pa{OLDCHROMOSOME}, $BS);

if ($pa{SCALE} eq "genome")
{
  $pa{GENVER}++;
}
elsif ($pa{SCALE} eq "chrom")
{
  $pa{VERNAME}++;
  $pa{VERNAME} = "0" . $pa{VERNAME} while (length($pa{VERNAME}) < 2);
}
$pa{NEWCHROMOSOME}  = $pa{SPECIES} . "_" . $pa{SEQID} . "_" 
                    . $pa{GENVER} . "_" . $pa{VERNAME};
$pa{NEWFILE}        = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                      $pa{SEQID} . "/" . $pa{NEWCHROMOSOME} . ".gff";
system("cp $pa{OLDFILE} $pa{NEWFILE}");
system("chmod 777 $pa{NEWFILE}");

print "CONFIGURING DBS...\n";
my $DBLIST = list_databases($BS);
unless (exists ($DBLIST->{$pa{OLDCHROMOSOME}}) )
{
  create_database($pa{OLDCHROMOSOME}, $BS);
  load_database($pa{OLDCHROMOSOME}, $BS);
}
drop_database($pa{NEWCHROMOSOME}, $BS) if exists($DBLIST->{$pa{NEWCHROMOSOME}});
create_database($pa{NEWCHROMOSOME}, $BS);
load_database($pa{NEWCHROMOSOME}, $BS);
$pa{DB} = fetch_database($pa{NEWCHROMOSOME}, $BS);

my @genes = $pa{DB}->features(
    -seq_id => $pa{SEQID},
    -types  => 'gene', 
    -start  => $pa{STARTPOS}, 
    -end    => $pa{STOPPOS},
    -range_type => 'contains');
my %dogenes = map {$_->display_name() => $_} @genes;
$pa{DOGENES} = \%dogenes if ($pa{SCOPE} eq "seg");
$pa{GENEREPORT} = {};
  
my @allgenes  = $pa{DB}->get_features_by_type("gene");
my %geneinf   = map {$_->Tag_load_id => $_} @genes;
$pa{GENEINF}  = \%geneinf;
$pa{ALLORFS}  = ORF_compile(\@allgenes);
$pa{ORIGCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});
$pa{EDITCHR}  = $pa{DB}->fetch_sequence($pa{SEQID});

# compile CDSes to individual gene masks and a big chromosome mask
$pa{GENEMASKS} = {};
my @CDSes;  
foreach my $gene (@allgenes)
{
  my @exons = $gene->get_SeqFeatures("CDS");
  $pa{GENEMASKS}->{$gene->Tag_load_id} = 
              make_mask($gene->end - $gene->start + 1, \@exons, $gene->start-1);
  push @CDSes, @exons;
}
$pa{CHRMASK} = make_mask(length($pa{ORIGCHR}), \@CDSes);

# make display names for the genes for the summary later
$pa{GENEDISPLAY} = $BS->{enable_gbrowse} 
  ? gbrowse_gene_names(\@genes, \%pa, $BS)
  : gene_names(\@genes, $BS);
$pa{NEWGFF} = [];

print "CONFIGURING BLAST...\n";
#Check for and or make BLAST databases
my $BLAST_LABEL    = $pa{OLDCHROMOSOME} . "_genome";
my $wt_chrs = gather_versions($pa{SPECIES}, 0, $BS);
my $BLASTdb = make_BLAST_db($wt_chrs, $BS, $BLAST_LABEL);
my $megaBLASTidx = make_megaBLAST_index($BLASTdb, $BS, $BLAST_LABEL);

$pa{ORF_TAG_INC}    = 1000;
$pa{MELT_FORMULA}   = 3;
my %fams;
my %dicodons;
my $nofirsts;
#Single family codons can't be first or last codons
#codons that don't share their siblings' first two bases can't be first codons
foreach (keys %$RSCU_TABLE)
{
  my $aa = $CODON_TABLE->{$_};
  $fams{$aa} = [] if (! exists $fams{$aa});
  $dicodons{$aa} = {} if (! exists $dicodons{$aa});
  push @{$fams{$aa}}, $_;
  $dicodons{$aa}->{substr($_, 0, 2)}++;
}
foreach my $aa (grep {scalar(@{$fams{$_}}) > 1} keys %dicodons)
{  my $flag = 0;
  foreach (keys %{$dicodons{$aa}})
  {
    $flag++ if ($dicodons{$aa}->{$_} == 1);
  }
  push @$nofirsts, $aa if ($flag != 0);
}
$pa{NOFIRST} = "[" . join("", @$nofirsts) . "]";
$pa{AVOIDAA} = "[" . join("", grep {scalar(@{$fams{$_}}) == 1} keys %fams) ."]";
$pa{RSCU_TABLE} = $RSCU_TABLE;
$pa{CODON_TABLE} = $CODON_TABLE;

#If wiki is in use, create wiki topics
wiki_edit_prep(\%pa, $BS, ["PCR_product", "tag"]) if ($BS->{enable_wiki});

################################################################################
################################### EDITING ####################################
################################################################################

print "EDITING...\n";
my %state;
my $genemasks = {};
my @newcomments;

#Filter ORFs by size
foreach my $geneid (keys %{$pa{GENEINF}})
{
  $state{$geneid} = 1;
  $pa{GENEREPORT}->{$geneid} = "";
  if (length($pa{ALLORFS}->{$geneid}) < $pa{MINORFLEN})
  {
    if ($pa{SCOPE} eq "seg" && ! exists $pa{DOGENES}->{$geneid})
    {
      next;
    }
    $state{$geneid} = -1;
    $pa{GENEREPORT}->{$geneid} .= " Too small for tags;";
  }
}
#Stop processing any genes out of scope now
if ($pa{SCOPE} eq "seg")
{
  my @outscopes = grep { ! exists $pa{DOGENES}->{$_} } keys %{$pa{GENEINF}};
  $state{$_} = 0 foreach (@outscopes);
}
foreach my $geneid (grep {$state{$_} == 1} keys %{$pa{GENEINF}})
{
  my $gene = $pa{GENEINF}->{$geneid};
  $genemasks->{$geneid} = substr($pa{CHRMASK}, $gene->start-1, $gene->end - $gene->start +1);
  my $ubermask = mask_combine($pa{GENEMASKS}->{$geneid}, $genemasks->{$geneid});
  my @laps = $ubermask =~ m/([^2])/g;
  if (length($ubermask) - scalar(@laps) < $pa{MINAMPLEN} )
  {
    $state{$geneid} = -2;
    $pa{GENEREPORT}->{$geneid} .= " Too overlapped;";
  }
}

# Find tags for each gene
my $chr_tags = [];
my $genecount = 0;
print "Parsing genes for tags:\n";
foreach my $geneid (grep {$state{$_} == 1} keys %{$pa{GENEINF}})
{
  my $gene = $pa{GENEINF}->{$geneid};
  print "$geneid  \t";
  $genecount++;
  print "\n" if ($genecount % 10 == 0);
  #Pick tag starting location so that it is not in an intron
  my $start = $gene->strand == 1
      ?  $pa{FIVEPRIMESTART} + $gene->start
      :  $gene->start;
  my $stop = $gene->strand == 1
      ?  $gene->end
      :  $gene->end - $pa{FIVEPRIMESTART};
  my $dist = $start - $gene->start;
  my @checkint = substr($pa{GENEMASKS}->{$geneid}, 0, $dist) =~ m/(0)/g;
  if (scalar(@checkint) != 0)
  {
    my $caught = 0;
    my @subs = flatten_subfeats($gene);
    foreach my $child (@subs)
    {
      if ($child->type eq "CDS" && $child->end < $start)
      {
        $caught += $child->end - $child->start + 1;
        next;
      }
      next if ($child->type eq "intron");
      $start = $child->start + ($pa{FIVEPRIMESTART} - $caught);
      last;
    }
  }
  #Slide for tags
  #print "$gene has $start and $stop (", $gene->start, "..", $gene->stop, ")\n";
  my $tags = tagslide( $gene, $start, $stop, $genemasks->{$geneid}, \%pa);
  push @$chr_tags, @$tags;
}

# BLAST individual tags
print "\n\nBLASTING ", scalar(@$chr_tags), " tags for $pa{SEQID}...\n";
$pa{ID} = $pa{ID} ? $pa{ID} : Digest::MD5::md5_hex(time().{}.rand().$$);
my $blastfile = $BS->{blast_directory} . "/" . $pa{ID} . "_alltags";
open (TAGS, ">$blastfile") || die "can't open $blastfile for some reason $!";
my ($taghsh, $wcounthsh, $mcounthsh) = ({}, {}, {});
foreach my $tag ( @$chr_tags )
{
  my $name = join("", $tag->get_tag_values("load_id"));
	print TAGS ">wt_$name\n", join("", $tag->get_tag_values("wtseq")), "\n";
	print TAGS ">md_$name\n", join("", $tag->get_tag_values("newseq")), "\n";
  $wcounthsh->{"wt_$name"} = 0;
  $wcounthsh->{"md_$name"} = 0;
  $taghsh->{$name} = $tag;
}
close TAGS;
my $command  = "$BS->{blastn} -db $BLASTdb -query $blastfile -task megablast";
   $command .= " -word_size 17 -use_index true -index_name $megaBLASTidx ";
   $command .= "-perc_identity 70 -outfmt 6";
my @resarr = `$command`;
foreach my $hit (@resarr)
{
	if ($hit =~ $BLASTHIT)
	{
		my ($name, $sw) = ($2, $1);
		$wcounthsh->{$name}++ if ($sw eq "wt");
		$mcounthsh->{$name}++ if ($sw eq "md");
	}
}
my @bads = grep {! exists($wcounthsh->{$_}) || $wcounthsh->{$_} != 1 || exists ($mcounthsh->{$_})} keys %$taghsh;
delete @{$taghsh}{@bads};
@$chr_tags = values %$taghsh;

my $genetaghsh = {};
foreach my $tag (@$chr_tags)
{
  my @genes = $tag->get_tag_values("ingene");
  $genetaghsh->{$genes[0]} = [] unless (exists $genetaghsh->{$genes[0]});
  push @{$genetaghsh->{$genes[0]}}, $tag;
}

# Pick pairs
$genecount = 0;
print "Tagging genes...\n";
foreach my $geneid (sort grep {$state{$_} == 1} keys %{$pa{GENEINF}})
{
  my $gene = $pa{GENEINF}->{$geneid};
  print "$geneid  \t";
  $genecount++;
  print "\n" if ($genecount % 10 == 0);
  my $tagtarget = (length($pa{ALLORFS}->{$geneid})-$pa{MINORFLEN});
  $tagtarget = $tagtarget / $pa{ORF_TAG_INC};
  $tagtarget = ($tagtarget % $pa{ORF_TAG_INC}) + 1;
  my $tagcount = 0;
  my @chosen;
  my $offset = $gene->start;
  my @tagarr = exists $genetaghsh->{$geneid}  ? @{$genetaghsh->{$geneid}}  : ();
  my @rc = sort {$a->start <=> $b->start} @tagarr;
  my %tagref = map { $_ => $_ } @rc;
  my @tagmask = (0) x length($genemasks->{$geneid});
  
  #No tags met all criteria
  if (scalar(@rc) <= 1)
  {
    $state{$geneid} = -3;
    $pa{GENEREPORT}->{$geneid} .= " No good tags;";
    next;
  }
  #Range of tags meeting all criteria too narrow for amplicon
  if($rc[0]->start + $pa{MINAMPLEN} > $rc[-1]->end)
  {
    $state{$geneid} = -3;
    $pa{GENEREPORT}->{$geneid} .= " Bad tag range;";
    next;
  }
  #Choose Pairs
  my %usedtags = ();
  #Sort all the tags by the identity between original and recoded oligos
  sub sortdiff
  {
    my @diffa = $a->get_tag_values('difference');
    my @diffb = $b->get_tag_values('difference');
    return $diffa[0] <=> $diffb[0];
  }
  #Pull out all downstream tags eligible to pair with each upstream tag, and vet
  foreach my $utag (sort sortdiff @rc)
  {
    my $tmask = join("", @tagmask);
    my $len = $utag->end - $utag->start + 1;
    next if (substr($tmask, $utag->start - 1 - $offset, $len) =~ /[ud]/);
    my %possibles;
    my @pool = grep {  ! exists $usedtags{$_}
              && $_->end - $utag->start <= $pa{MAXAMPLEN}
              && $_->end - $utag->start >= $pa{MINAMPLEN}
            } @rc;
    foreach my $dtag (@pool)
    {
      #nominal size of the amplicon, with and without intron removal
      my $utagoffset = $utag->start - $offset - 1;
      my $pampsize = $dtag->end - $utag->start;
      my $pampmask = substr($pa{GENEMASKS}->{$geneid}, $utagoffset, $pampsize);
      my $intronbp = scalar( $pampmask =~ m/([0])/g);
      my $ampsize = $pampsize -  $intronbp;
      next if ($ampsize > $pa{MAXAMPLEN} || $ampsize < $pa{MINAMPLEN});
      
      #how many bases this tag would overlap a previously chosen tag
      my $dtaglen = $dtag->end - $dtag->start + 1;
      my $dtagoffset = $dtag->start - $offset - 1;
      my $olap  = scalar(substr($tmask, $dtagoffset, $dtaglen) =~ m/([ud])/g);
      next if ($olap > 0);
      
      #how many bases this amplicon would overlap an existing amplicon
      my @ampolap  = substr($tmask, $utagoffset, $pampsize) =~ m/([^0])/g;
      next if (scalar(@ampolap) > ($pa{MAXAMPOLAP}/100)*$pampsize);
      my @diffs = $dtag->get_tag_values('difference');
      $possibles{$dtag} = [scalar(@ampolap), $diffs[0]];
    }
    if (! scalar(keys %possibles))
    {
      next;
    }
    my @downstreams = sort {  $possibles{$a}->[0] <=> $possibles{$b}->[0] 
                           || $possibles{$a}->[1] <=> $possibles{$b}->[1] } 
                      keys %possibles;
    my @downbuds = map {$tagref{$_}} @downstreams;
    my $partner = BLAST_pairs($utag, \@downbuds, $BLASTdb, $BS, $pa{ID});
    if ($partner)
    {
      $tagcount++;
      push @chosen, [$utag, $partner];
      $usedtags{$utag}++;
      $usedtags{$partner}++;
      
      for my $x ($utag->start - 1 - $offset .. $partner->end - 1 - $offset)
      {
        $tagmask[$x] = $tagmask[$x] =~ /\d/
              ?  $tagmask[$x]+1
              :  $tagmask[$x];
      }
      for my $x ($utag->start - 1 - $offset .. $utag->end  - 1 - $offset)
      {
        $tagmask[$x] = 'u';
      }
      for my $x ($partner->start - 1 - $offset .. $partner->end  - 1 - $offset)
      {
        $tagmask[$x] = 'd';
      }
    }
    last if ($tagcount == $tagtarget);
  }
  if (scalar(@chosen) == 0)
  {
    $state{$geneid} = -5;
    $pa{GENEREPORT}->{$geneid} .= " Couldn't pick a single tag pair;";
    next;
  }
  if ($tagcount < $tagtarget)
  {
    $state{$geneid} = -6;
    $pa{GENEREPORT}->{$geneid} .= " Not as many pairs as wanted;";
  }
  else
  {
    $pa{GENEREPORT}->{$geneid} .= " $tagtarget pairs added;";
  }
  
  #Add tags and amplicons to database, make sequence changes
  my $tcount = 1;
  my %moddedgenes = ();
  foreach my $tagpair (@chosen)
  {
    my ($gutag, $gdtag) = @$tagpair;
    #convert generic seqfeatures to normalized
    my @norms;
    foreach my $g ($gutag, $gdtag)
    {
      my $atts = {'intro' => $pa{INTRO}, 'bsversion' => $pa{BSVERSION}};
      $atts->{$_} = join(",", $g->get_tag_values($_)) foreach $g->get_all_tags;
      my $tag = $pa{DB}->new_feature(
        -seq_id => $pa{SEQID},
        -start => $g->start,
        -end => $g->end,
        -primary_tag => $g->primary_tag,
        -source => "BIO",
        -attributes => $atts
      );
      push @norms, $tag;
    }
    my ($utag, $dtag) = @norms;
    my ($uid, $did) = ($utag->Tag_load_id, $dtag->Tag_load_id);        
    my $aid = $utag->Tag_ingene . "_amp" . $tcount . "v1";
    
    my $amp = $pa{DB}->new_feature(
      -start => $utag->start,
      -end   => $dtag->end,
      -primary_tag => "PCR_product",
      -source => "BIO",
      -seq_id => $pa{SEQID},
      -attributes => {
        'intro' => $pa{INTRO}, 
        'bsversion' => $pa{BSVERSION}, 
        'ingene' => $utag->Tag_ingene,
        'load_id' => $aid,
        'uptag' => $utag->Tag_load_id,
        'dntag' => $dtag->Tag_load_id
      });

    if ($BS->{enable_wiki})
    {
      my $womment  = "# # PCR_product [[$aid]] annotated ";
         $womment .= "(tags [[$uid]] and [[$did]] added)<br>";
      push @{$pa{WIKICOMMENTS}}, $womment;
      foreach my $feat ($amp, $utag, $dtag)
      {
        my $featid = $feat->Tag_load_id;
        my %buds = ();
        $buds{"Down Tag"}    = $dtag if ($featid eq $uid || $featid eq $aid);
        $buds{"Up Tag"}      = $utag if ($featid eq $did || $featid eq $aid);
        $buds{"PCR_product"} = $amp  if ($featid eq $did || $featid eq $uid);
        wiki_add_feature(\%pa, $BS, $feat, \%buds);
      }
      
      my $flag = exists $moddedgenes{$geneid}  ? 1 : 0;
      my $add = "&nbsp;&nbsp;[[$aid]]<br>\n";
      my $note = "<br>PCR Products:<br>\n";
      wiki_update_feature(\%pa, $BS, $gene, $add, $flag, $note);
            
      $moddedgenes{$geneid}++;
    }  
    
    my $utaglen = $utag->end - $utag->start + 1;
    my $dtaglen = $dtag->end - $dtag->start + 1;
    substr($pa{EDITCHR}, $utag->start - 1, $utaglen) = $utag->Tag_newseq;
    substr($pa{EDITCHR}, $dtag->start - 1, $dtaglen) = $dtag->Tag_newseq;
    my $comment  = "# # PCR_product $aid annotated ";
       $comment .= "(tags $uid and $did added)\n";
    push @newcomments, $comment;
    $tcount++;
  }
}

################################################################################
############################### ERROR  CHECKING ################################
################################################################################

#check fidelity of tag substitutions in genes
my @tagged = grep {$state{$_} == 1 || $state{$_} == -6} keys %{$pa{GENEINF}};
foreach my $geneid (@tagged)
{
  my $genfeat = $pa{GENEINF}->{$geneid};
  my $newgeneseq = get_feature_sequence($genfeat, $pa{EDITCHR});
  my $oldgeneseq = get_feature_sequence($genfeat, $pa{ORIGCHR});
  if ($newgeneseq eq $oldgeneseq)
  {
    print "Oh no, the seqence hasn't changed! $geneid\n";
    print "new: $newgeneseq\n\nold: $oldgeneseq\n\n";
    $pa{GENEREPORT}->{$geneid} .= " No change in sequence, ";
    $pa{GENEREPORT}->{$geneid} .= "tags might not have been applied;";
    $state{$geneid} = -7;
  }
	my $neworfseq = "";
  my @subs = flatten_subfeats($genfeat);
  my @CDSes = grep {$_->primary_tag eq "CDS"} @subs;
  @CDSes = sort {$b->start <=> $a->start} @CDSes if ($genfeat->strand == -1);
  @CDSes = sort {$a->start <=> $b->start} @CDSes if ($genfeat->strand == 1);
  foreach (@CDSes)
  {
    my $seq = substr($pa{EDITCHR}, $_->start - 1, $_->end - $_->start + 1);
    $seq = complement($seq, 1) if ($_->strand == -1);
    $neworfseq .= $seq;
  }
  my $newpep = translate($neworfseq, 1, $CODON_TABLE);
  my $oldpep = translate($pa{ALLORFS}->{$geneid}, 1, $pa{CODON_TABLE});
  if ($newpep ne $oldpep)
  {
    print "Oh no, the amino acid sequence is wrong! $geneid\n";
    print "new:\n$newpep\n$neworfseq\n";
    print "old:\n$oldpep\n$pa{ALLORFS}->{$geneid}\n\n";
    $state{$geneid} = -7;
    $pa{GENEREPORT}->{$geneid} .= " Change in amino acid sequence, ";
    $pa{GENEREPORT}->{$geneid} .= "tags applied wrong?;";
  }
}
print "\n\n";

################################################################################
############################ WRITING AND REPORTING #############################
################################################################################
#Assemble new GFF file
#comments
my $comment  = "# $pa{NEWCHROMOSOME} created from $pa{OLDCHROMOSOME} ";
   $comment .= "$time{'yymmdd'} by $pa{EDITOR} ($pa{MEMO})\n";
unshift @newcomments, $comment;
my $womment  = "# [[$pa{NEWCHROMOSOME}]] created from [[$pa{OLDCHROMOSOME}]] ";
   $womment .= "$time{'yymmdd'} by [[Main.$pa{EDITOR}]] ($pa{MEMO})<br>";
unshift @{$pa{WIKICOMMENTS}}, $womment;

make_GFF3(\%pa, $BS, \@newcomments);
update_wiki($BS, \%pa, $pa{WIKICOMMENTS}) if ($BS->{enable_wiki});
update_gbrowse($BS, \%pa) if ($BS->{enable_gbrowse});

print "\n\n";
#check fidelity of newseq tags
foreach my $feature ($pa{DB}->features())
{
  my $flag = check_new_sequence($feature);
  if ($flag == 0)
  {
    print "WARNING - ", $feature->Tag_load_id, " has bad newseq tag; ";
    print $feature->Tag_newseq, " tag vs ", $feature->seq->seq, " actual\n";
  }
}
print "\n\n";

#Summarize the results
my @list = sort {$pa{GENEINF}->{$a}->start <=> $pa{GENEINF}->{$b}->start} 
              keys %state;

my @donegene = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} ==  1} @list;
if (@donegene)
{
  print "The following genes were tagged:\n", genelist(\@donegene), "\n\n";  
}

my @fewpairs = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -6} @list;
if (@fewpairs)
{
  print "The following genes were not tagged as much as one would like:\n";
  print genelist(\@fewpairs), "\n\n";  
}

my @smalls   = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -1} @list;
if (@smalls)
{
  print "The following genes were smaller than $pa{MINORFLEN}bp and were not ";
  print "tagged:\n", genelist(\@smalls), "\n\n";  
}

my @olapped  = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -2} @list;
if (@olapped)
{
  print "The following genes overlapped other genes too much to have an ";
  print "amplicon of at least $pa{MINAMPLEN}bp:\n";
  print genelist(\@olapped), "\n\n";  
}

my @notags   = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -3} @list;
if (@notags)
{
  print "The following genes had either zero or too few tags that met the ";
  print "provided restrictions:\n", genelist(\@notags), "\n\n"   
}

my @badpick  = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -5} @list;
if (@badpick)
{
  print "The following genes had no good pairs:\n";
  print genelist(\@badpick), "\n\n";  
}

my @badinteg = map {$pa{GENEDISPLAY}->{$_}} grep {$state{$_} == -7} @list;
if (@badinteg)
{
  print "The following genes had bad sequence integration:\n";
  print genelist(\@badinteg), "\n\n";  
}

print "Report\n";
foreach my $geneid (sort grep {$pa{GENEREPORT}->{$_}} keys %{$pa{GENEREPORT}})
{
  print $pa{GENEDISPLAY}->{$geneid}, " : $pa{GENEREPORT}->{$geneid}\n";
}

exit;

sub genelist
{
  my ($listref) = @_;
  my ($string, $count) = ("", 0);
  foreach my $item (@$listref)
  {
    $string .= $item;
    $count++;      
    $string .= ", " if ($count < scalar(@$listref));
    $string .= "\n" if ($count % 10 == 0);
  }
  return $string;
}

sub tagslide
{
  my ($gene, $gstart, $gstop, $inmask, $pa) = @_;
  my @tags;
  my ($start, $stop) = ($gstart - $gene->start, $gstop - $gene->start);
  my $geneseq = $gene->strand == 1  ? $gene->seq->seq : complement($gene->seq->seq, 1);
  my $genename = $gene->Tag_load_id;
  my $peptide = translate($pa->{ALLORFS}->{$genename}, 1, $pa->{CODON_TABLE});
  my $count = $pa->{MAXTAGLEN}; 
  my $chrmask = $inmask;
  my $flag = 0;
  while ($start < $stop - $pa->{MINTAGLEN})
  {
    $flag++;
    $start += 2 if ($gene->strand == -1 && $flag == 1);
    my $woligo = substr($geneseq, $start-2, $count+2);
    my $oligo = $gene->strand == 1  
                ? substr($woligo, 2)
                : substr($woligo, 0, length($woligo)-2);
    my $coord = $gene->start + $start;

    #Exclude oligos that don't meet melting standard
    my $currTm = int( melt( $oligo, $pa->{MELT_FORMULA} ) + 0.5 );
    if ($currTm  < $pa->{MINTAGMELT} || $currTm > $pa->{MAXTAGMELT})
    {
      if ($currTm < $pa->{MINTAGMELT})
      {
        $start = $start + 3;
        $count = $pa->{MAXTAGLEN};
      }
      elsif ($currTm > $pa->{MAXTAGMELT})
      {
        ($count, $start) = counterset($count, $start, $pa)
      }
      next;
    }
    
    #Exclude oligos that aren't entirely in exons
    my $olmask = substr($pa->{GENEMASKS}->{$genename}, $start, $count);
    my @nonexons = $olmask =~ m/([^1])/g;
    if (scalar(@nonexons) != 0)
    {
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }
    
    #Exclude oligos that are overlapped by other genes
    $olmask = substr($chrmask, $start -1, $count);
    @nonexons = $olmask =~ m/([^1])/g;
    if (scalar(@nonexons) != 0)
    {
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }
    
    #May have come off an intron wrong    
    my $wtrans = translate( $woligo, $gene->strand, $pa->{CODON_TABLE} );
    if ($peptide !~ regres($wtrans, 2))
    {
      $start--;
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }
    
    #Exclude oligos that begin in unswappable codons or oligos that begin or end in immutable codons
    if    ( substr($wtrans, 0, 1) =~ $pa->{NOFIRST} 
       || ( substr($wtrans, 0, 1) =~ $pa->{AVOIDAA} || substr($wtrans, -1)   =~ $pa->{AVOIDAA} ))
    {
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }    
    
    #Exclude oligos whose recodes don't meet percent difference standard
    my $roligo = complement($woligo, 1);
    my $wmdoligo = $gene->strand == 1 
      ? change_codons($woligo, $pa->{CODON_TABLE}, $pa->{RSCU_TABLE}, 3, 1)
      : change_codons($roligo, $pa->{CODON_TABLE}, $pa->{RSCU_TABLE}, 3, 1);
    $wmdoligo = complement($wmdoligo, 1) if ($gene->strand == -1);  
    my $mdoligo = $gene->strand == 1  
                ? substr($wmdoligo, 2)
                : substr($wmdoligo, 0, length($wmdoligo)-2);
    
    my $comps = compare_sequences($oligo, $mdoligo);
    if ( $$comps{'P'} < $pa->{MINPERDIFF})
    {
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }
    
    #Exclude oligos whose recodes don't meet melting standard
    my $MDTm = int( melt( $mdoligo, $pa->{MELT_FORMULA} ) + 0.5 );
    if ($MDTm  < $pa->{MINTAGMELT} || $MDTm > $pa->{MAXTAGMELT})
    {
      ($count, $start) = counterset($count, $start, $pa);
      next;
    }
    
    #Collect everything else
    $coord = $coord -2 if ($gene->strand == -1);
    my $offset = $coord - $gene->start +1;
    my $tagid = $gene->Tag_load_id . "_" . $offset;
    my $tag = Bio::SeqFeature::Generic->new(
      -start       => $coord,
      -end         => $coord + length($oligo) - 1,
      -primary_tag => "tag",
      -tag         => {
        wtseq => $oligo,
        newseq => $mdoligo,
        wtpos => $coord,
        ingene => $gene->Tag_load_id,
        difference => $comps->{'P'},
        translation => $wtrans,
        display_name => $tagid,
        load_id => $tagid  
      }
    );
    push @tags, $tag;
    ($count, $start) = counterset($count, $start, $pa);
  }  
  return \@tags;
}

sub counterset
{
  my ($count, $start, $pa) = @_;
  $start = $start + 3 if ($count < $pa->{MINTAGLEN} + 2);
  $count = $count >= $pa->{MINTAGLEN} + 2 ? $count - 3 : $pa->{MAXTAGLEN};
  return ($count, $start);
}

#BLAST a list of tag pairs for amplification uniqueness
sub BLAST_pairs
{
  my ($utag, $dntaglist, $BLASTdb, $BS, $id) = @_;
	$id = $id ? $id : Digest::MD5::md5_hex(time().{}.rand().$$);
  my (@wthits, @mdhits, %rcwthits, %rcmdhits) = ((), (), (), ());
  my $blastfile = $BS->{blast_directory} . "/" . $id . "_temptags";
  open (TAGS, ">$blastfile");
  my %dtags;
  my $uname = join("", $utag->get_tag_values("load_id"));
  print TAGS ">wt_$uname\n", join("", $utag->get_tag_values("wtseq")), "\n";
  print TAGS ">md_$uname\n", join("", $utag->get_tag_values("newseq")), "\n";
  foreach my $dtag (@$dntaglist)
  {
    my $dname = join("", $dtag->get_tag_values("load_id"));
    print TAGS ">rcwt_$dname\n", complement(join("", $dtag->get_tag_values("wtseq")), 1), "\n";
    print TAGS ">rcmd_$dname\n", complement(join("", $dtag->get_tag_values("newseq")), 1), "\n";
    $rcwthits{"rcwt_$dname"} = [];
    $rcmdhits{"rcwt_$dname"} = [];
    $dtags{"rcwt_$dname"} = $dtag;
  }
  close TAGS;
  my $command  = "$BS->{blastn} -db $BLASTdb -query $blastfile -task ";
     $command .= "blastn-short -word_size 4 -gapextend 2 -gapopen 1 -penalty ";
     $command .= "-1 -perc_identity 70 -best_hit_overhang 0.25 -outfmt 6";
  my @resarr = `$command`;
  system ("rm $blastfile");
  foreach my $hit (@resarr)
  {
    my @temp = split(/\s+/, $hit);
    if ($temp[2] >= 85 && $temp[3] >= 0.7*($utag->end - $utag->start + 1))
    {
      my $swit = $1 if ($temp[0] =~ $TAGNAME);
      push @wthits, \@temp if ($swit eq "wt");
      push @mdhits, \@temp if ($swit eq "md");
      if ($swit eq "rcwt")
      {
        next if ($temp[9] eq $dtags{$temp[0]}->start);
        push @{$rcwthits{$temp[0]}}, \@temp;
      }
      elsif ($swit eq "rcmd")
      {
        push @{$rcmdhits{$temp[0]}}, \@temp;
      }
    }
  }
  foreach my $rctagid (keys %dtags)
  {
    my $rctag = $dtags{$rctagid};
    my ($rcflag, $mdflag) = (0, 0);
    if (! exists $rcwthits{$rctagid})
    {
      $rcflag++;
    }
    else
    {	
      my $nogo = 0;
      foreach my $temp (@{$rcwthits{$rctagid}})
      {
        foreach my $wthit (@wthits)
        {
          next if ($$temp[1] ne $$wthit[1]);
          next if ($$temp[6] > $$temp[7] && $$wthit[6] > $$wthit[7]);
          next if ($$temp[7] > $$temp[6] && $$wthit[7] > $$wthit[6]);
          $nogo++;
        }
      }
      $rcflag++ if ($nogo == 0);
    }
    if (! exists $rcmdhits{$rctagid})
    {
      $mdflag++;
    }
    else
    {
      my $nogo = 0;
      foreach my $temp (@{$rcmdhits{$rctagid}})
      {
        foreach my $mdhit (@mdhits)
        {
          next if ($$temp[1] ne $$mdhit[1]);
          next if ($$temp[6] > $$temp[7] && $$mdhit[6] > $$mdhit[7]);
          next if ($$temp[7] > $$temp[6] && $$mdhit[7] > $$mdhit[6]);
        }
      }
      $mdflag++ if ($nogo == 0);
    }
    if ($rcflag + $mdflag == 2)
    {
      return $rctag;
      last;
    }
  }
  return undef;
}

__END__

=head1 NAME

  BS_PCRTagger.pl

=head1 VERSION

  Version 2.00

=head1 DESCRIPTION

  This utility creates unique tags for open reading frames to aid the analysis 
    of synthetic content in a nascent synthetic genome. Each tag in a gene has
    a wildtype and a synthetic version that correspond to the same offset in the
    gene; each tag can be paired with another to form gene specific amplicons
    which are also specific to either wildtype or synthetic sequence, depending
    on which tags are used.
    
  To pick tags for a chromosome, each open reading frame over I<MINORFLEN> base 
   pairs long will be slightly recoded to contain a set of PCR tags. The 
   locations and sequences of these tags are carefully chosen to maximize the 
   selectivity of the tags for either wild type or synthetic sequence. Each wild 
   type or synthetic tag and its reverse complement are unique in the entire 
   wild type genome; this is accomplished by creating a BLAST database for the 
   entire wild type genome and BLASTing each potential tag against it (this 
   requires that a complete wild type genome is available in the BioStudio
   repository). Pairs of tags are selected in such a way that they will not 
   amplify any other genomic sequence under 1000 bases long. Each synthetic
   counterpart to a wild type tag is recoded with GeneDesign's “most different” 
   algorithm to guarantee maximum nucleotide sequence difference while 
   maintaining identical protein sequence and, hopefully, minimizing any effect
   on gene expression. The synthetic tags are all at least I<MINPERDIFF> percent 
   recoded from the wild type tags. Each tag is positioned in such a way that
   the first and last nucleotides correspond to the wobble of a codon that can 
   be edited to change its wobble without changing its amino acid.  This usually
   automatically excludes methionine or tryptophan, but it can exclude others
   when a I<MINRSCUVAL> filter is in place. The wobble restriction ensures that
   the synthetic and wild type counterparts have different 5’ and 3’ 
   nucleotides, minimizing the chances that they (and their complements) will 
   cross-prime. This means that tags will be between I<MINTAGLEN> and 
   I<MAXTAGLEN> base pairs long, where I<TAGLEN> is a multiple of 3 plus 1. All 
   tags have melting temperature between I<MINTAGMELT> and I<MAXTAGMELT> so they
   can be used in a single set of PCR conditions.
   
  Tag pairs are chosen to form amplicons specific for each ORF, with at least 
   one amplicon chosen per kilobase of ORF. Each amplicon is between 
   I<MINAMPLEN> and I<MAXAMPLEN> base pairs long, ensuring that they will all 
   fall within an easily identifiable range on an agarose gel. No amplicon will 
   be chosen within the first I<FIVEPRIMESTART> base pairs of an ORF to avoid 
   disrupting unknown regulatory features. Amplicons are forbidden from 
   overlapping each other by more than I<MAXAMPOLAP> percent.

=head1 ARGUMENTS

Required arguments:

  -O, --OLDCHROMOSOME : The chromosome to be modified
  -E, --EDITOR : The person responsible for the edits
  -ME, --MEMO : Justification for the edits
  
Optional arguments:

  -SCA, --SCALE : [genome, chrom (def)] Which version number to increment
  -SCO, --SCOPE : [seg, chrom (def)] How much sequence will the edit affect. 
                  seg requires STARTPOS and STOPPOS.
  -STA, --STARTPOS : The first base for analysis;ignored unless SCOPE = seg
  -STO, --STOPPOS  : The last base for analysis;ignored unless SCOPE = seg
  --MINTAGMELT : (default 58) Minimum melting temperature for tags
  --MAXTAGMELT : (default 60) Maximum melting temperature for tags
  --MINPERDIFF : (default 33) Minimum base pair difference between synthetic and
                 wildtype versions of a tag
  --MINTAGLEN  : (default 19) Minimum length for tags. Must be a multiple of 3, 
                 plus 1
  --MAXTAGLEN  : (default 28) Maximum length for tags. Must be a multiple of 3,
                 plus 1
  --MINAMPLEN  : (default 200) Minimum span for a pair of tags 
  --MAXAMPLEN  : (default 500) Maximum span for a pair of tags
  --MAXAMPOLAP : (default 25) Maximum percentage of overlap allowed between 
                 different tag pairs
  --MINORFLEN  : (default 501) Minimum size of gene for tagging eligibility
  --FIVEPRIMESTART : (default 101) The first base in a gene eligible for a tag
  --MINRSCUVAL : (default 0.06) The minimum RSCU value for any replacement codon 
                 in a tag
  --OUTPUT    : [html, txt (def)] Format of reporting and output.
  -h, --help : Display this message
  
=cut