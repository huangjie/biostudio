#!/usr/bin/env perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::GeneDesign::Codons qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Bio::BioStudio::Diff qw(:all);
use Bio::BioStudio::GFF3 qw(:all);
use Config::Auto;
use Getopt::Long;
use Pod::Usage;
use CGI qw(:all);

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_ChromosomeDiff_$VERSION";

$| = 1;

my %pa;
GetOptions (
			'FIRST=s'       => \$pa{FIRST},
			'SECOND=s'      => \$pa{SECOND},
			'OUTPUT=s'      => \$pa{OUTPUT},
			'PTRANSLATION'  => \$pa{TRX},
			'NSEQUENCE'     => \$pa{SEQ},
			'NALIGN'        => \$pa{ALN},
			'PALIGN'        => \$pa{TRXLN},
			'ID'            => \$pa{ID},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

my $xlationref = {}; #to speed degraded codon translation analysis;

$pa{OUTPUT} = "txt" unless $pa{OUTPUT};
$pa{BSVERSION} = $bsversion;

unless ($pa{FIRST} && $pa{SECOND})
{
  print "\n ERROR: You must specify both an original and ";
  print "a variant chromosome to compare.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
}
if ($pa{FIRST} eq $pa{SECOND})
{
  print "\n ERROR: There are no differences to display; you only selected ";
  print "a single chromosome version.\n\n";
  die();
}

my ($oldsp, $chrname) = ($1, $2) if ($pa{FIRST} =~ $VERNAME);
my ($newsp, $newname) = ($1, $2) if ($pa{SECOND} =~ $VERNAME);
if ($oldsp ne $newsp)
{
  print "\n ERROR: There are no differences to display; you selected ";
  print "chromosomes from two different species.\n\n";
  die();
}
if ($newname ne $chrname)
{
  print "\n ERROR: There are no differences to display; you selected two ";
  print "different chromosomes, as opposed to two versions of a single ";
  print "chromosome.\n\n";
  die();
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
my @output;
my @summary;

my $DBLIST = list_databases($BS);
unless (exists($DBLIST->{$pa{FIRST}}))
{
  drop_database($pa{FIRST}, $BS);
  create_database($pa{FIRST}, $BS);
  load_database($pa{FIRST}, $BS);  
}
unless (exists($DBLIST->{$pa{SECOND}}))
{
  drop_database($pa{SECOND}, $BS);
  create_database($pa{SECOND}, $BS);
  load_database($pa{SECOND}, $BS);  
}
my $FIRSTDB  = fetch_database($pa{FIRST}, $BS);
my $SECONDDB = fetch_database($pa{SECOND}, $BS);
$pa{CODON_TABLE} = define_codon_table($SPECIES{$oldsp});

################################################################################
################################## COMPARING ###################################
################################################################################
print "In going from $pa{FIRST} to $pa{SECOND}:\n\n";

my @DIFFS = compare_dbs(\%pa, $BS, $FIRSTDB, $SECONDDB);
my %types = map {$_->[0] => 1} @DIFFS;
my $x = 0;

foreach my $changetype (keys %types)
{
  my @foundchanges = sort { $a->[1]->start <=> $b->[1]->start } 
                     grep { $_->[0] == $changetype            } @DIFFS;
  my $header = scalar(@foundchanges) . " " . $CHANGES{$changetype}->[2] ."\n";
  
  if ($pa{OUTPUT} eq "html")
  {
    my @tablerows = map {td("&nbsp;", $$_[2])} @foundchanges;
    my $headanchor = "<a name=\"$x\">$header</a>";
    push @summary, "<a href=\"\#$x\">$header</a><br>";   
    unshift @tablerows, td($headanchor);
    push @output, table({-border=>1, -width=>'90%', -align=>'CENTER'},
              TR({-align=>'LEFT', -valign=>'TOP'}, \@tablerows));
  }
  else
  {
    push @summary, $header;
    push @output, $header;
    push @output,  map {" " . $$_[2] . "\n"} @foundchanges;
  }  
  push @output, "\n\n";
  $x++;
}

#Version annotation 
if ($pa{OUTPUT} eq "html")
{
  push @summary, "<a href=\"\#$x\">Diff of Comments</a><br>";
  my $topper  = "<a name = \"$x\">Diff</a> of annotation comments ";
     $topper .= "from $pa{FIRST} to $pa{SECOND}:<br>";
  push @output, $topper;
}
else
{
  push @summary, "Diff of Comments\n";
  push @output,  "Diff of annotation comments from $pa{FIRST} to $pa{SECOND}:\n";
}
my $FIRSTCOM  = get_GFF_comments($pa{FIRST}, $BS);
my $SECONDCOM = get_GFF_comments($pa{SECOND}, $BS);
my $diff      = compare_comments($FIRSTCOM, $SECONDCOM);
push @output, $diff;

print "Summary:\n";
print $_ foreach (@summary);
print "\n\n";
print "Results:\n";
print @output;

exit;

__END__

=head1 NAME

  BS_ChromosomeDiff.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility takes two versions of a chromosome and lists the differences
   between them. It will take note of added or deleted features, the addition or
   loss of subfeatures, changes in ORF translation, changes in feature sequence,
   and changes in annotation.

=head1 ARGUMENTS

Required arguments:

  -F   --FIRST   : the 'original' chromosome
  -S   --SECOND  : the 'variant' chromosome
  
Optional arguments:

  -PT,   --PTRANSLATION : check ORF translations
  -PA,   --PALIGN : make alignments for features with translation differences
  -NS,   --NSEQUENCE : check nucleotide sequence differences
  -NA,   --NALIGN : make alignments for features with nucleotide changes
  -O,    --OUTPUT : html or defaults to txt
  -h,    --help : Display this message

=cut
