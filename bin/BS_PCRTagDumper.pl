#!/usr/bin/perl

use Bio::GeneDesign::Basic qw(:all);
use Bio::BioStudio::Basic qw(:all);
use Bio::BioStudio::MySQL qw(:all);
use Config::Auto;
use Getopt::Long;
use Pod::Usage;

use strict;
use warnings;

use vars qw($VERSION);
$VERSION = '1.00';
my $bsversion = "BS_PCRTagDumper_$VERSION";

$| = 1;

my %pa;
GetOptions (
      'CHROMOSOME=s'  => \$pa{CHROMOSOME},
      'SCOPE=s'       => \$pa{SCOPE},
      'STARTPOS=i'    => \$pa{STARTPOS},
      'STOPPOS=i'     => \$pa{STOPPOS},
      'OUTPUT=s'      => \$pa{OUTPUT},
			'help'			    => \$pa{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($pa{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $BS = configure_BioStudio("**CONFLOC**");

$pa{SCOPE} = "chrom" unless $pa{SCOPE};
$pa{OUTPUT} = "txt" unless $pa{OUTPUT};
$pa{BSVERSION} = $bsversion;

unless($pa{CHROMOSOME})
{
  print "\nERROR: No chromosome was named.\n\n";
  pod2usage(-verbose=>99, -sections=>"ARGUMENTS");
  die;
}  

if ($pa{CHROMOSOME} =~ $VERNAME)
{
  ($pa{SPECIES}, $pa{CHRNAME}) = ($1, $2);
  $pa{SEQID} = "chr" . $pa{CHRNAME};  
  $pa{OLDFILE} = $BS->{genome_repository} . "/" . $pa{SPECIES} . "/" . 
                   $pa{SEQID} . "/" . $pa{CHROMOSOME} . ".gff";
  die "\n ERROR: Can't find the chromosome in the repository.\n"
    unless (-e $pa{OLDFILE});
}
else
{
  die ("\n ERROR: Chromosome name doesn't parse to BioStudio standard.\n");
}

if ($pa{SCOPE} && $pa{SCOPE} eq "seg" && 
  ((! $pa{STARTPOS} || ! $pa{STOPPOS}) || $pa{STOPPOS} <= $pa{STARTPOS}))
{
  die "\n ERROR: The start and stop coordinates do not parse.\n";
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
my $PCRPRODUCT =	qr/\_amp(\d+)v(\d+)/;

my $DBLIST = list_databases($BS);
unless (exists($DBLIST->{$pa{CHROMOSOME}}))
{
  create_database($pa{CHROMOSOME}, $BS);
  load_database($pa{CHROMOSOME}, $BS);  
}

my $db = fetch_database($pa{CHROMOSOME}, $BS);

my $dna = $db->fetch_sequence($pa{SEQID});

my ($start, $end) = $pa{SCOPE} eq "seg"  
                  ? ($pa{STARTPOS}, $pa{STOPPOS}) 
                  : (1, length($dna));
my @amps = $db->features(
    -range_type => 'contains', 
    -types  => 'PCR_product', 
    -start  => $start,
    -end    => $end);

print "PCR products in $pa{CHROMOSOME} from $start to $end";
print "All complete amplicons inside the range $start..$end are listed here in
    5\'-3\' orientation. Percent difference and melting temperatures are
    calculated as averages.\nCAUTION: if the annotated tag sequence does not 
    match the current sequence, an asterisk will appear by the amplicon number; 
    see the bottom of the page for the actual sequence. \n\n";
print "   ORF      Amp# \t  5'-3' forward Wild Type   \t  ";
print "5'-3' reverse Wild Type   \t  5'-3' forward Synthetic   \t  ";
print "5'-3' reverse Synthetic   \tSize \t%Diff\tWT Tm\tSyn Tm";
print "\t  Links" if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html");
print "\n\n";
my @takenotes;

foreach my $amplicon (sort {$a->start <=> $b->start} @amps)
{
  my $warning = 0;
  my $genename = $amplicon->Tag_ingene;
  my $number = $1 if ($amplicon =~ $PCRPRODUCT);
  my $intro = $amplicon->Tag_intro;
  my $wtsrc = $pa{SPECIES} . $pa{SEQID} . "_$intro";
  my @uptags = $db->features(-name=>$amplicon->Tag_uptag); 
  my @dntags = $db->features(-name=>$amplicon->Tag_dntag);
  my ($uptag, $dntag)   = ($uptags[0], $dntags[0]);
  my ($fwtseq, $rwtseq) = ($uptag->Tag_wtseq, $dntag->Tag_wtseq);
  my ($fmdseq, $rmdseq) = ($uptag->Tag_newseq, $dntag->Tag_newseq);
  my ($fdiff, $rdiff)   = ($uptag->Tag_difference, $dntag->Tag_difference); 
  my ($floc, $rloc)     = ($uptag->location(), $dntag->location());
  
  my $sp1 = " " x (28 - length($fwtseq));
  my $sp2 = " " x (28 - length($rwtseq));
  my $floclen = $floc->end - $floc->start + 1;
  my $rloclen = $rloc->end - $rloc->start + 1;
  my $f_check = substr($dna, $floc->start - $start, $floclen);
  my $r_check = substr($dna, $rloc->end - $rloclen - $start + 1, $rloclen);
  
  if ($rmdseq ne $r_check)
  {
    $warning++;
    my $warnmsg = "* The current sequence for the reverse synthetic primer ";
       $warnmsg = "of amplicon $number in $genename is $r_check\n";
    push @takenotes, $warnmsg;
  }
  if ($fmdseq ne $f_check)
  {
    $warning++;
    my $warnmsg = "* The current sequence for the forward synthetic primer ";
       $warnmsg = "of amplicon $number in $genename is $f_check\n";
    push @takenotes, $warnmsg;
  }
  my $wtstart = $uptag->Tag_wtpos;
  my $wtend = $wtstart + ($amplicon->end - $amplicon->start + 1) - 1;
  my $disclaimer = $warning > 0  ?  "*"  :  "";
  my $diff = int(($rdiff + $fdiff) / 2 + .5);
  my $size = $amplicon->stop - $amplicon->start + 1;
  my $wt_Tm = int( ( melt($fwtseq, 3) + melt($rwtseq, 3) ) / 2 + 0.5);
  my $md_Tm = int( ( melt($fmdseq, 3) + melt($rmdseq, 3) ) / 2 + 0.5);
  print $genename, "      ", $number, " ", $disclaimer, "\t", 
    $fwtseq, "$sp1\t", complement($rwtseq, 1), "$sp2\t", 
    $fmdseq, "$sp1\t", complement($rmdseq, 1), "$sp2\t",  
    $size, "  \t  ", $diff, " \t ", $wt_Tm, " \t    ", $md_Tm;
  if ($BS->{enable_gbrowse} && $pa{OUTPUT} eq "html")
  {

    my $wthref  = "http://$BS->{this_server}/cgi-bin/gb2/gbrowse/$wtsrc/?start";
       $wthref .= "=$wtstart;stop=$wtend;ref=$pa{SEQID};";
    my $wtlink  = "<a href=\"$wthref\" target=\"_blank\" style=";
       $wtlink .= "\"text-decoration:none\">wt</a>";
    my $synhref  = "http://$BS->{this_server}/cgi-bin/gb2/gbrowse/";
       $synhref .= "$pa{CHROMOSOME}/?start=" . $amplicon->start . ";stop=";
       $synhref .= $amplicon->end . ";ref=$pa{SEQID};";
    my $synlink  = "<a href=\"$synhref\" target=\"_blank\" style=";
       $synlink .= "\"text-decoration:none\">syn</a>";    
    print " \t  ", $wtlink, " ", $synlink;    
  }
  print "\n";
}

print "\n\n";
print "$_\n" foreach (@takenotes);
print "\n";

exit;

__END__

=head1 NAME

  BS_PCRTagDumper.pl

=head1 VERSION

  Version 1.00

=head1 DESCRIPTION

  This utility creates a list of PCR Tags from a chromosome.  It will alert when
   the sequence for a synthetic tag is not what was expected; this usually means
   that a subsequent edit modified the sequence without considerately updating
   the tags annotation.

=head1 ARGUMENTS

Required arguments:

  -C, --CHROMOSOME : The chromosome to be parsed

Optional arguments:

  -SC,  --SCOPE : [seg, chrom (def)] How much sequence to parse for tags. 
                  seg requires STARTPOS and STOPPOS.
  -STA, --STARTPOS : The first base for parsing; ignored unless SCOPE = seg
  -STO, --STOPPOS  : The last base for parsing; ignored unless SCOPE = seg
  -OU,  --OUTPUT   : [html, txt (def)] Format of the output
  -h,   --help : Display this message
  
=cut
