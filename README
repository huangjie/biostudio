PREREQUISITES
	
Perl v5.10 is recommended, at least Perl v5.8 is required, although not
everything has been tested with the older version.
  
Required perl libraries are Bio::BioStudio and Bio::GeneDesign, which are
available from cpan
		$ sudo cpan
		> install Bio::BioStudio::Basic Bio::GeneDesign::Basic
or from bitbucket
		$ git clone https://bitbucket.org/notadoctor/biostudio-lib.git
		$ git clone https://bitbucket.org/notadoctor/genedesign-lib.git
		$ cd [biostudio/genedesign]-lib
		$ sudo perl Build.PL
		$ sudo perl ./Build test
		$ sudo perl ./Build install

MySQL 5 is used by BioStudio to index genomes, and by GBrowse2 as a data storage
engine. WARNING! If you use 64 bit perl, use 64 bit mysql.  Mixing 64 bit and 32
bit mysql/perl will result in an intractable library error from DBD::mysql and 
DBI. You will need to create a `bss' user for BioStudio to use MySQL;
		$ mysql -u root
		> create user 'bss'@'localhost' identified by 'bsspass';
		> grant SELECT,INSERT,UPDATE,DELETE,CREATE,DROP to 'bss'@'localhost';

BLAST+ is required for chromosome comparison and PCR Tag generation.  Use the 
new blast+ executabes, available from 
		ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/
and not the older blast you compile in C yourself.  You can tell you have the 
right one by its inclusion of the legacy_blast.pl utility. If you use the Mac OS
X .dmg installer, you may have to correct the legacy_blast.pl script; change 
line 43 to read 
	use constant DEFAULT_PATH => "/usr/local/ncbi/blast/bin";
or whatever directory you installed BLAST+ to and it should be fine.

GBrowse is not required by BioStudio, but it currently provides the only 
graphical user interface. If you do not want to use BioStudio on the command
line, you will have to use GBrowse. Here's how to install GBrowse.

You need a webserver; I recommend Apache 2; GBrowse2 will automatically
configure Apache and it is often already on the computer you are using.

Graphics libraries:
	libjpeg http://sourceforge.net/projects/libjpeg/files/
	libpng http://sourceforge.net/projects/libpng/files/
	libgd https://bitbucket.org/pierrejoye/gd-libgd
	
Bioperl-live is required; this is the development branch.  
The stable will not do for GBrowse2!
	$ git clone git://github.com/bioperl/bioperl-live.git
	$ cd bioperl-live/
	$ sudo perl Build.PL
	$ sudo ./Build installdeps
	$ sudo ./Build test
	$ sudo ./Build install

GBrowse2 is available from github, the defaults are usually appropriate.  
	$ git clone git://github.com/GMOD/GBrowse.git
	$ cd Generic-Genome-Browser/
	$ sudo perl Build.PL
	$ sudo ./Build installdeps
	$ sudo ./Build test
	$ sudo ./Build config
	$ sudo ./Build install



INSTALLATION

To install BioStudio, run the following command:

	sudo perl DEPLOY.PL



GETTING STARTED

Here's an example with yeast.

We must fetch the latest S288C genome annotation from SGD in gff3 format. 
http://downloads.yeastgenome.org/chromosomal_feature/saccharomyces_cerevisiae.gff
This file includes all sixteen chromosomes, the mitochondrial chromosome, and 
the 2-micron plasmid, making it large and too unwieldy to be useful. I usually 
append the accession date to the SGD filename, _YYMMDD, so that the age of the 
annotation will be included in the new gff3 files. We will split the SGD file 
into  separate gff3 files for each DNA molecule with the BS_GFF3Split.pl 
utility. SGD does not include information about essentiality or fast growth 
phenotype in the GFF3 file (it may be in the Note tag, but nowhere else), but 
BioStudio uses that information to render gene features in attention grabbing 
colors and to make decisions about edit preceence. BS_GFF3_Split.pl will take 
lists of essential genes and a list of genes required for fast growth, and 
annotate the newly split chromosomes with that information.
	BS_GFF3Split.pl -F saccharomyces_cerevisiae.gff -S yeast -L essentials.txt

Now the genome repository is populated with 16 chromosomes. To get a chromosome 
into GBrowse, we run the BS_PrepareGBrowse.pl script:
	BS_PrepareGBrowse -CHR yeast_chr01_0_00
	
Now you can navigate to localhost/cgi-bin/gb2/gbrowse/yeast_chr01_0_00 and begin
graphically editing the chromosome.



TROUBLESHOOTING	

Some of GBrowse2’s default features can obscure the editing process.  For 
instance, there is a new “padding” option that shows flanking data on either 
side of a view; this may be confusing to anyone who used the original GBrowse 
and is used to assuming that what is in view is actually encompassed by the 
current coordinates.  To avoid this problem, edit GBrowse.conf and set 
“pad_left = 0” and “pad_right = 0”.  Also, GBrowse2 has a “details multiplier”, 
which loads extra data so a user can scroll right or left in the details view.  
This can be a problem if a user accidentally shifts the view without realizing 
that they are changing the orders to the editor; they will not be modifying the 
segment they think they are modifying.  If this seems to be a problem, set 
“details multiplier = 0”.

Make sure that the genome repository folder, the BLAST folder, the tmp folder, 
and the bs_confs folder are world writeable (substitute your paths):
	
	chmod -R 777 /etc/BioStudio/genomes
	chmod -R 777 /etc/BioStudio/blast
	chmod -R 777 /etc/BioStudio/tmp
	chmod -R 777 /etc/gbrowse2/bs_confs

Make sure the BioStudio bin directory is full of executables:
	
	chmod -R 755 /usr/local/BioStudio/bin


SUPPORT AND DOCUMENTATION

	Each script has POD documentation embedded; documentation is ongoing.
	Email notadoctor@jhu.edu for support.


LICENSE AND COPYRIGHT

Copyright (C) 2011 Sarah Richardson

This program is distributed under the (Revised) BSD License:
L<http://www.opensource.org/licenses/bsd-license.php>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

* Neither the name of Sarah Richardson's Organization
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

