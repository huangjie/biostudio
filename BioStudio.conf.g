#GENERAL
this_server         = **SERVER**
server_permissions  = 1
bin                 = **BINLOC**
feature_file        = **CONFLOC**/config/features.txt
log_file            = **CONFLOC**/logs/BS_debug.log
configuration_dir   = **CONFLOC**/config/
enzyme_dir          = **CONFLOC**/config/enzymes
marker_dir          = **CONFLOC**/config/markers
genome_repository   = **CONFLOC**/genomes
tmp_dir             = **CONFLOC**/tmp


# Bioperl
bioperl_bin         = **BIOPERLLOC**


# GBrowse
enable_gbrowse      = 1
conf_repository     = **GBROWSECONFLOC**/bs_confs
reference_conf_file = **CONFLOC**/gbrowse/BS_conf
GBrowse_conf_file   = **GBROWSECONFLOC**/GBrowse.conf


#  Colors
gene.colors = Verified:darkblue Uncharacterized:lightblue transposable_element:darkgreen pseudogene:lightgreen Essential:red fast_growth:purple Nonessential:darkblue Dubious:grey
tag.color     = green
intron.color  = maroon
stop.color    = maroon
UTR.color     = black
transposable_element.color = green
    

#MySQL
mysql_user  = **MYSQLUSER**
mysql_pass  = **MYSQLPASS**


# BLAST
blast_directory = **CONFLOC**/blast
blastn          = **BLASTLOC**/blastn
makeblastdb     = **BLASTLOC**/makeblastdb
makembindex     = **BLASTLOC**/makembindex
legacyblast     = **BLASTLOC**/legacy_blast.pl


# BioStudio DB
bsdbname            = biostudio_development
bsdbuser            = root
bsdbpass            = 


#Foswiki
enable_wiki       = 0
wiki_default_web  = **CONFLOC**/foswiki
wiki_data         = /Library/WebServer/foswiki/data/
wiki_user         = BioStudioBot
wiki_placeholder  = PLACEHOLDER
wiki_server       = /foswiki/bin/view/BioStudio_
